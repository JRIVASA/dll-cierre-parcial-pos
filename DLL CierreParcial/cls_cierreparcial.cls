VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_cierreparcial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Sub Class_Initialize()
    
    Srv_Local_BD_ADM = "ADM_LOCAL"
    Srv_Local_BD_POS = "POS_LOCAL"
    Srv_Remote_BD_ADM = "VAD10"
    Srv_Remote_BD_POS = "VAD20"
    
End Sub

Property Let Srv_Remote_BD_ADM_Prop(ByVal pValue As String)
    Srv_Remote_BD_ADM = pValue
End Property

Property Get Srv_Remote_BD_ADM_Prop() As String
    Srv_Remote_BD_ADM_Prop = Srv_Remote_BD_ADM
End Property

Property Let Srv_Remote_BD_POS_Prop(ByVal pValue As String)
    Srv_Remote_BD_POS = pValue
End Property

Property Get Srv_Remote_BD_POS_Prop() As String
    Srv_Remote_BD_POS_Prop = Srv_Remote_BD_POS
End Property

Property Let Srv_Local_BD_ADM_Prop(ByVal pValue As String)
    Srv_Local_BD_ADM = pValue
End Property

Property Get Srv_Local_BD_ADM_Prop() As String
    Srv_Local_BD_ADM_Prop = Srv_Local_BD_ADM
End Property

Property Let Srv_Local_BD_POS_Prop(ByVal pValue As String)
    Srv_Local_BD_POS = pValue
End Property

Property Get Srv_Local_BD_POS_Prop() As String
    Srv_Local_BD_POS_Prop = Srv_Local_BD_POS
End Property

Property Let RetiroParcialLocal_ImprimirCopia(ByVal pValue As String)
    mVarImprimirCopia = pValue
End Property

Property Get RetiroParcialLocal_ImprimirCopia() As String
    RetiroParcialLocal_ImprimirCopia = mVarImprimirCopia
End Property

Public Function MostrarCierre(ConexionVAD10 As ADODB.Connection, ConexionVAD20 As ADODB.Connection, _
ConexionVAD10Pos As ADODB.Connection, ConexionVAD20Pos As ADODB.Connection, NumeroCaja As String, _
Sucursal As String, ServidorRemoto As String, ObjetoFiscal, esFiscal As Boolean, _
TipoFiscal, Optional pPosTecladoVirtual As Integer = 1)
    
    On Error GoTo ErrorMostrarCierre
    
    Dim Rs As New ADODB.Recordset, Sql As String
    
    Set ConexionVAD10Red = ConexionVAD10
    Set ConexionVAD20Red = ConexionVAD20
    Set ConexionVAD10Local = ConexionVAD10Pos
    Set ConexionVAD20Local = ConexionVAD20Pos
    
    Set ConexionVAD20RedGrabar = New Connection 'AbrirConexion(ServidorRemoto, "" & Srv_Remote_BD_POS & "")
    ConexionVAD20RedGrabar.ConnectionString = ConexionVAD20Red.ConnectionString
    ConexionVAD20RedGrabar.Open
    
    Set DllFiscal = ObjetoFiscal
    
    EsImpresoraFiscal = esFiscal
    
    Tipo_ImpresoraFiscal = TipoFiscal
    
    TipoPos_TecladoManual = pPosTecladoVirtual
    
    'caja = Numerocaja
    Localidad = Sucursal
    
    Select Case Val(BuscarReglaNegocioStr("ImpresoraTicket_TipoLetra", "0"))
        Case 0 ' Default Versiones Anteriores. Sin Cambio.
            ImpresoraTicket_TipoLetra = Empty
        Case 1
            ImpresoraTicket_TipoLetra = "Courier New"
        Case 2
            ImpresoraTicket_TipoLetra = "Courier"
        Case 3
            ImpresoraTicket_TipoLetra = "Lucida Console"
        Case 4
            ImpresoraTicket_TipoLetra = "Console"
    End Select
    
    ImpresoraTicket_TamañoFuente = Val(BuscarReglaNegocioStr("ImpresoraTicket_TamañoFuente", "0"))
    POS_IngresarEfectivoMontoDirecto = Val(BuscarReglaNegocioStr("POS_IngresarEfectivoMontoDirecto", "0"))
    POS_DeclararEfectivoDesgloseAutomatico = Val(BuscarReglaNegocioStr("POS_DeclararEfectivoDesgloseAutomatico", "0"))
    POS_CP_AutoDeposito_PorLote = Val(BuscarReglaNegocioStr("POS_CP_AutoDeposito_PorLote", 0)) = 1 _
    And ExisteCampoTablaV2("TR_CIERRES", "c_ID_Lote_AutoDeposito", ConexionVAD20)
    
    Sql = "SELECT TR_CAJA.C_Codigo AS Codigo, TR_CAJA.C_Cajero AS CodigoCajero, " & _
    "TR_CAJA.C_Desc_Cajero AS DesCajero, TR_CAJA.Turno AS Turno " & _
    "FROM TR_CAJA " & _
    "LEFT JOIN MA_CAJA " & _
    "ON MA_CAJA.C_Codigo = TR_CAJA.C_Codigo " & _
    "WHERE MA_CAJA.c_CodLocalidad = '" & Sucursal & "' " & _
    "AND TR_CAJA.c_Estado NOT IN ('C') " & _
    "AND TR_CAJA.C_Codigo = '" & NumeroCaja & "' "
    
    Rs.Open Sql, ConexionVAD20, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not Rs.EOF Then
        
        LcCajero = Rs!CodigoCajero
        LcDesCajero = Rs!DesCajero
        NumDoc = Rs!Codigo
        Turno = Rs!Turno
        nCaja = Rs!Codigo
        
        Rs.Close
        
        Ficha_Cierre_parcial.Show vbModal
        
    Else
        
        Rs.Close
        
        Mensaje True, "Faltan Datos para hacer el cierre de caja."
        
    End If
    
    Exit Function
    
ErrorMostrarCierre:
    
    'MsgBox ("Ocurrio un error al abrir el cierre, reporte: " & Err.Description)
    
End Function

Private Function AbrirConexion(Servidor As String, BaseDatos As String) As ADODB.Connection
    
    Dim Cadena As String, Conexion As New ADODB.Connection
    
    Cadena = "Driver={SQL Server};Server=" & Servidor & ";Database=" & BaseDatos & ";Uid=sa;Pwd=;"
    
    Conexion.Open Cadena
    
    Set AbrirConexion = Conexion
    
End Function
