VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "obj_listview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Sub AdicionarLw(ByRef lw As ListView, valores As Variant)
    Dim itmX As ListItem
    Set itmX = lw.ListItems.add(, , CStr(IIf(IsNull(valores(0)), "", valores(0))))
    Dim i
    lonvalores = UBound(valores)
    If lonvalores > 0 Then
        For i = 1 To lonvalores
            
            itmX.SubItems(i) = IIf(IsNull(valores(i)), "", valores(i))
        Next
    End If
End Sub
   
Sub BorrarLw(ByRef lw As ListView, ByVal POS As Integer)
    lw.ListItems.Remove (POS)
End Sub

Sub ActualizarLw(ByRef lw As ListView, POS As Integer, valores As Variant)
    Dim itmX As ListItem
    
    lw.ListItems(POS).Text = valores(0)
    Set itmX = lw.ListItems(POS)
    lonvalores = UBound(valores)
    If lonvalores > 0 Then
        For i = 1 To lonvalores
            itmX.SubItems(i) = valores(i)
        Next
    End If
    
End Sub

Function TomardataLw(ByRef lw As ListView, POS As Integer) As Variant
    Dim itmX As ListItem
    Dim loncol As Integer
    Dim tmp()
    
    loncol = lw.ColumnHeaders.Count
    ReDim tmp(loncol - 1)
    Set itmX = lw.ListItems(POS)
    tmp(0) = lw.ListItems(POS)
    If loncol > 1 Then
        For i = 1 To loncol - 1
            tmp(i) = itmX.SubItems(i)
        Next
    End If
    TomardataLw = tmp
End Function

   
