VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Dat_Nom_Caja 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11070
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   15240
   ControlBox      =   0   'False
   Icon            =   "Dat_Nom_Caja.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11070
   ScaleWidth      =   15240
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12765
         TabIndex        =   12
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15825
      TabIndex        =   8
      Top             =   421
      Width           =   15855
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   0
         TabIndex        =   9
         Top             =   120
         Width           =   15240
         _ExtentX        =   26882
         _ExtentY        =   1429
         ButtonWidth     =   2170
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   6
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "F4 &Grabar"
               Key             =   "Grabar"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "F5 &Modificar"
               Key             =   "Modificar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "F8 S&eleccion"
               Key             =   "Seleccion"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "F12 &Salir"
               Key             =   "Salir"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   5
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   240
      TabIndex        =   1
      Top             =   720
      Width           =   14655
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "para Salir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Index           =   5
         Left            =   8040
         TabIndex        =   7
         Top             =   200
         Width           =   1005
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "F12"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Index           =   4
         Left            =   7440
         TabIndex        =   6
         Top             =   200
         Width           =   390
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Presione "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Index           =   3
         Left            =   6360
         TabIndex        =   5
         Top             =   200
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "para seleccionar el Documento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Index           =   2
         Left            =   2040
         TabIndex        =   4
         Top             =   200
         Width           =   3240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ENTER"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   3
         Top             =   200
         Width           =   735
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Presione "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   200
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid fg2 
      Height          =   8895
      Left            =   240
      TabIndex        =   0
      Top             =   1800
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   15690
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16448250
      BackColorSel    =   16761024
      ForeColorSel    =   16448250
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   6120
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Dat_Nom_Caja.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Dat_Nom_Caja.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Dat_Nom_Caja.frx":57EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Dat_Nom_Caja.frx":7580
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Dat_Nom_Caja.frx":9312
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Dat_Nom_Caja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const ColorUp As Long = &H80000018
Private mFactorMoneda As Double
Private rec_consulta As New ADODB.Recordset

Private Sub fg2_DblClick()
    fg2_KeyPress vbKeyReturn
End Sub

Private Sub fg2_KeyPress(KeyAscii As Integer)
    
    ' Nuevo Codigo para seleccionar los que deseo cargar
    
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        
        If Not ValidarConexion(ConexionVAD20RedGrabar) Then
            'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
            Mensaje True, StellarMensaje(16054)
            Exit Sub
        End If
        
        Call ValidarConexion(ConexionVAD10Red)
        
        With fg2
            
            .Col = 4
                
                If .Text = "F" Then
                    .Text = "X"
                    IgualA = "F"
                    Asignar = "X"
                    NumeroColor = &H80000018
                ElseIf .Text = "X" Then
                    .Text = "F"
                    IgualA = "X"
                    Asignar = "F"
                    NumeroColor = &H80000005
                End If
                
                .CellAlignment = 4
                
                For cl = 0 To 5
                    .Col = cl
                    .CellBackColor = NumeroColor
                Next
                
                .Row = .RowSel
                .Col = i
                
            .Col = 0
                NumBanco = .Text
            .Col = 2
                NumCheque = .Text
            .Col = 5
                NumFactura = .Text
            .Col = 12
                IDDenomina = .Text
                
            If rec_consulta.State = adStateOpen Then
                rec_consulta.Close
            End If
            
            rec_consulta.CursorLocation = adUseServer
            
            rec_consulta.Open _
            "SELECT * FROM MA_DETALLEPAGO " & _
            "WHERE c_Util = '" & IgualA & "' " & _
            "AND c_CodBanco = '" & NumBanco & "' " & _
            "AND c_Numero = '" & NumCheque & "' " & _
            "AND c_Factura = '" & NumFactura & "' " & _
            "AND c_Caja = '" & nCaja & "' " & _
            "AND ID = " & IDDenomina & " " & _
            "AND Turno = " & CDec(Turno) & " ", _
            ConexionVAD20RedGrabar, adOpenDynamic, adLockBatchOptimistic
            
            If Not rec_consulta.EOF Then
                rec_consulta.Update
                    rec_consulta!c_Util = Asignar
                    rec_consulta!n_Falta = 0
                rec_consulta.UpdateBatch
            Else
                'MsgBox "Comuniquese con el Dpto de Informatica", vbCritical + vbOKOnly, "Mensaje Stellar"
                Mensaje True, Stellar_Mensaje(16038, True)
                Unload Me
                Exit Sub
            End If
            
            .Col = 0
            oTeclado.Key_Right
            
        End With
        
    End If
    
End Sub

Private Sub Form_Activate()
    If gCambioDenomina Then
        CargarGridDenominaciones
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        Select Case KeyCode

            Case Is = vbKeyG                'ALT+D
                Call Form_KeyDown(vbKeyF4, 0)

            Case Is = vbKeyM                'ALT+M
                Call Form_KeyDown(vbKeyF5, 0)

            Case Is = vbKeyE                'ALT+M
                Call Form_KeyDown(vbKeyF8, 0)

            Case Is = vbKeyA                'ALT+A

            Case Is = vbKeyS                'ALT+S
                Call Form_KeyDown(vbKeyF12, 0)
        End Select
    End If
    
    Select Case KeyCode
        
        Case Is = vbKeyF5
        
        Case Is = vbKeyF4
            
            With fg2
                Monto = 0
                For i = 1 To .Rows - 1
                    .Row = i
                    .Col = 4
                        If .Text = "X" Then
                            .Col = 3
                            Monto = Monto + CDbl(.Text)
                        End If
                Next
            End With
            
            With Forma1.monitor
                
                .Row = .RowSel
                .Col = 3
                    If .Text = Empty Then
                        .Text = 0
                    End If
                    montov = CDbl(.Text)
                .Col = 4
                    .Text = Format(Monto, "###,###,##0.00")
                .Col = 8
                    .Text = Format((Monto - montov), "###,###,##0.00")
                    If CDbl(.Text) < 0 Then
                        .CellForeColor = rojo
                    Else
                        .CellForeColor = negro
                    End If
                    
                .Col = 0
                oTeclado.Key_Right
                
            End With
            
            LcGrabar = True
            
            Unload Me
            
            Exit Sub
            
        Case Is = vbKeyF8
            
            mContFilaGrid = Me.fg2.Rows - 1
            
            For mContadorMarcar = 1 To mContFilaGrid
                Me.fg2.Row = mContadorMarcar
                fg2_KeyPress vbKeyReturn
            Next
            
        Case Is = vbKeyF12
            
            'If vbYes = MsgBox("Al Salir los cambios realizados no ser�n grabados." & Chr(13) & Chr(13) & "�Est� seguro de salir?", vbInformation + vbYesNo, "Mensaje Stellar") Then
            If Mensaje(False, Stellar_Mensaje(16036, True) & Chr(13) & Chr(13) & Stellar_Mensaje(16037, True)) Then
                LcGrabar = False
                Unload Me
                Exit Sub
            End If
            
    End Select
    
End Sub

Private Sub Form_Load()
    
    If Not ValidarConexion(ConexionVAD10Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    'titulo = "Datos Nominales"
    Titulo = Stellar_Mensaje(16035, True)
    Cant = Me.Width / (Len(Titulo) + Len(Stellar))
    Cant = Cant / 4
    'Me.Caption = STELLAR & Space(CANT) & titulo
    Me.lbl_Organizacion.Caption = Stellar & Space(Cant) & Titulo
    LcGrabar = False
    Set Forma1 = Forma
    mFactorMoneda = BuscarFactorMonedaPredeterminada()
    
    CargarGridDenominaciones
    
    Me.Toolbar1.Buttons(1).Caption = Stellar_Mensaje(103, True) 'grabar
    Me.Toolbar1.Buttons(2).Caption = Stellar_Mensaje(207, True) 'modificar
    Me.Toolbar1.Buttons(4).Caption = Stellar_Mensaje(10168, True) 'seleccionar
    Me.Toolbar1.Buttons(5).Caption = Stellar_Mensaje(54, True) 'salir
    Me.Toolbar1.Buttons(6).Caption = Stellar_Mensaje(7, True) 'ayuda
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 And UnloadMode <> 1 Then Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not ValidarConexion(ConexionVAD20RedGrabar) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    If LcGrabar = False Then
        
        With fg2
            For i = 1 To .Rows - 1
                .Row = i
                .Col = 4
                    .Text = "F"
            Next
            Form_KeyDown vbKeyF4, 0
        End With
        
        RANGO = Fecha()
        
        ConexionVAD20RedGrabar.Execute _
        "UPDATE MA_DETALLEPAGO " & _
        "SET c_Util = 'F', " & _
        "n_Falta = n_Monto " & _
        "WHERE c_CodMoneda  = '" & Moneda_Sel & "' " & _
        "AND c_CodDenominacion = '" & Denominacion_Sel & "' " & _
        "AND c_Util = 'X' " & _
        "AND Turno = " & CDec(Turno) & " "
        
    End If
    
    Set rec_consulta = Nothing
    
    Forma1.Calculo
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case Is = UCase("Grabar")
            Form_KeyDown vbKeyF4, 0
            
        Case Is = UCase("Modificar")
            Form_KeyDown vbKeyF5, 0
            
        Case Is = UCase("Seleccion")
            fg2_KeyPress (vbKeySpace)
            
        Case Is = UCase("Salir")
            Form_KeyDown vbKeyF12, 0
            
    End Select
    
End Sub

Private Sub CargarGridDenominaciones()
    
    With fg2
        
        .Row = 0
        .RowHeightMin = 720
        .Cols = 13
        
        .Col = 0
            '.Text = "C�digo Banco"
            .Text = Stellar_Mensaje(15002)
            .CellAlignment = 4
        .Col = 1
            '.Text = "Descripci�n"
            .Text = Stellar_Mensaje(143)
            .CellAlignment = 4
        .Col = 2
            '.Text = "N�mero"
            .Text = Stellar_Mensaje(16033)
            .CellAlignment = 4
        .Col = 3
            '.Text = "Total"
            .Text = Stellar_Mensaje(141)
            .CellAlignment = 4
        .Col = 4
            '.Text = "Selecci�n"
            .Text = Stellar_Mensaje(10086)
        .Col = 5
            '.Text = "Factura"
            .Text = Stellar_Mensaje(2501)
            .CellAlignment = 4
        .Col = 6
            .Text = "moneda_cod"
        .Col = 7
            .Text = "moneda_des"
        .Col = 8
            .Text = "moneda_fac"
        .Col = 9
            .Text = "denomina_cod"
        .Col = 10
            .Text = "denomina_des"
        .Col = 11
            .Text = "denomina_real"
        .Col = 12
            .Text = "ID"
            '.Text = Stellar_Mensaje(15002)
            
        .ColWidth(0) = 2500
        .ColWidth(1) = 5000
        .ColWidth(2) = 2800
        .ColWidth(3) = 2700
        .ColWidth(5) = 0
        .ColWidth(6) = 0
        .ColWidth(7) = 0
        .ColWidth(8) = 0
        .ColWidth(9) = 0
        .ColWidth(10) = 0
        .ColWidth(11) = 0
        .ColWidth(12) = 0
        
        .Col = 0
        .Row = 1
        
    End With
    
    RANGO = Fecha()
    
    Call Apertura_Recordset(rec_consulta)
    
    ' *(" & mFactorMoneda & " / n_factor)
    
    rec_consulta.Open _
    "select c_codbanco, c_banco, c_numero, " & _
    "(n_monto / n_factor) as n_monto, c_caja, c_util, c_factura, ID " & _
    "from ma_detallepago " & _
    "where c_codmoneda  = '" & Moneda_Sel & "' " & _
    "and c_caja = '" & nCaja & "' " & _
    "and c_coddenominacion = '" & Denominacion_Sel & "' " & _
    "and Turno = " & CDec(Turno) & " " & _
    "AND C_UTIL IN ('F', 'X') " & _
    "and c_codcajero = '" & LcCajero & "' " & _
    "order by c_banco ", _
    ConexionVAD20Red, adOpenDynamic, adLockBatchOptimistic
    
    If Not rec_consulta.EOF Then
        
        i = 1
        
        Do Until rec_consulta.EOF()
            
            With fg2
                
                .Row = i
                
                .Col = 0
                    .Text = rec_consulta!c_CodBanco
                .Col = 1
                    .Text = rec_consulta!c_Banco
                .Col = 2
                    .Text = rec_consulta!c_Numero
                .Col = 3
                    .Text = Format(rec_consulta!n_Monto, "###,###,##0.00")
                .Col = 4
                    .Text = rec_consulta!c_Util
                    .CellAlignment = 4
                .Col = 5
                    .Text = rec_consulta!c_Factura
                .Col = 6
                    .Text = Moneda_Cod
                .Col = 7
                    .Text = Moneda_Des
                .Col = 8
                    .Text = Moneda_Fac
                .Col = 9
                    .Text = Denomina_Cod
                .Col = 10
                    .Text = Denomina_Des
                .Col = 11
                    .Text = Denomina_Real
                .Col = 12
                    .Text = rec_consulta!ID
                .CellAlignment = 4
                
                If rec_consulta!c_Util = "X" Then
                    For cl = 0 To 5
                        .Col = cl
                        .CellBackColor = ColorUp
                    Next
                    'gRutinas.CambiarColorFila fg2, .Row, ColorUp
                End If
                
            End With
            
            rec_consulta.MoveNext
            
            i = i + 1
            fg2.Rows = fg2.Rows + 1
            
            If rec_consulta.EOF() Then
                Exit Do
            End If
            
        Loop
        
        fg2.Rows = i
        fg2.Row = 1
        fg2.Col = 0
        oTeclado.Key_Right
        
    Else
        
        'MsgBox "No existen documentos para esa Denominaci�n", vbCritical + vbOKOnly, "Mensaje Stellar"
        Mensaje True, Stellar_Mensaje(16034, True)
        Unload Me
        Exit Sub
        
    End If
    
End Sub
