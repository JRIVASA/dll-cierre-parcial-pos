VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Ficha_Cierre_parcial 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   120
   ClientTop       =   -210
   ClientWidth     =   15330
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "Ficha_cierre_parcial.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   15
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1080
      ScaleWidth      =   15615
      TabIndex        =   11
      Top             =   421
      Width           =   15615
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1185
         Left            =   9960
         TabIndex        =   16
         Top             =   0
         Width           =   5155
         Begin VB.Label lbl_fecha 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "01/01/2016"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   2490
            TabIndex        =   20
            Top             =   690
            Width           =   2415
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   330
            TabIndex        =   19
            Top             =   690
            Width           =   735
         End
         Begin VB.Label lbl_consecutivo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "000000000"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   3210
            TabIndex        =   18
            Top             =   210
            Width           =   1695
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Retiro Parcial No."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   330
            TabIndex        =   17
            Top             =   210
            Width           =   1695
         End
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   12
         Top             =   120
         Width           =   9120
         _ExtentX        =   16087
         _ExtentY        =   1429
         ButtonWidth     =   1561
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   6
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               Object.ToolTipText     =   "Teclado"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1185
      Left            =   240
      TabIndex        =   1
      Top             =   1650
      Width           =   14775
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Diferencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   210
         Left            =   150
         TabIndex        =   10
         Top             =   1500
         Visible         =   0   'False
         Width           =   1020
      End
      Begin VB.Label diferencia 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   3180
         TabIndex        =   9
         Top             =   1530
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.Line Line2 
         Visible         =   0   'False
         X1              =   3060
         X2              =   5400
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line1 
         Visible         =   0   'False
         X1              =   3060
         X2              =   5400
         Y1              =   1380
         Y2              =   1380
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bs. Vendidos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   210
         Left            =   150
         TabIndex        =   8
         Top             =   1020
         Visible         =   0   'False
         Width           =   1260
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Caja No. "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   150
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.Label no_caja 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   1440
         TabIndex        =   6
         Top             =   240
         Width           =   645
      End
      Begin VB.Label bs_vendido 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   3180
         TabIndex        =   5
         Top             =   1020
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label cajero 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   2130
         TabIndex        =   4
         Top             =   240
         Width           =   2865
      End
      Begin VB.Label bs_recibidos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   3180
         TabIndex        =   3
         Top             =   630
         UseMnemonic     =   0   'False
         Width           =   1815
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Efectivo Recibido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   150
         TabIndex        =   2
         Top             =   630
         Width           =   1650
      End
   End
   Begin MSFlexGridLib.MSFlexGrid monitor 
      Height          =   7515
      Left            =   240
      TabIndex        =   0
      Top             =   3030
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   13256
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   0
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":57EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":7580
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_Cierre_parcial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Devolucion As Double

Private RsCierres As New ADODB.Recordset
Private RsTrCierres As New ADODB.Recordset
Private RxDenomina As New ADODB.Recordset
Private RsDetPago As New ADODB.Recordset
Private RsMonitor As New ADODB.Recordset
Private RxMonedas As New ADODB.Recordset
Private Rec_Det_Den As New ADODB.Recordset
Private RsEureka As New ADODB.Recordset
Private RsBingo As New ADODB.Recordset

Private Sub Form_Activate()
    If Me.monitor.Rows <= 1 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 And UnloadMode <> 1 Then Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not ValidarConexion(ConexionVAD20RedGrabar) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    If Not LcGrabar Then
        
        RANGO = Fecha()
        
        ConexionVAD20RedGrabar.Execute _
        "UPDATE MA_DETALLEPAGO SET " & _
        "c_Util = 'F', " & _
        "n_Falta = n_Monto " & _
        "WHERE Turno = " & CDec(Turno) & " " & _
        "AND c_Util IN ('X') " & _
        "AND c_Caja = '" & Trim(no_caja.Caption) & "' "
        
        ExecuteSafeSQL _
        "DELETE FROM TR_DENOMINA_TEMP " & _
        "WHERE Caja = '" & Trim(no_caja.Caption) & "' " & _
        "AND Usuario = '" & LcCajero & "' " & _
        "AND Turno = " & CDec(Turno) & " ", _
        ConexionVAD20RedGrabar
        
    End If
    
    Set RsCierres = Nothing
    Set RsTrCierres = Nothing
    Set RxDenomina = Nothing
    Set RsDetPago = Nothing
    Set RsMonitor = Nothing
    Set Ficha_Cierre_parcial = Nothing
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        Select Case KeyCode

            Case Is = vbKeyG                'ALT+D
                Call Form_KeyDown(vbKeyF4, 0)

            Case Is = vbKeyA                'ALT+A

            Case Is = vbKeyS                'ALT+S
                Call Form_KeyDown(vbKeyF12, 0)
        End Select
    End If
    
    Select Case KeyCode
        
        ' Grabar
        
        Case Is = vbKeyF4
            
            Dim Rec_Grabar As Recordset
            Dim Rec_Denomina As Recordset
            Dim bs_Efectivo As Double, bs_Ancelar As Double
            Dim mFactorMonedaPredeterminada As Double
            Dim mInicio As Boolean, mTotalRetiro As Double
            
            'Set conexionvad20RedGrabar = Conexionvad20Red
            
            On Error GoTo Errores
            
            If CDec(bs_recibidos) <> 0 Then
                
                If Not ValidarConexion(ConexionVAD20RedGrabar) Then
                    'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
                    Mensaje True, StellarMensaje(16054)
                    Exit Sub
                End If
                
                Call ValidarConexion(ConexionVAD10Red)
                
                Buscar_Moneda , , , True
                
                mFactorMonedaPredeterminada = BuscarFactorMonedaPredeterminada
                
                ConexionVAD20RedGrabar.BeginTrans
                
                mInicio = True
                    
                    Screen.MousePointer = 11
                    '****************************************************
                    '*Busca el consecutivo de ajuste
                    '****************************************************
                    'Ent.rscajacons.Requery
                    'Toma el actual
                    'lcconsecu = Format(Ent.rscajacons!cierre_par, "00000000#")
                    'lcconsecu = Format(NO_CONSECUTIVO("cierre_par", True), "00000000#")
                    LcConsecu = Format(Consecutivos("cierre_par", ConexionVAD20Red), "00000000#")
                    '****************************************************
                    'Lo incrementa
                    'new_consec = Ent.rscajacons!cierre_par + 1
                    '****************************************************
                    'comienza actualizacion
                    'Ent.rscajacons.update
                    'Ent.rscajacons!cierre_par = new_consec
                    'Ent.rscajacons.UpdateBatch
                    
                    lbl_consecutivo = Format(CDec(LcConsecu) + 1, "00000000#")
                    
                    '****************************************************
                    '* Actualiza el archivo de ventas diarias
                    '****************************************************
                    'Ficha_Pos.fg2.Row = Ficha_Pos.fg2.RowSel
                    'Ficha_Pos.fg2.Col = 0
                    'Codigo = Ficha_Pos.fg2.Text
                    'i = 1
                    
                    Dim RestanteXFactor As Dictionary
                    Dim RecibidoCierreTotal As Double: RecibidoCierreTotal = 0
                    Dim FactorInfo As Dictionary
                    
                    With fg2
                        
                        '************************************************
                        '* ABRE UN RECORDSET PARA LAS TRANSACCIONES
                        '************************************************
                        
                        Call Apertura_Recordset(RsTrCierres)
                        
                        RsTrCierres.Open _
                        "SELECT * FROM TR_CIERRES " & _
                        "WHERE 1 = 2 ", _
                        ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                        
                        '**********************************************************
                        '* BUSCA LAS DENOMINACIONES REALES RECIBIDAS POR LA CAJA
                        '* PRINCIPAL Y CREA LAS TRANSACCIONES DE CIERRES PARCIALES
                        '**********************************************************
                        
                        Call Apertura_Recordset(RxDenomina)
                        
                        ' SE ORDENA POR CODIGO DE BANCO POR QUE COMO EL EFECTIVO ES '' ENTONCES CUANDRO IMPRIMAMOS EL CIERRE SALGA DE PRIMERO
                        
                        RxDenomina.CursorLocation = adUseClient
                        
                        RxDenomina.Open _
                        "SELECT * FROM TR_DENOMINA_TEMP " & _
                        "WHERE Cantidad <> 0 " & _
                        "AND Caja = '" & Trim(no_caja.Caption) & "' " & _
                        "AND Usuario = '" & LcCajero & "' " & _
                        "AND Turno = " & CDec(Turno) & " " & _
                        IIf(POS_DeclararEfectivoDesgloseAutomatico > 0, _
                        "AND isNULL(CodigoBanco, '') <> '' ", Empty) & _
                        "ORDER BY CodigoBanco, Valor DESC ", _
                        ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                        
                        RxDenomina.ActiveConnection = Nothing
                        
                        ' Consulta para ejecutar algoritmo para corregir transacciones con _
                        multiples factores de multimoneda en el mismo turno.
                        
                        '"AND c_CodMoneda = '" & RxDenomina!CodMoneda & "' " & vbNewLine & _
                        "AND c_CodDenominacion = '" & RxDenomina!CodDenomina & "' " & vbNewLine & _

                        ' Obtener el recibido por denominaci�n por factor seg�n lo vendido.
                        
                        mSQLDenFactores = _
                        ";WITH [Factores] AS ( " & vbNewLine & _
                        "SELECT c_CodMoneda, c_CodDenominacion AS c_CodDenomina, n_Factor, " & vbNewLine & _
                        "MAX(CAST(CAST(d_Fecha AS DATE) AS DATETIME) + CAST(CAST(f_Hora AS TIME) AS DATETIME)) AS FechaFactorMasReciente, " & vbNewLine & _
                        "ROUND(SUM(ROUND(CAST(n_Falta AS MONEY), " & Std_Decm & ", 0)), 8, 0) AS Monto, ROUND(SUM((ROUND(CAST(n_Falta AS MONEY), " & Std_Decm & ", 0) / n_Factor)), 8, 0) AS Cant " & vbNewLine & _
                        "FROM MA_DETALLEPAGO DET " & vbNewLine & _
                        "WHERE Turno = " & Turno & " " & vbNewLine & _
                        "AND c_Caja = '" & Trim(no_caja.Caption) & "' " & vbNewLine & _
                        "AND c_CodCajero = '" & LcCajero & "' " & vbNewLine & _
                        "GROUP BY c_CodMoneda, c_CodDenominacion, n_Factor " & vbNewLine & _
                        ") " & vbNewLine & _
                        "SELECT [Factores].*, Moneda.n_Factor AS FactorDelMomento " & vbNewLine & _
                        "FROM [Factores] INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS Moneda " & vbNewLine & _
                        "ON [Factores].c_CodMoneda = Moneda.c_CodMoneda " & vbNewLine & _
                        "WHERE 1 = 1 " & vbNewLine & _
                        "ORDER BY [Factores].c_CodMoneda, c_CodDenomina, FechaFactorMasReciente DESC; "
                        
                        Dim mRsDenFactores As ADODB.Recordset
                        Set mRsDenFactores = New ADODB.Recordset
                        mRsDenFactores.CursorLocation = adUseClient
                        
                        mRsDenFactores.Open mSQLDenFactores, _
                        ConexionVAD20RedGrabar, adOpenStatic, adLockReadOnly, adCmdText
                        
                        mRsDenFactores.ActiveConnection = Nothing
                        
                        Set RestanteXFactor = New Dictionary
                        
                        If Not mRsDenFactores.EOF Then
                            
                            While Not mRsDenFactores.EOF
                                
                                Dim TmpPK As String, TmpPKDet As String
                                
                                TmpPK = UCase(mRsDenFactores!c_CodMoneda & ";" & mRsDenFactores!c_CodDenomina)
                                
                                If Not RestanteXFactor.Exists(TmpPK) Then
                                    RestanteXFactor.Add TmpPK, New Dictionary
                                End If
                                
                                Set FactorInfo = New Dictionary
                                TmpPKDet = CStr(CDec(mRsDenFactores!n_Factor))
                                
                                If Not RestanteXFactor(TmpPK).Exists(TmpPKDet) _
                                And CDec(mRsDenFactores!Cant) > 0 Then
                                    FactorInfo("PK") = TmpPKDet
                                    FactorInfo("CodMoneda") = mRsDenFactores!c_CodMoneda
                                    FactorInfo("CodDenomina") = mRsDenFactores!c_CodDenomina
                                    FactorInfo("FormaPK") = TmpPK
                                    FactorInfo("Factor") = CDec(mRsDenFactores!n_Factor)
                                    FactorInfo("Cantidad") = CDec(mRsDenFactores!Cant)
                                    FactorInfo("MontoTeorico") = CDec(mRsDenFactores!Monto)
                                    FactorInfo("FechaUltimaTransaccion") = mRsDenFactores!FechaFactorMasReciente
                                    FactorInfo("CantRestante") = CDec(mRsDenFactores!Cant)
                                    RestanteXFactor(TmpPK).Add TmpPKDet, FactorInfo
                                End If
                                
                                mRsDenFactores.MoveNext
                                
                            Wend
                            
                            mRsDenFactores.MoveFirst
                            
                        End If
                        
                        If Not RxDenomina.EOF Then
                            
                            Do Until RxDenomina.EOF
                                
                                Dim ValorLinea As Double, bEsEfectivo As Boolean
                                Dim TmpValor As Double, TmpCant As Double
                                Dim TmpCodDenomina As String
                                
                                ValorLinea = RoundUp(RxDenomina!Cantidad * RxDenomina!Valor, 8)
                                
                                Dim Tmp
                                
                                Set Tmp = ConexionVAD10Red.Execute( _
                                "SELECT CASE WHEN c_Real = 1 AND n_Valor > 0 THEN 1 ELSE 0 END AS bEsEfectivo " & vbNewLine & _
                                "FROM MA_DENOMINACIONES WHERE c_CodMoneda = '" & RxDenomina!CodMoneda & "' " & vbNewLine & _
                                "AND c_CodDenomina = '" & RxDenomina!CodDenomina & "' " & vbNewLine)
                                
                                If Tmp.EOF Then
                                    Err.Raise 999, , "La denominaci�n [" & RxDenomina!CodMoneda & "][" & RxDenomina!CodDenomina & "] no existe."
                                End If
                                
                                bEsEfectivo = CBool(Tmp!bEsEfectivo)
                                
                                Tmp.Close
                                
                                If bEsEfectivo Then
                                    TmpCodDenomina = "Efectivo"
                                Else
                                    TmpCodDenomina = RxDenomina!CodDenomina
                                End If
                                
                                TmpPK = UCase(RxDenomina!CodMoneda & ";" & TmpCodDenomina)
                                
                                If RestanteXFactor.Exists(TmpPK) Then
                                    
                                    While ValorLinea > 0
                                        
                                        If RestanteXFactor(TmpPK).Count > 0 Then
                                            
                                            If bEsEfectivo Then
                                                
                                                ' Aqui cuando hay efectivo se ingresa la cantidad disponible sin _
                                                alterar el valor de la denominaci�n. Si el valor total no esta _
                                                disponible en un factor, entonces se sigue buscando con el resto _
                                                de los factores. Si no est� disponible en ninguno, entonces se _
                                                ingresa la cantidad restante con el factor actual. Es decir, las _
                                                cantidades restantes en un factor pudieran no agotarse por completo _
                                                a menos se utilice en lineas de efectivo donde el valor no sea alterado.
                                                
                                                For Each FactorInfo In AsEnumerable(RestanteXFactor(TmpPK).Items())
                                                    
                                                    If ValorLinea <= 0 Then Exit For
                                                    
                                                    If FactorInfo("CantRestante") <= 0 Then
                                                        
                                                        RestanteXFactor(TmpPK).Remove FactorInfo("PK")
                                                        
                                                    Else
                                                        
                                                        If ValorLinea <= FactorInfo("CantRestante") Then
                                                            TmpCant = Fix(ValorLinea / RxDenomina!Valor)
                                                        Else
                                                            TmpCant = Fix(FactorInfo("CantRestante") / RxDenomina!Valor)
                                                        End If
                                                        
                                                        TmpValor = RoundUp(TmpCant * RxDenomina!Valor, 8)
                                                        
                                                        If TmpValor > 0 Then
                                                            
                                                            GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                                            Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                                            CDec(FactorInfo("Factor")), TmpCant, _
                                                            CDec(RxDenomina!Valor), CVar(RxDenomina!CodigoBanco)
                                                            
                                                            MarcarComoUtilizado RxDenomina!CodMoneda, _
                                                            TmpCodDenomina, CDec(FactorInfo("Factor")), TmpValor
                                                            
                                                            FactorInfo("CantRestante") = _
                                                            RoundUp(FactorInfo("CantRestante") - TmpValor, 8)
                                                            
                                                            ValorLinea = RoundUp(ValorLinea - TmpValor, 8)
                                                            
                                                            mTotalRetiro = RoundUp(mTotalRetiro + _
                                                            (TmpValor * FactorInfo("Factor")), 8)
                                                            
                                                        End If
                                                        
                                                    End If
                                                    
                                                Next
                                                
                                                If ValorLinea > 0 Then
                                                    
                                                    TmpCant = Fix(ValorLinea / RxDenomina!Valor)
                                                    
                                                    TmpValor = RoundUp(TmpCant * RxDenomina!Valor, 8)
                                                    
                                                    If TmpValor > 0 Then
                                                        
                                                        GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                                        Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                                        CDec(RxDenomina!Factor), TmpCant, _
                                                        CDec(RxDenomina!Valor), CVar(RxDenomina!CodigoBanco)
                                                        
                                                        MarcarComoUtilizado RxDenomina!CodMoneda, _
                                                        TmpCodDenomina, CDec(RxDenomina!Factor), TmpValor
                                                        
                                                    End If
                                                    
                                                    ValorLinea = 0
                                                    
                                                    mTotalRetiro = RoundUp(mTotalRetiro + _
                                                    (TmpValor * RxDenomina!Factor), 8)
                                                    
                                                End If
                                                
                                            Else
                                                
                                                ' Aqu� para las denominaciones reales (resumidas) distintas a efectivo _
                                                se buscan las transacciones segun el factor mas reciente. Si el total de _
                                                una linea de denominacion no est� disponible en un factor, se parte la _
                                                denominacion y se utiliza el monto restante del factor que se este recorriendo. _
                                                Posteriormente el faltante de la linea se sigue llenando con la cantidad disponible _
                                                en otros factores. A diferencia del efectivo aqui partiremos (alteraremos el valor) _
                                                de la linea para dividirlo en tantas lineas sea necesario segun factores haya para _
                                                cuadrar los montos. Si llega a darse el caso de que al valor de la l�nea le quede un _
                                                sobrante y se agoten los factores, entonces el resto se ingresar� por el factor actual. _
                                                Aqui los factores se utilizan hasta que queden en cero antes de pasar al siguiente.
                                                
                                                Set FactorInfo = RestanteXFactor(TmpPK).Items()(0)
                                                
                                                If ValorLinea <= FactorInfo("CantRestante") Then
                                                    
                                                    TmpValor = RoundUp(ValorLinea / RxDenomina!Cantidad, 8)
                                                    
                                                    GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                                    Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                                    CDec(FactorInfo("Factor")), CDec(RxDenomina!Cantidad), _
                                                    TmpValor, CVar(RxDenomina!CodigoBanco)
                                                    
                                                    MarcarComoUtilizado RxDenomina!CodMoneda, TmpCodDenomina, _
                                                    CDec(FactorInfo("Factor")), ValorLinea
                                                    
                                                    FactorInfo("CantRestante") = _
                                                    RoundUp(FactorInfo("CantRestante") - ValorLinea, 8)
                                                    
                                                    mTotalRetiro = RoundUp(mTotalRetiro + _
                                                    (ValorLinea * FactorInfo("Factor")), 8)
                                                    
                                                    ValorLinea = 0
                                                    
                                                Else
                                                    
                                                    TmpValor = RoundUp(FactorInfo("CantRestante") / RxDenomina!Cantidad, 8)
                                                    
                                                    GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                                    Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                                    CDec(FactorInfo("Factor")), CDec(RxDenomina!Cantidad), _
                                                    TmpValor, CVar(RxDenomina!CodigoBanco)
                                                    
                                                    MarcarComoUtilizado CStr(RxDenomina!CodMoneda), TmpCodDenomina, _
                                                    CDec(FactorInfo("Factor")), CDec(FactorInfo("CantRestante"))
                                                    
                                                    ValorLinea = RoundUp(ValorLinea - FactorInfo("CantRestante"), 8)
                                                    
                                                    mTotalRetiro = RoundUp(mTotalRetiro + _
                                                    (FactorInfo("CantRestante") * FactorInfo("Factor")), 8)
                                                    
                                                    FactorInfo("CantRestante") = 0 'RoundUp(FactorInfo("CantRestante") _
                                                    - ValorLinea, 8)
                                                    
                                                End If
                                                
                                                If FactorInfo("CantRestante") <= 0 Then
                                                    RestanteXFactor(TmpPK).Remove FactorInfo("PK")
                                                End If
                                                
                                            End If
                                            
                                        Else
                                            
                                            ' Grabar con factor actual
                                            
                                            TmpValor = RoundUp(ValorLinea / RxDenomina!Cantidad, 8)
                                            
                                            GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                            Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                            CDec(RxDenomina!Factor), CDec(RxDenomina!Cantidad), _
                                            TmpValor, CVar(RxDenomina!CodigoBanco)
                                            
                                            mTotalRetiro = RoundUp(mTotalRetiro + _
                                            (ValorLinea * RxDenomina!Factor), 8)
                                            
                                            ValorLinea = 0
                                            
                                        End If
                                        
                                    Wend
                                    
                                Else
                                    
                                    ' No hay registros de factor.
                                    
                                    GrabarTRCierres_Real RsTrCierres, Trim(RxDenomina!CodMoneda), _
                                    Trim(RxDenomina!CodDenomina), Trim(RxDenomina!DesDenomina), _
                                    CDec(RxDenomina!Factor), CDec(RxDenomina!Cantidad), _
                                    CDec(RxDenomina!Valor), CVar(RxDenomina!CodigoBanco)
                                    
                                    mTotalRetiro = RoundUp(mTotalRetiro + _
                                    (ValorLinea * RxDenomina!Factor), 8)
                                    
                                End If
                                
                                RxDenomina.MoveNext
                                
                            Loop
                            
                        End If
                        
                        ' Nuevo algoritmo opcional. Distribucion autom�tica del Efectivo por factor.
                        ' Para solucionar el cuadre cuando se reciban monedas en multiples tasas en
                        ' un mismo turno.
                        
                        If POS_DeclararEfectivoDesgloseAutomatico > 0 Then
                            
                            Call Apertura_RecordsetC(RxMonedas)
                            
                            RxMonedas.Open _
                            "SELECT CodMoneda, SUM(Cantidad * Valor) AS Monto, " & vbNewLine & _
                            "Factor AS FactorActual " & vbNewLine & _
                            "FROM TR_DENOMINA_TEMP " & vbNewLine & _
                            "WHERE isNULL(CodigoBanco, '') = '' " & vbNewLine & _
                            "AND Cantidad > 0 " & vbNewLine & _
                            "AND Caja = '" & nCaja & "' " & vbNewLine & _
                            "AND Usuario = '" & LcCajero & "' " & vbNewLine & _
                            "AND Turno = " & CDec(Turno) & " " & vbNewLine & _
                            "GROUP BY CodMoneda, Factor " & vbNewLine, _
                            ConexionVAD20RedGrabar, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            Set RxMonedas.ActiveConnection = Nothing
                            
                            If Not RxMonedas.EOF Then
                            
                            While Not RxMonedas.EOF
                                
                                Dim mTotalMoneda As Double, mRestanteMoneda As Double
                                Dim mCantidadDeclarar As Double
                                
                                TmpPK = UCase(RxMonedas!CodMoneda & ";" & "Efectivo")
                                
                                mTotalMoneda = RxMonedas!Monto
                                mRestanteMoneda = mTotalMoneda
                                
                                If RestanteXFactor.Exists(TmpPK) Then
                                
                                For Each FactorInfo In AsEnumerable(RestanteXFactor(TmpPK).Items())
                                
                                If FactorInfo("CantRestante") > 0 Then
                                    
                                    Dim Asignaciones As Collection, Itm
                                    Dim TotalAsignado As Double
                                    
                                    mCantidadDeclarar = IIf(mRestanteMoneda < FactorInfo("CantRestante"), _
                                    mRestanteMoneda, FactorInfo("CantRestante"))
                                    
                                    AsignarEfectivoPorFactor FactorInfo("CodMoneda"), _
                                    mCantidadDeclarar, TotalAsignado, Asignaciones, _
                                    POS_DeclararEfectivoDesgloseAutomatico
                                    
                                    If Not Asignaciones Is Nothing Then
                                        
                                        For Each Itm In Asignaciones
                                            
                                            GrabarTRCierres_Real RsTrCierres, FactorInfo("CodMoneda"), _
                                            Itm(0), Itm(4), CDec(FactorInfo("Factor")), _
                                            Itm(2), Itm(1), vbNullString
                                            
                                            MarcarComoUtilizado FactorInfo("CodMoneda"), _
                                            FactorInfo("CodDenomina"), CDec(FactorInfo("Factor")), Itm(3)
                                            
                                            FactorInfo("CantRestante") = _
                                            RoundUp(FactorInfo("CantRestante") - Itm(3), 8)
                                            
                                            mRestanteMoneda = RoundUp(mRestanteMoneda - Itm(3), 8)
                                            
                                            mTotalRetiro = RoundUp(mTotalRetiro + _
                                            (Itm(3) * FactorInfo("Factor")), 8)
                                            
                                        Next
                                        
                                    End If
                                    
                                End If ' Cant > 0
                                
                                Next ' For Each
                                
                                End If ' Existe
                                
                                If mRestanteMoneda > 0 Then
                                    
                                    Dim RxFondo As ADODB.Recordset
                                    
                                    Set RxFondo = New ADODB.Recordset
                                    
                                    Dim mFondoTotal As Double, mFondoDeclarado As Double, _
                                    mFactorFondo As Double, mFondoDeclarar As Double
                                    
                                    RxFondo.CursorLocation = adUseClient
                                    
                                    Sql = _
                                    "SELECT isNULL(SUM(FondoTurno), 0) AS FondoTurno, " & _
                                    "isNULL(SUM(n_Factor), 0) AS FactorInicio, " & _
                                    "isNULL(SUM(FondoDeclarado), 0) AS FondoDeclarado " & _
                                    "FROM (" & vbNewLine & _
                                    "SELECT n_MontoFondo AS FondoTurno, n_Factor, 0 AS FondoDeclarado " & _
                                    "FROM TR_CAJA_FONDO " & _
                                    "WHERE c_CodMoneda = '" & RxMonedas!CodMoneda & "' " & _
                                    "AND c_CodDenomina = 'Efectivo' " & vbNewLine & _
                                    "AND c_CodCaja = '" & nCaja & "' " & _
                                    "AND c_Cajero = '" & LcCajero & "' " & _
                                    "AND Turno = '" & CDec(Turno) & "' "
                                    
                                    Sql = Sql & _
                                    "UNION ALL " & vbNewLine & _
                                    "SELECT 0 AS FondoTurno, 0 AS n_Factor, SUM(T.n_Valor) AS FondoDeclarado " & _
                                    "FROM TR_CIERRES T " & _
                                    "INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES DEN " & _
                                    "ON T.c_CodMoneda = DEN.c_CodMoneda " & _
                                    "AND T.c_CodDenomina = DEN.c_CodDenomina " & _
                                    "AND DEN.c_Real = 1 " & _
                                    "AND DEN.n_Valor > 0 " & _
                                    "WHERE T.c_Concepto = 'CIP' " & _
                                    "AND T.c_CodMoneda = '" & RxMonedas!CodMoneda & "' " & _
                                    "AND T.bFondoCaja = 1 " & _
                                    "AND T.c_CodCajero = '" & LcCajero & "' " & _
                                    "AND T.Turno = " & CDec(Turno) & " " & vbNewLine & _
                                    ") TBDatos "
                                    
                                    RxFondo.Open Sql, ConexionVAD20RedGrabar, _
                                    adOpenForwardOnly, adLockReadOnly, adCmdText
                                    
                                    RxFondo.ActiveConnection = Nothing
                                    
                                    If Not RxFondo.EOF Then
                                        mFondoTotal = RxFondo!FondoTurno
                                        mFondoDeclarado = RxFondo!FondoDeclarado
                                        mFactorFondo = RxFondo!FactorInicio
                                    Else
                                        mFondoTotal = 0
                                        mFondoDeclarado = 0
                                    End If
                                    
                                    RxFondo.Close
                                    
                                    mFondoDeclarar = RoundUp(mFondoTotal - mFondoDeclarado, 8)
                                    
                                    mFondoDeclarar = IIf(CDec(mFondoDeclarar) < CDec(mRestanteMoneda), _
                                    mFondoDeclarar, mRestanteMoneda)
                                    
                                    If mFondoDeclarar < 0 Then
                                        mFondoDeclarar = 0
                                    End If
                                    
                                    If mFondoDeclarar > 0 Then
                                        
                                        AsignarEfectivoPorFactor RxMonedas!CodMoneda, _
                                        mFondoDeclarar, TotalAsignado, Asignaciones, _
                                        POS_DeclararEfectivoDesgloseAutomatico
                                        
                                        If Not Asignaciones Is Nothing Then
                                            
                                            For Each Itm In Asignaciones
                                                
                                                GrabarTRCierres_Real RsTrCierres, RxMonedas!CodMoneda, _
                                                Itm(0), Itm(4), mFactorFondo, _
                                                Itm(2), Itm(1), vbNullString, True
                                                
                                                mRestanteMoneda = RoundUp(mRestanteMoneda - Itm(3), 8)
                                                
                                                mTotalRetiro = RoundUp(mTotalRetiro + _
                                                (Itm(3) * mFactorFondo), 8)
                                                
                                            Next
                                            
                                        End If
                                        
                                    End If
                                    
                                    If mRestanteMoneda > 0 Then
                                        
                                        AsignarEfectivoPorFactor RxMonedas!CodMoneda, _
                                        mRestanteMoneda, TotalAsignado, Asignaciones, _
                                        POS_DeclararEfectivoDesgloseAutomatico
                                        
                                        If Not Asignaciones Is Nothing Then
                                            
                                            For Each Itm In Asignaciones
                                                
                                                GrabarTRCierres_Real RsTrCierres, RxMonedas!CodMoneda, _
                                                Itm(0), Itm(4), CDec(RxMonedas!FactorActual), _
                                                Itm(2), Itm(1), vbNullString
                                                
                                                mRestanteMoneda = RoundUp(mRestanteMoneda - Itm(3), 8)
                                                
                                                mTotalRetiro = RoundUp(mTotalRetiro + _
                                                (Itm(3) * CDec(RxMonedas!FactorActual)), 8)
                                                
                                            Next
                                            
                                        End If
                                        
                                    End If
                                    
                                End If
                                
                                RxMonedas.MoveNext
                                
                            Wend
                            
                            End If
                            
                            RxMonedas.Close
                            
                        End If
                        
                        '*******************************************************
                        '* BUSCA EN EL MAESTRO DETALLE PAGO TODOS LAS TRANSACCIONES
                        '* QUE FUERON SELECCIONADAS
                        '*******************************************************
                        
                        Call Apertura_Recordset(RxDenomina)
                        
                        Set RxDenomina = New ADODB.Recordset
                        RxDenomina.CursorLocation = adUseServer
                        
                        ' SE ORDENA POR DENOMINACION PARA CUANDO SE IMPRIMA EL CIERRE ORDENARLO POR ID
                        
                        RxDenomina.Open _
                        "SELECT * FROM MA_DETALLEPAGO " & _
                        "WHERE c_Util = 'X' " & _
                        "AND c_Caja = '" & Trim(no_caja.Caption) & "' " & _
                        "AND c_CodCajero = '" & LcCajero & "' " & _
                        "AND Turno = " & Turno & " " & _
                        "ORDER BY c_CodDenominacion, n_Monto DESC ", _
                        ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                        
                        If Not RxDenomina.EOF Then
                            
                            Do Until RxDenomina.EOF
                                
                                Dim pIDLoteAutomatico As String
                                
                                If ExisteCampoTabla("c_ID_Lote_Automatico", RxDenomina) Then
                                    pIDLoteAutomatico = RxDenomina!c_ID_Lote_Automatico
                                End If
                                
                                GrabarTRCierres_Detallado RsTrCierres, Trim(RxDenomina!c_CodMoneda), _
                                Trim(RxDenomina!c_CodDenominacion), vbNullString, _
                                CDec(RxDenomina!n_Factor), (CDec(RxDenomina!n_Monto) / CDec(RxDenomina!n_Factor)), _
                                CVar(RxDenomina!c_CodBanco), Trim(RxDenomina!c_Numero), pIDLoteAutomatico
                                
                                mTotalRetiro = RoundUp(mTotalRetiro + CDec(RxDenomina!n_Monto), 8)
                                
                                '*******************************************************
                                '* CAMBIA EL STATUS DEL REGISTRO PASANDOLO A REGISTRO
                                '* SELECCIONADO "T"
                                '*******************************************************
                                
                                'RxDenomina.update
                                RxDenomina!c_Util = "T"
                                RxDenomina!n_Falta = 0
                                
                                RxDenomina.UpdateBatch
                                
                                RxDenomina.MoveNext
                                
                            Loop
                            
                        End If
                        
                    End With
                    
                    RxDenomina.Close
                    
                    '*
                    '* Actualiza la tabla ma_detallepago en la BDD de Srv_Remote_BD_POS
                    '*
                    
                    RANGO = Fecha()
                    
                    ' A CONTINUACI�N COMENTAR EL SIGUIENTE C�DIGO. Trasladado al m�todo _
                    MarcarComoUtilizado() para uso general en todas las denominaciones _
                    separado por moneda, denominacion y factor. Este m�todo fue utilizado _
                    al inicio de la rutina de grabar cuando se empieza a distribuir el monto _
                    declarado segun el factor.
                    
                    'If RxDenomina.State = adStateOpen Then RxDenomina.Close
                    
                    'RxDenomina.CursorLocation = adUseServer
                    
                    'SQL = "SELECT * FROM MA_DETALLEPAGO " & _
                    "WHERE c_Util = 'F' " & _
                    "AND Turno = " & Turno & " " & _
                    "AND c_Caja = '" & no_Caja & "' " & _
                    "AND n_Falta <> 0 " & _
                    "AND c_CodDenominacion = 'Efectivo' " & _
                    "AND c_CodCajero = '" & lcCajero & "' " & _
                    "ORDER BY n_Falta "
                    
                    'RxDenomina.Open SQL, ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                    
                    'If Not RxDenomina.EOF Then
                        
                        'bs_Efectivo = 0
                        
                        'For i = 1 To Monitor.Rows - 1
                            'Monitor.Row = i
                            ''Monitor.Col = 2
                            '' 2018-05-24. Comparaba por descripcion de denominaci�n. Debe ser por CODIGO!
                            'Monitor.Col = 6
                            'If UCase(Monitor.Text) = UCase("Efectivo") Then
                                'Monitor.Col = 4
                                ''bs_efectivo = bs_efectivo + CDbl(Monitor.Text)
                                'bs_Efectivo = RoundUp(bs_Efectivo + _
                                (CDbl(Monitor.Text) * CDbl(Monitor.TextMatrix(i, 1))), 8)
                            'End If
                        'Next
                        
                        'Do Until RxDenomina.EOF()
                            
                            '' Esta parte recorre todos los pagos en efectivo y les va
                            '' calculando el faltante hasta que se acabe el efectivo declarado total.
                            '' Cuando ya no hay mas efectivo declarado empiezan a quedar faltantes.
                            
                            'If bs_Efectivo >= RxDenomina!n_Falta Then
                                 ''RxDenomina.Close
                                 'RxDenomina.update
                                 'RxDenomina!c_Util = "T"
                                 'bs_Efectivo = RoundUp(bs_Efectivo - RxDenomina!n_Falta, 8)
                                 'RxDenomina!n_Falta = 0
                                 'RxDenomina.UpdateBatch
                            'Else
                                ''Valor = 0
                                'Valor = RxDenomina!n_Falta
                                'RxDenomina.update
                                    'RxDenomina!n_Falta = RoundUp(RxDenomina!n_Falta - bs_Efectivo, 8)
                                    'bs_Efectivo = RoundUp(bs_Efectivo - Valor, 8)
                                'RxDenomina.UpdateBatch
                            'End If
                            
                            'RxDenomina.MoveNext
                            
                            'If bs_Efectivo <= 0 Then Exit Do
                            'If RxDenomina.EOF() Then Exit Do
                            
                        'Loop
                        
                        ''RxDenomina.UpdateBatch
    
                    'End If
                    
                    Call Apertura_Recordset(RsEureka)
                    
                    RsEureka.Open _
                    "SELECT * FROM TR_CAJA " & _
                    "WHERE c_Codigo = '" & no_caja.Caption & "' " & _
                    "AND c_Cajero = '" & LcCajero & "' " & _
                    "AND Turno = " & CDec(Turno) & " ", _
                    ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                    
                    If Not RsEureka.EOF Then
                        RsEureka.Update
                        RsEureka!n_Vendido = RoundUp(RsEureka!n_Vendido - mTotalRetiro, 8)
                        RsEureka!n_Cortes = RoundUp(RsEureka!n_Cortes + 1, 8)
                        RsEureka.UpdateBatch
                    End If
                    
                    glGrabar = "insert"
                    
                    ''****************************************************
                    ''* Graba el Maestro del Cierre Parcial
                    ''****************************************************
                    
                    Call Apertura_Recordset(RsCierres)
                    
                    RsCierres.Open "SELECT * FROM MA_CIERRES WHERE 1 = 2", _
                    ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                    
                    With RsCierres
                        
                        .AddNew
                        
                        !c_Documento = LcConsecu
                        
                        '!c_CodLocalidad = lcLocalidad_user
                        !c_CodLocalidad = Localidad
                        
                        !c_CodCaja = no_caja.Caption
                        !c_Des_Cajera = Trim(cajero.Caption)
                        !c_CodCajero = LcCajero
                        
                        !d_Fecha = FechaBD(Now, , True) ' Date
                        
                        'Debug.Print bs_recibidos.Caption
                        
                        'unitario = RoundUp(CDbl(bs_recibidos.Caption), 2)
                        !n_BsRecibidos = mTotalRetiro   'unitario
                        
                        !c_Concepto = "CIP"
                        
                        'unitario = RoundUp(CDbl(diferencia.Caption), 2)
                        !n_Diferencia = mTotalRetiro   'unitario
                        
                        !n_Imp_Devolucion = 0
                        !c_CodUsuario = LcCajero
                        !c_Num_Cierre = "0"
                        !Turno = Turno
                        
                        .UpdateBatch
                        
                    End With
                    
                    'Limpia la tabla temporal de las denominaciones de efectivo.
                    
                    ' Comentado. Esto es ineficiente.
                    
                    'Call Apertura_Recordset(RxDenomina)
                    
                    'If ExisteCampoTabla("Usuario", , "SELECT Usuario from TR_DENOMINA_TEMP", , ConexionVAD20RedGrabar) Then
                        'RxDenomina.Open "SELECT * FROM TR_DENOMINA_TEMP " & _
                        '"WHERE Caja = '" & Me.no_Caja.Caption & "' " & _
                        '"AND Usuario = '" & LCCAJERO & "' ", _
                        'ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic
                    'Else
                        'RxDenomina.Open "SELECT * FROM TR_DENOMINA_TEMP " & _
                        '"WHERE Caja = '" & Me.no_Caja.Caption & "'", _
                        'ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic
                    'End If
                    
                    'If Not RxDenomina.EOF Then
                        'Do Until RxDenomina.EOF
                            'RxDenomina.delete
                            'RxDenomina.UpdateBatch
                            'RxDenomina.MoveNext
                        'Loop
                    'End If
                    
                    ' Metodo Simple y r�pido:
                    
                    ConexionVAD20RedGrabar.Execute _
                    "DELETE FROM TR_DENOMINA_TEMP " & _
                    "WHERE Caja = '" & Me.no_caja.Caption & "' " & _
                    "AND Usuario = '" & LcCajero & "' " & _
                    "AND Turno = " & CDec(Turno) & " "
                    
                    '
                    
                ConexionVAD20RedGrabar.CommitTrans
                
                mInicio = False
                
                '******** IMPRESION ****
                
                NumDoc = LcConsecu
                TipoCon = "RETIRO PARCIAL"
                LcDesCajero = cajero.Caption
                Titulo = no_caja.Caption
                
                MontoGr = mTotalRetiro 'bs_recibidos.Caption
                
                Call Imprimir_Retiro("ORIGINAL")
                
                If mVarImprimirCopia Then
                    
                    EsperaManualMillis 1000
                    
                    '******** IMPRESION ****
                    
                    NumDoc = LcConsecu
                    TipoCon = "RETIRO PARCIAL"
                    LcDesCajero = cajero.Caption
                    Titulo = no_caja.Caption
                    
                    MontoGr = mTotalRetiro 'bs_recibidos.Caption
                    
                    Call Imprimir_Retiro("COPIA")
                    
                End If
                
                Screen.MousePointer = 0
                
                INICIO_TRANSACCION = False
                
                LcGrabar = True
                
                Unload Me
                
            Else
                Mensaje True, "No ha realizado la Transacci�n."
            End If
            
        ' Salir
        Case Is = vbKeyF12
            
            If CDec(bs_recibidos) <> 0 Then
                If Mensaje(False, "Al Salir los cambios realizados no ser�n grabados." & Chr(13) & Chr(13) & _
                "�Est� seguro de salir?") Then
                    LcGrabar = False
                Else
                    LcGrabar = True
                End If
            End If
            
            Unload Me
            
    End Select
    
    Exit Sub
    
Errores:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If mInicio Then
        ConexionVAD20RedGrabar.RollbackTrans
        mInicio = False
    End If
    
    Screen.MousePointer = vbDefault
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Ficha_Cierre_Parcial_Grabar)"
    
End Sub

Private Sub Form_Load()
    
    If Not ValidarConexion(ConexionVAD10Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD20Red)
    
    gCambioDenomina = False
    RESP = Buscar_Moneda(, , , True, , True, False)
    Titulo = "Retiro Parcial"
    Cant = Me.Width / (Len(Titulo) + Len(Stellar))
    Cant = Cant / 4
    
    'Me.Caption = STELLAR & Space(CANT) & titulo
    '***** TEXTOS***************
    
    Me.lbl_Organizacion.Caption = Titulo
    Me.Toolbar1.Buttons(1).Caption = "F4" & " " & Stellar_Mensaje(103, True) 'grabar
    Me.Toolbar1.Buttons(3).Caption = "F12" & " " & Stellar_Mensaje(54, True) 'salir
    Me.Toolbar1.Buttons(4).Caption = Stellar_Mensaje(7, True) 'ayuda
    Me.Toolbar1.Buttons(6).Caption = Stellar_Mensaje(16040, True) 'teclado
    
    Me.Label3.Caption = Stellar_Mensaje(16041, True) 'retiro parcial n�
    Me.Label6.Caption = Stellar_Mensaje(128, True) 'fecha
    
    Me.Label2.Caption = Stellar_Mensaje(2017, True) 'caja n�
    Me.Label5.Caption = Stellar_Mensaje(16042, True) ' Total Retiro
    
    '****************************
    
    LcGrabar = False
    bs_recibidos = FormatNumber(0, Std_Decm)
    Bs_Diferencia = FormatNumber(0, Std_Decm)
    diferencia = FormatNumber(0, Std_Decm)
    lbl_consecutivo = Format(ConsecutivoCierre, "00000000#")
    
    If lbl_consecutivo = Empty Then
        Unload Me
    End If
    
    lbl_fecha = Date
    Caja = NumDoc
    no_caja.Caption = Caja
    cajero.Caption = LcDesCajero
    
    Call Cargar_Monitor
    
    If Not Monitor_Parcial Then
        Exit Sub
    End If
    
    monitor.Col = 0
    Set Forma = Ficha_Cierre_parcial
    
End Sub

Private Function ConsecutivoCierre() As String
    
    On Error GoTo ErrorConsecutivo
    
    Dim Sql As String, Rs As New ADODB.Recordset
    
    Sql = "SELECT cierre_par From MA_CONSECUTIVOS "
    
    Rs.Open Sql, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not Rs.EOF Then
        ConsecutivoCierre = Rs!cierre_par
    Else
        ConsecutivoCierre = Empty
    End If
    
    Exit Function
    
ErrorConsecutivo:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(ConsecutivoCierre)"
    
End Function

Private Function Buscar_Moneda(Optional campo2 As Control, Optional campo3 As Control, Optional campo4 As Control, Optional prede As Boolean, Optional val_moneda As String, Optional Asignar As Boolean = False, Optional OBJETOS As Boolean = False) As Boolean
    
    Dim rc As New ADODB.Recordset
    
    'Call apertura_recordsetc(rc)
    If prede = True Then
        rc.Open "select * from ma_monedas where b_preferencia = 1", _
        ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    Else
        rc.Open "select * from ma_monedas where c_codmoneda = '" & val_moneda & "'", _
        ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    End If
    
    If Not rc.EOF Then
        If OBJETOS = True Then
            campo2 = rc!c_CodMoneda
            campo3 = rc!c_Descripcion
            campo4 = FormatNumber(rc!n_Factor, rc!n_Decimales)
        Else
            Moneda_Cod = rc!c_CodMoneda
            Moneda_Des = rc!c_Descripcion
            Moneda_Fac = FormatNumber(rc!n_Factor, rc!n_Decimales)
        End If
        If Asignar = True Then Std_Decm = rc!n_Decimales
        Buscar_Moneda = True
    Else
        Std_Decm = 2
        Moneda_Fac = 1
        Buscar_Moneda = False
    End If
    
    'Call Cerrar_Recordset(rc)
    rc.Close
    
End Function

Private Sub moneda_KeyPress(KeyAscii As Integer)
    If KeyAscii = 0 Then
        KeyAscii = 0
        oTeclado.Key_Tab
    End If
End Sub

Private Sub monitor_DblClick()
    
    With monitor
        
        If Trim(.TextMatrix(.Row, 0)) <> Empty _
        And Trim(.TextMatrix(.Row, 1)) <> Empty Then
            
            .Col = 0
                Moneda_Des = .Text
            .Col = 1
                Moneda_Fac = .Text
            .Col = 2
                Denomina_Des = .Text
            .Col = 5
                Moneda_Cod = .Text
                Moneda_Sel = .Text
            .Col = 6
                Denomina_Cod = .Text
                Denominacion_Sel = .Text
            .Col = 7
                Denomina_Real = .Text
                
                If .Text = "" Then Exit Sub
                
                nCaja = Me.no_caja.Caption
                
                If UCase(Denomina_Cod) <> UCase("Efectivo") _
                And Not CBool(.TextMatrix(.Row, 7)) Then
                    
                    Dat_Nom_Caja.Caption = Denomina_Des
                    Dat_Nom_Caja.Show vbModal
                    
                    If gCambioDenomina Then
                        Cargar_Monitor
                        gCambioDenomina = False
                    End If
                    
                ElseIf UCase(Denomina_Cod) <> UCase("Efectivo") _
                And CBool(.TextMatrix(.Row, 7)) Then
                    
                    Lista_Denominacionreal.Show vbModal
                    
                Else
                    
                    If POS_IngresarEfectivoMontoDirecto <= 0 Then
                        
                        lista_denominaciones.Show vbModal
                        
                    Else
                        
                        '"Ingrese el monto de efectivo      Escriba el monto
                        mValorEfec = QuickInputRequest(StellarMensaje(3180) & ".", True, , _
                        IIf(CDbl(.TextMatrix(.Row, 4)) <= 0, .TextMatrix(.Row, 3), .TextMatrix(.Row, 4)), _
                        StellarMensaje(3181), , , , , , False, True)
                        
                        If Not IsNumeric(mValorEfec) Then
                            Exit Sub
                        ElseIf CDbl(mValorEfec) < 0 Then
                            Exit Sub
                        End If
                        
                        mValorEfecNew = AutoAsignarEfectivo(Moneda_Sel, _
                        mValorEfec, POS_IngresarEfectivoMontoDirecto)
                        
                        .TextMatrix(.Row, 4) = FormatNumber(mValorEfecNew, 2)
                        .TextMatrix(.Row, 8) = FormatNumber(mValorEfecNew - CDec(.TextMatrix(.Row, 3)), 2)
                        
                        If CDec(mValorEfecNew) <> CDec(mValorEfec) Then
                            ActivarMensajeGrande 50
                            '"El monto ingresado (" & FormatoDecimalesDinamicos(mValorEfec) & ") es imposible de establecer _
                            con la plantilla de billetes actual. El valor ingresado fue ajustado al monto mas cercano: " & _
                            FormatoDecimalesDinamicos(mValorEfecNew) & "" & vbNewLine & vbNewLine & _
                            "Para mayor precisi�n ajuste la configuraci�n de billetes para esta moneda."
                            Mensaje True, Replace(Replace(Replace(StellarMensaje(3182), _
                            "$(Input)", "(" & FormatoDecimalesDinamicos(mValorEfec) & ")"), _
                            "$(Result)", FormatoDecimalesDinamicos(mValorEfecNew)), _
                            "$(Line)", vbNewLine), vbOKOnly, ""
                        End If
                        
                        Calculo
                        
                    End If
                    
                End If
                
        End If
        
    End With
    
End Sub

Private Sub monitor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call monitor_DblClick
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        
        Case Is = "Grabar"
            Call Form_KeyDown(vbKeyF4, 1)
            
        Case Is = "Salir"
            Call Form_KeyDown(vbKeyF12, 1)
             
        Case Is = "Ayuda"
        
        Case Is = "Teclado"
            Tmp = Me.monitor.Text
            Set CampoT = monitor
            'configPosTecladoManual
            'TECLADO.Show vbModal
            TecladoPOS CampoT
            Me.monitor.Text = Tmp
            
    End Select
    
End Sub

Private Function Monitor_Parcial() As Boolean
    
    On Error GoTo Errores
    
    Call Cargar_Grid
    
    Monitor_Parcial = True
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Function
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    If Not RsMonitor.EOF Then
        
        RsMonitor.MoveFirst
        
        numrow = 1
        
        EfectivoRec = FormatNumber(0, Std_Decm)
        
        Do Until RsMonitor.EOF()
            
            With monitor
                
                .Row = numrow
                .Col = 0
                    
                    Call Apertura_Recordset(RxMonedas)
                    
                    RxMonedas.Open "select * from ma_monedas " & _
                    "where c_codmoneda = '" & RsMonitor!c_CodMoneda & "' ", _
                    ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    Moneda_Sel = RxMonedas!c_CodMoneda
                    
                    .Text = RxMonedas!c_Descripcion
                
                .Col = 1
                    
                    .Text = FormatNumber(RsMonitor!n_Factor, RxMonedas!n_Decimales)
                    .CellAlignment = 7
                
                .Col = 2
                    
                    If UCase(RsMonitor!c_CodDenominacion) <> UCase("Efectivo") Then
                        
                        ' octavio 10/09/2004
                        
                        Call Apertura_Recordset(RxDenomina)
                        
                        RxDenomina.Open "select * from ma_denominaciones " & _
                        "where c_coddenomina = '" & RsMonitor!c_CodDenominacion & "' " & _
                        "and c_codmoneda = '" & RsMonitor!c_CodMoneda & "' ", _
                        ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                        
                        If Not RxDenomina.EOF Then
                            .Text = RxDenomina!c_Denominacion
                            .Col = 7
                                .Text = IIf(RxDenomina!c_Real, 1, 0)
                        Else
                            'MsgBox ("No existen datos relacionados en el sistema.")
                            Mensaje True, Stellar_Mensaje(16044, True)
                            Exit Do
                        End If
                        
                    Else
                        
                        .Text = "Efectivo"
                        
                        If Not Grabar_Denominaciones Then
                            GoTo NoTraeDenominacion
                        End If
                        
                        .Col = 7
                        .Text = "1"
                        
                        EfectivoRec = FormatNumber(CDec(EfectivoRec) + RsMonitor!Monto, 2)
                        
                    End If
                    
                .Col = 3
                    
                    If UCase(RsMonitor!c_CodDenominacion) <> UCase("Efectivo") Then
                        .Text = FormatNumber(RsMonitor!Monto, 2)
                    Else
                        .Text = FormatNumber(0, 2)
                    End If
                    
                    .CellAlignment = 7
                    MontoLn = MontoLn + RsMonitor!Monto
                    MontoGr = MontoGr + RsMonitor!Monto
                    
                .Col = 4
                    
                    .Text = FormatNumber(0, 2)
                    
                .Col = 5
                    
                    .Text = RsMonitor!c_CodMoneda
                    Moneda_Sel = RsMonitor!c_CodMoneda
                    
                .Col = 6
                    
                    .Text = IIf(IsNull(RsMonitor!c_CodDenominacion), "Efectivo", RsMonitor!c_CodDenominacion)
                    .ColAlignment(6) = 1
                    
                .Col = 8
                    
                    If UCase(RsMonitor!c_CodDenominacion) <> UCase("Efectivo") Then
                        .Text = FormatNumber(RsMonitor!Monto * -1, 2)
                    Else
                        .Text = FormatNumber(0, 2)
                    End If
                    
                    .CellForeColor = rojo
                    
                RsMonitor.MoveNext
                
                monitor.Rows = monitor.Rows + 1
                
                numrow = numrow + 1
                
            End With
            
        Loop
        
        Call Cerrar_Recordset(RsMonitor)
        
        monitor.Rows = monitor.Rows - 1
        monitor.Row = 1
        
        monitor.Col = 3
        'monitor.Sort = 7
        EfectivoRec = FormatNumber(CDbl(EfectivoRec) - Devolucion, 2)
        
        Call Calculo
        
        monitor.Row = 1
        
        oTeclado.Key_Right
        
    Else
        
Errores:
        
        Mensaje True, "No hay denominaciones recibidas para realizar un Retiro." & vbNewLine & _
        "A continuaci�n el formulario se cerrar�."
        
        GoTo NoTraeDenominacion
        
    End If
    
    Exit Function
    
NoTraeDenominacion:
    
    Monitor_Parcial = False
    
End Function

Private Sub Cargar_Monitor()
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Dim mSQL As String
    
    bs_vendido = FormatNumber(0, Std_Decm)
    
    ConexionVAD20Red.Execute _
    "UPDATE MA_DETALLEPAGO SET " & _
    "c_Util = 'F', " & _
    "n_Falta = n_Monto " & _
    "WHERE c_Util = 'X' " & _
    "AND Turno = " & Turno & " " & _
    "AND c_Caja = '" & Caja & "' " & _
    "AND c_CodCajero = '" & LcCajero & "' "
    
    ' * (" & BuscarFactorMonedaPredeterminada & "/n_factor)
    
    'mSql = " Select c_caja, c_codmoneda, c_coddenominacion, n_factor, sum(n_FALTA) / n_Factor as monto from MA_DETALLEPAGO " _
         '& " WHERE (C_UTIL = 'F' OR C_UTIL = 'X') AND turno = " & turno & " and c_caja = '" & caja & "' and c_codcajero = '" & lccajero & "' " _
         '& " group by c_caja,c_codmoneda,c_coddenominacion,n_factor "   'order by c_caja
    
    ' OR c_Util = 'T' ' Traer solo lo que falta.
    
    Dim mCampoMonto As String
    
    'mCampoMonto = "(SUM(ROUND(CAST(n_Falta AS MONEY), " & Std_Decm & ", 0)) / n_Factor)"
    mCampoMonto = "(SUM(n_Falta) / n_Factor)"
    
    mSQL = " SELECT 1 AS Orden, c_Caja, c_CodMoneda, c_CodDenominacion, n_Factor, " & _
    "" & mCampoMonto & " AS Monto FROM MA_DETALLEPAGO " & _
    "WHERE (c_Util = 'F') " & _
    "AND Turno = " & Turno & " AND c_Caja = '" & Caja & "' " & _
    "AND c_CodCajero = '" & LcCajero & "' " & _
    "GROUP BY c_Caja, c_CodMoneda, c_CodDenominacion, n_Factor "   'order by c_Caja
    
    mSQL = mSQL & " UNION ALL " & vbNewLine & _
    "SELECT 2 AS Orden, '" & Caja & "' AS c_Caja, D.c_CodMoneda, " & _
    "D.c_CodDenomina AS c_CodDenominacion, M.n_Factor, 0 AS Monto " & _
    "FROM " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES D " & _
    "INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = d.c_CodMoneda " & vbNewLine & _
    "--AND M.b_Preferencia = 1 " & vbNewLine & _
    "WHERE D.c_Real = 1 AND D.n_Valor = 0 AND c_POS = 1"
    
    mSQL = mSQL & " UNION ALL " & vbNewLine & _
    "SELECT 0 AS Orden, '" & Caja & "' AS c_Caja, D.c_CodMoneda, " & _
    "D.c_CodDenomina AS c_CodDenominacion, M.n_Factor, 0 AS Monto " & _
    "FROM " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES D " & _
    "INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = d.c_CodMoneda " & vbNewLine & _
    "--AND M.b_Preferencia = 1 " & vbNewLine & _
    "WHERE D.c_CodDenomina = 'Efectivo'"
    
    'mSql = "Select c_caja,m.c_codmoneda,m.n_factor as n_factor,c_coddenominacion,sum(monto) as monto from (" & mSql & ") Tb " _
         & "left join " & Srv_Remote_BD_ADM & ".dbo.ma_monedas m on m.c_codmoneda=tb.c_codmoneda " _
         & "group by c_caja,m.c_codmoneda,m.n_factor,c_coddenominacion order by c_caja "
    
    mSQL = "SELECT TB.c_Caja, TB.c_CodMoneda, TB.c_CodDenominacion, " & _
    "M.n_Factor, SUM(TB.Monto) AS Monto FROM (" & mSQL & ") TB " & _
    "LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = TB.c_CodMoneda " & _
    "GROUP BY TB.c_Caja, TB.c_CodMoneda, TB.c_CodDenominacion, M.n_Factor " & _
    "ORDER BY TB.c_CodMoneda, MIN(TB.Orden), TB.c_CodDenominacion "
    
    Call Apertura_Recordset(RsMonitor)
    
    RsMonitor.Open mSQL, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsMonitor.EOF Then
        
        Do Until RsMonitor.EOF
            bs_vendido = FormatNumber(CDbl(bs_vendido) + _
            (RsMonitor!Monto * RsMonitor!n_Factor), Std_Decm)
            RsMonitor.MoveNext
        Loop
        
        RsMonitor.MoveFirst
        
        bs_vendido = FormatNumber(CDbl(bs_vendido), Std_Decm)
        
    End If
    
End Sub

Public Sub Calculo()
    
    Monto = CDec(0)
    montor = CDec(0)
    
    With monitor
        
        OriginalRow = .Row
        
        For i = 1 To .Rows - 1
            
            .Row = i
            
            If Not IsNumeric(.TextMatrix(i, 1)) Then
                .TextMatrix(i, 1) = 1
            End If
            
            .Col = 4
                montor = montor + (CDec(.Text) * CDec(.TextMatrix(i, 1)))
            .Col = 8
                Monto = Monto + (CDec(.Text) * CDec(.TextMatrix(i, 1)))
                
        Next
        
        bs_recibidos = FormatNumber(montor, 2)
        diferencia = FormatNumber(Monto, 2)
        
        If Monto < 0 Then
            diferencia.ForeColor = rojo
        Else
            diferencia.ForeColor = azul
        End If
        
        .Row = OriginalRow
        .Col = 0
        
        oTeclado.Key_Right
        
    End With
    
End Sub

Private Sub Cargar_Grid()
    
    monitor.Rows = 2
    
    monitor.RowHeightMin = 720
    
    'Call MSGridAsign(monitor, 0, 0, "MONEDA", 1400, flexAlignCenterCenter)
    'Call MSGridAsign(monitor, 0, 1, "FACTOR", 1000, flexAlignCenterCenter)
    'Call MSGridAsign(monitor, 0, 2, "DENOMINACI�N", 3000, flexAlignCenterCenter)
    'Call MSGridAsign(monitor, 0, 3, "TOTAL VENDIDO", 2500, flexAlignCenterCenter)
    'Call MSGridAsign(monitor, 0, 4, "TOTAL RECIBIDO", 2500, flexAlignCenterCenter)
    'Call MSGridAsign(monitor, 0, 5, "MONEDA", 0, flexAlignRightCenter)
    'Call MSGridAsign(monitor, 0, 6, "DENOMINACION", 0, flexAlignRightCenter)
    'Call MSGridAsign(monitor, 0, 7, "REAL", 0, flexAlignRightCenter)
    'Call MSGridAsign(monitor, 0, 8, "DIFERENCIA", 2500, flexAlignCenterCenter)
    
    Call MSGridAsign(monitor, 0, 0, UCase(Stellar_Mensaje(16045, True)), 3000, flexAlignCenterCenter)
    ' Ocultado el factor ya que no es relevante para el cierre. Los multiples factores se estaran manejando a lo interno.
    Call MSGridAsign(monitor, 0, 1, UCase(Stellar_Mensaje(16046, True)), 0, flexAlignCenterCenter)
    Call MSGridAsign(monitor, 0, 2, UCase(Stellar_Mensaje(16047, True)), 3500, flexAlignCenterCenter)
    Call MSGridAsign(monitor, 0, 3, UCase(Stellar_Mensaje(16048, True)), 2500, flexAlignCenterCenter)
    Call MSGridAsign(monitor, 0, 4, UCase(Stellar_Mensaje(16049, True)), 2500, flexAlignCenterCenter)
    Call MSGridAsign(monitor, 0, 5, "MONEDA", 0, flexAlignRightCenter)
    Call MSGridAsign(monitor, 0, 6, "DENOMINACION", 0, flexAlignRightCenter)
    Call MSGridAsign(monitor, 0, 7, "REAL", 0, flexAlignRightCenter)
    Call MSGridAsign(monitor, 0, 8, UCase(Stellar_Mensaje(16053, True)), 2500, flexAlignCenterCenter)
    
End Sub

Private Function Grabar_Denominaciones() As Boolean
    
    ConexionVAD20Red.Execute _
    "DELETE FROM TR_DENOMINA_TEMP " & _
    "WHERE CodMoneda = '" & Moneda_Sel & "' " & _
    "AND Caja = '" & nCaja & "' " & _
    "AND Usuario = '" & LcCajero & "' " & _
    "AND Turno = " & CDec(Turno) & " "
    
    Call Apertura_RecordsetC(Rec_Det_Den)
    
    Rec_Det_Den.Open _
    "SELECT * FROM TR_DENOMINA_TEMP " & _
    "WHERE CodMoneda = '" & Moneda_Sel & "' " & _
    "AND Caja = '" & nCaja & "' " & _
    "AND Usuario = '" & LcCajero & "' " & _
    "AND Turno = " & CDec(Turno) & " ", _
    ConexionVAD20Red, adOpenDynamic, adLockBatchOptimistic
    
    Call Apertura_RecordsetC(RxDenomina)
    
    RxDenomina.Open _
    "SELECT * FROM MA_DENOMINACIONES " & _
    "WHERE c_codmoneda = '" & Moneda_Sel & "' " & _
    "AND c_Real = 1 " & _
    "ORDER BY C_CODDENOMINA ", _
    ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set RxDenomina.ActiveConnection = Nothing
    
    If Not RxDenomina.EOF Then
        
        RxDenomina.MoveFirst
        
        Do Until RxDenomina.EOF
            
            With Rec_Det_Den
                
                .AddNew
                    
                    !CodMoneda = Moneda_Sel
                    !CodDenomina = Trim(RxDenomina!c_CodDenomina)
                    !DesDenomina = Trim(RxDenomina!c_Denominacion)
                    !Cantidad = 0
                    !Valor = RxDenomina!n_Valor
                    !Factor = RsMonitor!n_Factor
                    
                    !Caja = nCaja
                    !Usuario = LcCajero
                    !Turno = CDec(Turno)
                    
                .UpdateBatch
                
            End With
            
            RxDenomina.MoveNext
            
        Loop
        
        Grabar_Denominaciones = True
        
    Else
        
        'MsgBox ("No existen denominaciones para la moneda.")
        Mensaje True, Stellar_Mensaje(16050, True)
        'Ficha_Cierre_parcial = Nothing
        Grabar_Denominaciones = False
        Unload Me
        Exit Function
        
    End If
    
    Call Cerrar_Recordset(RxDenomina)
    Call Cerrar_Recordset(Rec_Det_Den)
    
End Function

Private Function BuscarDescripcionDenominaSRV(pCn As ADODB.Connection, _
pMoneda As String, pDenomina As String) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "Select c_denominacion " & _
    "from " & Srv_Remote_BD_ADM & ".dbo.ma_denominaciones " & _
    "where c_codmoneda = '" & pMoneda & "' " & _
    "and c_coddenomina = '" & pDenomina & "' "
    
    mRs.CursorLocation = adUseClient
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    
    If Not mRs.EOF Then
        BuscarDescripcionDenominaSRV = mRs!c_Denominacion
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Private Sub GrabarTRCierres_Real(ByRef RsTrCierres, ByVal CodMon, ByVal CodDen, _
ByVal Denominacion, ByVal Factor, ByVal Cantidad, ByVal Valor, ByVal CodigoBanco, _
Optional ByVal pEsFondoCaja As Boolean = False)
    
    With RsTrCierres
        
        .AddNew
            
            !c_Documento = LcConsecu
            !d_Fecha = FechaBD(Now, , True) ' Date
            
            '!c_CodLocalidad = lcLocalidad_user
            !c_CodLocalidad = Localidad
            
            !c_CodMoneda = CodMon
            !c_CodDenomina = CodDen
            !n_Factor = Factor
            !n_Cantidad = RoundUp(Cantidad, 8)
            !n_Falta = RoundUp(Cantidad, 8)
            !n_Valor = RoundUp(Valor, 8)
            
            'If Not IsNull(CodigoBanco) Then
                'If CodigoBanco <> "" Then
                    !c_CodBanco = CodigoBanco
                'End If
            'End If
            
            !n_Bolivares = RoundUp(Cantidad * Valor, Std_Decm)
            !c_Factura = "N"
            !c_Real = 1
            !c_Concepto = "CIP"
            !Turno = Turno
            !c_Cerrado = 0
            !n_Depositado = 0
            !c_Num_Cierre = "0"
            !c_CodCajero = LcCajero
            
            If ExisteCampoTabla("bFondoCaja", RsTrCierres) Then
                !bFondoCaja = pEsFondoCaja
            End If
            
        .UpdateBatch
        
    End With
                                
    '*******************************************************
    '* BUSCA EN EL MAESTRO DE CAJA SI EXISTE LA DENOMINACION
    '* SELECCIONADA SI NO SE ENCUENTRA ENTONCES LA CREA
    '*******************************************************
                                
    Call Apertura_Recordset(RsEureka)
    
    'RsEureka.CursorLocation = adUseClient
    
    RsEureka.Open _
    "SELECT * FROM MA_DENOMINACIONES " & _
    "WHERE c_CodMoneda = '" & CodMon & "' " & _
    "AND c_CodDenomina = '" & CodDen & "' ", _
    ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    'RsEureka.ActiveConnection = Nothing
    
    If Not RsEureka.EOF Then
        
        RsEureka.Update
        
        If CodigoBanco <> Empty Then
            RsEureka!n_Bolivares = RoundUp(RsEureka!n_Bolivares _
            + RoundUp((Cantidad * Valor), Std_Decm), 8)
        End If
        
        RsEureka!n_Cantidad = RoundUp(RsEureka!n_Cantidad + Cantidad, 8)
        RsEureka!n_Entrado = RoundUp(RsEureka!n_Entrado + Cantidad, 8)
        
        RsEureka.UpdateBatch
        
    Else
        
        RsEureka.AddNew
        
            RsEureka!c_CodMoneda = CodMon
            RsEureka!c_CodDenomina = CodDen
            RsEureka!c_Denominacion = Denominacion
            
            RsEureka!n_Factor = Factor
            RsEureka!n_Cantidad = RoundUp(Cantidad, 8)
            RsEureka!n_Entrado = RoundUp(Cantidad, 8)
            
            If CodigoBanco <> Empty Then
                RsEureka!n_Bolivares = RoundUp((Cantidad * Valor), Std_Decm)
                RsEureka!n_Valor = 0
            Else
                RsEureka!n_Valor = RoundUp(BuscarValorDenominaAdm( _
                CodMon, CodDen), 8)
                RsEureka!n_Bolivares = 0 ' Para que sea congruente con la parte del update....
            End If
            
            RsEureka!c_Real = 1 '"S"
            
        RsEureka.UpdateBatch
        
    End If
    
End Sub

Private Sub GrabarTRCierres_Detallado(ByRef RsTrCierres, ByVal CodMon, ByVal CodDen, _
ByVal Denominacion, ByVal Factor, ByVal Valor, ByVal CodigoBanco, ByVal Numero, _
Optional ByVal pIDLoteAutomatico As String)
    
    With RsTrCierres
        
        .AddNew
            
            !c_Documento = LcConsecu
            !d_Fecha = FechaBD(Now, , True) ' Date
            
            '!c_CodLocalidad = lcLocalidad_user
            !c_CodLocalidad = Localidad
            
            !c_CodMoneda = CodMon
            !c_CodDenomina = CodDen
            !n_Factor = Factor
            
            !n_Cantidad = 1
            !n_Falta = 1
            !n_Valor = 0
            
            'If Not IsNull(CodigoBanco) Then
                'If CodigoBanco <> "" Then
                    !c_CodBanco = CodigoBanco
                'End If
            'End If
            
            !c_TDC = Numero
            
            !n_Bolivares = RoundUp(Valor, Std_Decm)
            !c_Factura = "N"
            !c_Real = 0
            !c_Concepto = "CIP"
            !Turno = Turno
            !c_Cerrado = 0
            !n_Depositado = 0
            !c_Num_Cierre = "0"
            !c_CodCajero = LcCajero
            
            If ExisteCampoTabla("c_ID_Lote_AutoDeposito", RsTrCierres) Then
                !c_ID_Lote_AutoDeposito = pIDLoteAutomatico
            End If
            
        .UpdateBatch
        
    End With
    
    '*******************************************************
    '* BUSCA EN EL MAESTRO DE CAJA SI EXISTE LA DENOMINACION
    '* SELECCIONADA SI NO SE ENCUENTRA ENTONCES LA CREA
    '*******************************************************
    
    Call Apertura_Recordset(RsEureka)
    
    'RsEureka.CursorLocation = adUseClient
    
    RsEureka.Open _
    "SELECT * FROM MA_DENOMINACIONES " & _
    "WHERE c_CodMoneda = '" & CodMon & "' " & _
    "AND c_CodDenomina = '" & CodDen & "' ", _
    ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    'RsEureka.ActiveConnection = Nothing
    
    If Not RsEureka.EOF Then
        
        RsEureka.Update
        
        RsEureka!n_Cantidad = RoundUp(RsEureka!n_Cantidad + 1, 8)
        RsEureka!n_Entrado = RoundUp(RsEureka!n_Entrado + 1, 8)
        RsEureka!n_Bolivares = RoundUp(RsEureka!n_Bolivares + _
        RoundUp(Valor, Std_Decm), 8)
        
        RsEureka.UpdateBatch
        
    Else
        
        RsEureka.AddNew
            
            RsEureka!c_CodMoneda = CodMon
            RsEureka!c_CodDenomina = CodDen
            
            'RsEureka!c_Denominacion = Denominacion
            RsEureka!c_Denominacion = BuscarValorBD("Den", _
            "SELECT c_Denominacion AS Den " & vbNewLine & _
            "FROM MA_DENOMINACIONES " & vbNewLine & _
            "WHERE c_CodMoneda = '" & CodMon & "' " & vbNewLine & _
            "AND c_CodDenomina = '" & CodDen & "' " & vbNewLine)
            
            RsEureka!n_Factor = Factor
            RsEureka!n_Cantidad = 1
            RsEureka!n_Entrado = 1
            RsEureka!n_Bolivares = RoundUp(Valor, Std_Decm)
            RsEureka!n_Valor = 0
            RsEureka!c_Real = 0 '"N"
            
        RsEureka.UpdateBatch
        
    End If
    
End Sub

Private Sub MarcarComoUtilizado(ByVal CodMon As String, ByVal CodDen As String, _
ByVal Factor As Double, ByVal Valor As Double)
    
    Dim RxDetPagFaltante As ADODB.Recordset
    Set RxDetPagFaltante = New ADODB.Recordset
    
    Dim DeclaradoRestante As Double
    
    RxDetPagFaltante.CursorLocation = adUseServer
    
    Sql = "SELECT * FROM MA_DETALLEPAGO " & _
    "WHERE c_Util = 'F' " & _
    "AND Turno = " & Turno & " " & _
    "AND c_Caja = '" & no_caja & "' " & _
    "AND n_Falta <> 0 " & _
    "AND c_CodMoneda = '" & CodMon & "' " & _
    "AND c_CodDenominacion = '" & CodDen & "' " & _
    "AND n_Factor = " & Factor & " " & _
    "AND c_CodCajero = '" & LcCajero & "' " & _
    "ORDER BY n_Falta "
    
    RxDetPagFaltante.Open Sql, ConexionVAD20RedGrabar, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not RxDetPagFaltante.EOF Then
        
        DeclaradoRestante = RoundUp(Valor * Factor, 8)
        
        Do Until RxDetPagFaltante.EOF()
            
            ' Esta parte recorre todos los pagos en denominaci�n y factor especifico y les va
            ' calculando el faltante hasta que se acabe el valor declarado.
            ' Cuando ya no hay mas valor declarado empiezan a quedar faltantes.
            
            If DeclaradoRestante >= RoundUp(RxDetPagFaltante!n_Falta, Std_Decm) Then
                'RxDetPagFaltante.Close
                RxDetPagFaltante.Update
                RxDetPagFaltante!c_Util = "T"
                If RxDetPagFaltante!n_Falta >= 0 Then ' Omitir casos efectivo negativo.
                    DeclaradoRestante = RoundUp(DeclaradoRestante - RoundUp(RxDetPagFaltante!n_Falta, Std_Decm), 8)
                End If
                RxDetPagFaltante!n_Falta = 0
                RxDetPagFaltante.UpdateBatch
            Else
                RxDetPagFaltante.Update
                    RxDetPagFaltante!n_Falta = RoundUp(RxDetPagFaltante!n_Falta - DeclaradoRestante, 8)
                    DeclaradoRestante = 0
                RxDetPagFaltante.UpdateBatch
            End If
            
            RxDetPagFaltante.MoveNext
            
            If DeclaradoRestante <= 0 Then Exit Do
            If RxDetPagFaltante.EOF() Then Exit Do
            
        Loop
        
        'RxDetPagFaltante.UpdateBatch
        
    End If
    
    RxDetPagFaltante.Close
    
End Sub

Public Function AutoAsignarEfectivo(ByVal pCodMoneda As String, _
ByVal pMontoEvaluar As Double, _
Optional ByVal pModalidad As Integer = 1) As Double
    
    Dim TotalAsignado As Double, Asignaciones As Collection
    
    AsignarEfectivoPorFactor pCodMoneda, pMontoEvaluar, TotalAsignado, Asignaciones, pModalidad
    
    ConexionVAD20RedGrabar.Execute _
    "UPDATE TR_DENOMINA_TEMP SET " & _
    "Cantidad = 0 " & _
    "WHERE CodMoneda = '" & pCodMoneda & "' " & _
    "AND Caja = '" & nCaja & "' " & _
    "AND CodigoBanco = '' " & _
    "AND Usuario = '" & LcCajero & "' " & _
    "AND Turno = " & CDec(Turno) & " "
    
    If Not Asignaciones Is Nothing Then
        
        For Each Itm In Asignaciones
            
            ConexionVAD20RedGrabar.Execute _
            "UPDATE TR_DENOMINA_TEMP " & vbNewLine & _
            "SET Cantidad = (" & Itm(2) & ") " & _
            "WHERE 1 = 1 " & vbNewLine & _
            "AND CodMoneda = '" & pCodMoneda & "' " & vbNewLine & _
            "AND CodDenomina = '" & Itm(0) & "' " & vbNewLine & _
            "AND Caja = '" & nCaja & "' " & vbNewLine & _
            "AND Usuario = '" & LcCajero & "' " & vbNewLine & _
            "AND Turno = " & CDec(Turno) & " " & vbNewLine
            
        Next
        
    End If
    
    AutoAsignarEfectivo = TotalAsignado
    
End Function

Public Sub AsignarEfectivoPorFactor(ByVal pCodMoneda As String, ByVal pMontoBase As Double, _
ByRef oTotalAsignado As Double, ByRef oAsignaciones As Object, _
Optional ByVal pModalidad As Integer = -1)
    
    If pModalidad <= 0 Then
        pModalidad = POS_DeclararEfectivoDesgloseAutomatico
    End If
    
    If pModalidad <= 0 Or pModalidad > 2 Then
        pModalidad = 1
    End If
    
    oTotalAsignado = 0
    Set oAsignaciones = Nothing
    
    Call Apertura_RecordsetC(RxDenomina)
    
    RxDenomina.Open _
    "SELECT TR.* FROM TR_DENOMINA_TEMP TR " & vbNewLine & _
    "INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES DEN " & vbNewLine & _
    "ON TR.CodMoneda = DEN.c_CodMoneda " & vbNewLine & _
    "AND TR.CodDenomina = DEN.c_CodDenomina " & vbNewLine & _
    "WHERE TR.CodMoneda = '" & pCodMoneda & "' " & vbNewLine & _
    "AND DEN.c_Real = 1 " & _
    "AND DEN.n_Valor > 0 " & vbNewLine & _
    "AND TR.Caja = '" & nCaja & "' " & vbNewLine & _
    "AND TR.Usuario = '" & LcCajero & "' " & vbNewLine & _
    "AND TR.Turno = " & CDec(Turno) & " " & vbNewLine & _
    "AND TR.Valor > 0 " & vbNewLine & _
    "ORDER BY DEN.n_Valor " & _
    IIf(pModalidad = 1, "DESC", "ASC") & " ", _
    ConexionVAD20RedGrabar, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set RxDenomina.ActiveConnection = Nothing
    
    If Not RxDenomina.EOF Then
        
        Dim MontoRestante As Double, CantAsignar As Double
        Dim TotalAsignado As Double, TotalResiduo As Double
        
        Dim ColDen As Collection, Asignaciones As Collection
        Dim Itm As Variant, ItmAsignar As Variant
        
        Set Asignaciones = New Collection
        Set ColDen = New Collection
        
        MontoRestante = pMontoBase
        TotalAsignado = 0
        
        While Not RxDenomina.EOF
            ColDen.Add Array(CStr(RxDenomina!CodDenomina), _
            CDbl(RxDenomina!Valor), CStr(RxDenomina!DesDenomina)), _
            CStr(RxDenomina!CodDenomina)
            RxDenomina.MoveNext
        Wend
        
        If ColDen.Count > 0 And pModalidad = 2 Then
            
            ColIndex1 = 1
            TmpValor1 = CDbl(ColDen.Item(ColIndex1)(1))
            CantValor1 = (MontoRestante / TmpValor1)
            Residuo1 = RoundUp((CantValor1 - Fix(CantValor1)) * TmpValor1, 8)
            CantValor1 = Fix(CantValor1)
            
            If Residuo1 > 0 Then
                
                Residuo3 = Residuo1
                Match = False
                Mejora = False
                
                For A = 1 To ColDen.Count
                    
                    ColIndex2 = A
                    TmpValor2 = CDbl(ColDen.Item(ColIndex2)(1))
                    CantValor2 = (MontoRestante / TmpValor2)
                    Residuo2 = RoundUp((CantValor2 - Fix(CantValor2)) * TmpValor2, 8)
                    CantValor2 = Fix(CantValor2)
                    
                    For i = 1 To ColDen.Count
                        
                        If i <> ColIndex2 Then
                            
                            ProxValor = ColDen.Item(i)(1)
                            
                            For j = 1 To 50
                                
                                If j > CantValor2 Then Exit For
                                
                                TmpSumValor = Residuo2 + (j * TmpValor2)
                                
                                TmpResCant = (TmpSumValor / ProxValor)
                                
                                TmpResiduo = RoundUp((TmpResCant - Fix(TmpResCant)) * ProxValor, 8)
                                
                                TmpResCant = Fix(TmpResCant)
                                
                                If TmpResiduo = 0 Then
                                    
                                    Match = True
                                    Mejora = False
                                    CantResta = j
                                    ColIndex3 = i
                                    TmpValor3 = ProxValor
                                    CantValor3 = TmpResCant
                                    Residuo3 = 0
                                    Exit For
                                    
                                ElseIf TmpResiduo < Residuo3 Then
                                    
                                    Mejora = True ' Se disminuyo el residuo pero no es 0 aun. Seguir recorriendo.
                                    CantResta = j
                                    ColIndex3 = i
                                    TmpValor3 = ProxValor
                                    CantValor3 = TmpResCant
                                    Residuo3 = TmpResiduo
                                    
                                    ColIndex4 = ColIndex2
                                    TmpValor4 = TmpValor2
                                    CantValor4 = CantValor2
                                    
                                End If
                                
                            Next j
                            
                            If Match Then ' Residuo cero.
                                Exit For
                            End If
                            
                        End If
                        
                    Next i
                    
                    If Match Then  ' Residuo cero.
                        Exit For
                    End If
                    
                Next A
                
                If Mejora Then
                    ColIndex2 = ColIndex4
                    TmpValor2 = TmpValor4
                    CantValor2 = CantValor4
                End If
                
                If Match Or Mejora Then ' Residuo cero.
                    
                    ColIndex1 = ColIndex2
                    TmpValor1 = TmpValor2
                    CantValor1 = (CantValor2 - CantResta)
                    
                    ColIndex2 = ColIndex3
                    TmpValor2 = TmpValor3
                    CantValor2 = CantValor3
                    
                Else
                    
                    ColIndex2 = 0
                    TmpValor2 = 0
                    CantValor2 = 0
                    
                End If
                
            Else
                
                ColIndex2 = 0
                TmpValor2 = 0
                CantValor2 = 0
                
            End If
            
            TotalAsignado = 0
            
            Itm = ColDen.Item(ColIndex1)
            
            ItmAsignar = Array(Itm(0), Itm(1), CantValor1, CDbl(CantValor1 * Itm(1)), Itm(2))
            Asignaciones.Add ItmAsignar
            
            TotalAsignado = Round(TotalAsignado + ItmAsignar(3), 8)
            
            If ColIndex2 > 0 Then
                
                Itm = ColDen.Item(ColIndex2)
                
                ItmAsignar = Array(Itm(0), Itm(1), CantValor2, CDbl(CantValor2 * Itm(1)), Itm(2))
                Asignaciones.Add ItmAsignar
                
                TotalAsignado = Round(TotalAsignado + ItmAsignar(3), 8)
                
            End If
            
        ElseIf ColDen.Count > 0 And pModalidad = 1 Then
            
            MontoRestante = pMontoBase
            TotalAsignado = 0
            
            For Each Itm In ColDen
                
                If (MontoRestante >= Itm(1)) Then
                    Cant = Fix(MontoRestante / Itm(1))
                    ItmAsignar = Array(Itm(0), Itm(1), Cant, CDbl(Cant * Itm(1)), Itm(2))
                    Asignaciones.Add ItmAsignar
                    ColDen.Remove Itm(0)
                    MontoRestante = RoundUp(MontoRestante - ItmAsignar(3), 8)
                    TotalAsignado = RoundUp(TotalAsignado + ItmAsignar(3), 8)
                End If
                
            Next
            
        End If
        
        oTotalAsignado = TotalAsignado
        
        If Asignaciones.Count > 0 Then
            
            Set oAsignaciones = Asignaciones
            
        End If
        
    End If ' Not RxDenomina.EOF
    
    RxDenomina.Close
    
End Sub
