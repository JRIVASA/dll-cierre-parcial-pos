VERSION 5.00
Begin VB.Form frm_Mensajeria 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3135
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   7965
   ControlBox      =   0   'False
   Icon            =   "mensajeria.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   7965
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   7440
         Picture         =   "mensajeria.frx":628A
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5535
         TabIndex        =   6
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.CommandButton btnCopy 
      BackColor       =   &H80000004&
      Caption         =   "F3"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   1110
      Picture         =   "mensajeria.frx":6ACC
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "F3"
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   6645
      Picture         =   "mensajeria.frx":884E
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2025
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   5355
      Picture         =   "mensajeria.frx":A5D0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2025
      Width           =   1095
   End
   Begin VB.TextBox Mensaje 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1200
      Left            =   1230
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   635
      Width           =   6525
   End
   Begin VB.Label lblMarginTop 
      BackColor       =   &H80000005&
      Height          =   1400
      Left            =   1130
      TabIndex        =   8
      Top             =   535
      Width           =   6625
   End
   Begin VB.Label lbl_Progress 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   480
      TabIndex        =   3
      Top             =   1800
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Image Icono 
      Appearance      =   0  'Flat
      Height          =   720
      Left            =   210
      Picture         =   "mensajeria.frx":C352
      Top             =   900
      Width           =   720
   End
End
Attribute VB_Name = "frm_Mensajeria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada As Boolean

Private Declare Function SendMessage Lib "user32" _
Alias "SendMessageA" (ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
lParam As Any) As Long

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Private Sub aceptar_Click()
    Tecla_Pulsada = False
    Retorno = True
    Unload Me
End Sub

Private Sub btnCopy_Click()
    
    On Error GoTo ErrC
    
    Clipboard.Clear
    Clipboard.SetText Mensaje.Text
    
    btnCopy.Caption = StellarMensaje(76) '"Copiado"
    
    Exit Sub
    
ErrC:
    
    btnCopy.Caption = StellarMensaje(77) '"Fallido"
    
End Sub

Private Sub Cancelar_Click()
    Form_KeyDown vbKeyF12, 0
End Sub

Private Sub Exit_Click()
    Form_KeyDown vbKeyF12, 0
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        FormaCargada = True
        AjustarPantalla Me
    End If
    
    ModalDisponible = False
    
    ' C�digo para imponer el mensaje al usuario, ya que a veces los mensajes
    ' Quedan detr�s de otras ventanas cuando se produce un status inactivo en la aplicaci�n.
    
    DoEvents
    Me.Refresh
    
    'Traslado la funci�n de gRutinas para que este formulario sea portable y no requiera componentes externos.
    
    If PuedeObtenerFoco(Me) Then Me.SetFocus
    If PuedeObtenerFoco(Aceptar) Then Aceptar.SetFocus
    
    If Uno = True Then
        Cancelar.Enabled = False
    Else
        Cancelar.Enabled = True
    End If
    
    DoEvents
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            btnCopy_Click
        Case vbKeyF12
            Retorno = False
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    
    Screen.MousePointer = vbDefault
    Me.lbl_Organizacion.Caption = StellarMensaje(78) '"Informaci�n Stellar"
    
    Aceptar.Caption = StellarMensaje(5) ' Aceptar
    Cancelar.Caption = StellarMensaje(6) ' Cancelar
    btnCopy.Caption = StellarMensaje(79) ' Copiar [F3]
    
    If txtMensaje1 <> Empty Then Aceptar.Caption = txtMensaje1
    If txtMensaje2 <> Empty Then Cancelar.Caption = txtMensaje2
    
    If Uno Then
        Cancelar.Enabled = False
        'cancelar.Visible = False
        'frm_Mensajeria.Width = 7605
    Else
        Cancelar.Enabled = True
        
        'cancelar.Visible = True
        'frm_Mensajeria.Width = 8730
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_Mensajeria = Nothing
    ModalDisponible = True
End Sub

Private Sub mensaje_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            btnCopy_Click
    End Select
End Sub
