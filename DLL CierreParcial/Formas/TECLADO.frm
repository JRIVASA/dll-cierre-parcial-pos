VERSION 5.00
Begin VB.Form TECLADO 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Teclado Stellar                                                   BIGWISE  VENEZUELA, S.A.      "
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10725
   ClipControls    =   0   'False
   Icon            =   "TECLADO.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   10725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "M"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   246
      Left            =   5790
      TabIndex        =   38
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Enter"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   1
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   870
      Width           =   1065
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Barra Espaciadora"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   2
      Left            =   7710
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   2490
      Width           =   1905
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "<----"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   0
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   60
      Width           =   1065
   End
   Begin VB.CommandButton Command1 
      Caption         =   "D"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   232
      Left            =   1950
      TabIndex        =   24
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   239
      Left            =   8670
      TabIndex        =   31
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "N"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   245
      Left            =   4830
      TabIndex        =   37
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "B"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   244
      Left            =   3870
      TabIndex        =   36
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Y"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   225
      Left            =   4830
      TabIndex        =   16
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "R"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   223
      Left            =   2910
      TabIndex        =   14
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   229
      Left            =   8670
      TabIndex        =   20
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "J"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   236
      Left            =   5790
      TabIndex        =   28
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "V"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   243
      Left            =   2910
      TabIndex        =   35
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "T"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   224
      Left            =   3870
      TabIndex        =   15
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "A"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   230
      Left            =   30
      TabIndex        =   22
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "K"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   237
      Left            =   6750
      TabIndex        =   29
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "S"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   231
      Left            =   990
      TabIndex        =   23
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "L"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   238
      Left            =   7710
      TabIndex        =   30
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "E"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   222
      Left            =   1950
      TabIndex        =   13
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Q"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   220
      Left            =   30
      TabIndex        =   11
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "U"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   226
      Left            =   5790
      TabIndex        =   17
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "F"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   233
      Left            =   2910
      TabIndex        =   25
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Z"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   240
      Left            =   30
      TabIndex        =   32
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "W"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   221
      Left            =   990
      TabIndex        =   12
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "I"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   227
      Left            =   6750
      TabIndex        =   18
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "G"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   234
      Left            =   3870
      TabIndex        =   26
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "X"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   241
      Left            =   990
      TabIndex        =   33
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "O"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   228
      Left            =   7710
      TabIndex        =   19
      Top             =   870
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "H"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   235
      Left            =   4830
      TabIndex        =   27
      Top             =   1680
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "C"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   242
      Left            =   1950
      TabIndex        =   34
      Top             =   2490
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "0"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   309
      Left            =   8670
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "7"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   300
      Left            =   5790
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "4"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   303
      Left            =   2910
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "1"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   306
      Left            =   30
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "8"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   301
      Left            =   6750
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "5"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   304
      Left            =   3870
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "2"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   307
      Left            =   990
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "9"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   302
      Left            =   7710
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "6"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   305
      Left            =   4830
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "3"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   308
      Left            =   1950
      MaskColor       =   &H80000018&
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   60
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   405
      Left            =   6750
      TabIndex        =   39
      Top             =   2490
      Width           =   945
   End
End
Attribute VB_Name = "TECLADO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PosT As Long
Public PosH As Long
Public ParentForm_PosT As Long
Public ParentForm_PosH As Long

Private Sub Command1_Click(index As Integer)
    Select Case index
        Case 220 To 246
            CampoT.Text = CampoT.Text & Command1(index).Caption
        
        Case 300 To 309
            CampoT.Text = CampoT.Text & Command1(index).Caption
            
        Case 405
            CampoT.Text = CampoT.Text & SDecimal()
        
        Case Is = 0
            If CampoT.Text <> "" Then CampoT.Text = Mid(CampoT.Text, 1, Len(CampoT.Text) - 1)
        
        Case 1, 3
            FormaTotal = False
            'SendKeys "{enter}"
            oTeclado.Key_Return
            Unload Me
        Case Is = 2
            CampoT.Text = CampoT.Text & " "
        
    End Select
    
End Sub

Private Sub Form_Activate()

    'Conseguir la posicion del formulario que va a instanciar el teclado.
    'Sumarsela a la posicion de la pantalla donde debe aparecer el teclado.
    
    PosT = ParentForm_PosT + PosT
    PosH = ParentForm_PosH + PosH
    
    'Si se defini� una posicion diferente a la posicion default, asignar dichas coordenadas al teclado
    
    If Not PosT = 0 Then TECLADO.Top = PosT
    If Not PosH = 0 Then TECLADO.Left = PosH
    
End Sub

Private Sub Form_Load()
'
'    If FORMATOTAL = True Then
'        TECLADO.Top = 4300
'        TECLADO.Left = 100
'    Else
'        TECLADO.Top = 0
'        TECLADO.Left = 100
'        FORMATOTAL = False
'    End If
'    'campot.SelStart = 0
'    'campot.SelLength = Len(campot)
End Sub
