VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_Super_Consultas 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8790
   ClientLeft      =   1350
   ClientTop       =   1365
   ClientWidth     =   11835
   ControlBox      =   0   'False
   Icon            =   "Frm_Super_Consultas1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8790
   ScaleMode       =   0  'User
   ScaleWidth      =   11835
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdBuscar 
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      Height          =   1035
      Index           =   0
      Left            =   9360
      Picture         =   "Frm_Super_Consultas1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   5400
      Width           =   1035
   End
   Begin VB.CommandButton CmdAceptarCancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      Height          =   1035
      Index           =   0
      Left            =   10485
      Picture         =   "Frm_Super_Consultas1.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   5400
      Width           =   1035
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9075
         TabIndex        =   18
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11160
         Picture         =   "Frm_Super_Consultas1.frx":9D8E
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " B�squeda Avanzada "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1665
      Left            =   225
      TabIndex        =   12
      Top             =   6945
      Width           =   11415
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   " Condici�n "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1245
         Left            =   7530
         TabIndex        =   9
         Top             =   180
         Width           =   2175
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&O"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   1
            Left            =   1080
            TabIndex        =   5
            Top             =   570
            Width           =   705
         End
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&Y"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   0
            Left            =   180
            TabIndex        =   4
            Top             =   570
            Value           =   -1  'True
            Width           =   705
         End
      End
      Begin VB.CommandButton Btn_Agregar 
         Caption         =   "A&gregar"
         Height          =   1125
         Left            =   9930
         Picture         =   "Frm_Super_Consultas1.frx":A5D0
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   360
         Width           =   1335
      End
      Begin MSComctlLib.ListView Lst_Avanzada 
         CausesValidation=   0   'False
         Height          =   1215
         Left            =   210
         TabIndex        =   6
         Top             =   210
         Width           =   6885
         _ExtentX        =   12144
         _ExtentY        =   2143
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   5790296
         BackColor       =   16448250
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "StrCampo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campo"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Valor"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Operador"
            Object.Width           =   3351
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Height          =   1080
      Left            =   -90
      TabIndex        =   10
      Top             =   421
      Width           =   11865
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   180
         TabIndex        =   11
         Top             =   150
         Width           =   6030
         _ExtentX        =   10636
         _ExtentY        =   1429
         ButtonWidth     =   2170
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Informaci�n"
               Key             =   "Informacion"
               Object.ToolTipText     =   "Informacion sobre el elemento Seleccionado"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir de la B�squeda"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema"
               ImageIndex      =   3
            EndProperty
         EndProperty
         Begin VB.Timer Tim_Progreso 
            Enabled         =   0   'False
            Interval        =   500
            Left            =   5760
            Top             =   120
         End
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1380
      Left            =   225
      TabIndex        =   8
      Top             =   5250
      Width           =   8955
      Begin VB.CheckBox Chk_Avanzada 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "&Avanzada"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   915
         Width           =   1185
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2430
         TabIndex        =   13
         Top             =   375
         Width           =   4785
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   375
         Width           =   2010
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   225
         Left            =   2430
         TabIndex        =   14
         Top             =   1005
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   1005
         Left            =   7500
         TabIndex        =   21
         Top             =   240
         Width           =   1095
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   7740
         MouseIcon       =   "Frm_Super_Consultas1.frx":C352
         MousePointer    =   99  'Custom
         Picture         =   "Frm_Super_Consultas1.frx":C65C
         Stretch         =   -1  'True
         Top             =   480
         Width           =   600
      End
      Begin VB.Label Lbl_Progreso 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Progreso"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   2430
         TabIndex        =   15
         Top             =   780
         Width           =   4785
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   1815
      Top             =   870
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":C708
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":E49A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":1022C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":11FBE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   3285
      Left            =   225
      TabIndex        =   7
      Top             =   1770
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   5794
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16448250
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Image Image1 
      Height          =   2160
      Left            =   3472
      Picture         =   "Frm_Super_Consultas1.frx":13D50
      Stretch         =   -1  'True
      Top             =   2227
      Visible         =   0   'False
      Width           =   2400
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   225
      TabIndex        =   1
      Top             =   1500
      Visible         =   0   'False
      Width           =   9135
   End
End
Attribute VB_Name = "Frm_Super_Consultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim isPrimeravez As Boolean ' esta varible es para que cuando tenga registro y no este visible por primravez arraque el focus en el grid
Private strSQL As String
Public frmPantallaInfo As Form
Public formPadre As Form
Public strBotonPresionado As String
Public CampoCodigo As String
Public strCondicion As String
Public strCadBusCod As String
Public strCadBusDes As String
Public strOrderBy As String
Dim strSqlMasCondicion As String
Public strTituloForma As String
Public UsaCodigosAlternos As Boolean
Public FormatoAplicar As Boolean
Public FormatoCantStr As Long
Public FormatoRelleno As String
Dim arrCamposBusquedas()
Dim arrCamposDeLaConsulta()
Dim arrPropiedadCampo()
Dim arrRegistroSeleccionado()
Dim Objlw As New obj_listview
Private Conex_SC As ADODB.Connection
Private Band As Boolean
Public arrResultado
Dim ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String

Private Function Consulta_Avanzada() As String
    Dim SqlW As String, Sql As String
    Sql = strSQL
    If Me.Lst_Avanzada.ListItems.Count = 0 Then
        Mensaje True, "No ha especificado ning�n Campo de B�squeda"
        Consulta_Avanzada = ""
        Exit Function
    End If
    SqlW = " WHERE "
    For i = 1 To Me.Lst_Avanzada.ListItems.Count
        SqlW = SqlW + " (" + Me.Lst_Avanzada.ListItems(i) & " LIKE '" + Me.Lst_Avanzada.ListItems(i).SubItems(2) + "%') "
        If i < Me.Lst_Avanzada.ListItems.Count Then
            If Me.Lst_Avanzada.ListItems(i).SubItems(3) = "Y" Then
                SqlW = SqlW + " AND "
            Else
                SqlW = SqlW + " OR "
            End If
        End If
    Next
    If Trim(Me.strCondicion) <> "" Then
        If SqlW = " WHERE " Then
            SqlW = SqlW + " (" + strCondicion + ") "
        Else
            SqlW = SqlW + " AND (" + strCondicion + ") "
        End If
    End If
    Sql = Sql + SqlW
    If Trim(Me.strOrderBy) <> "" Then
        Sql = Sql + " ORDER BY " + Me.strOrderBy
    End If
    Consulta_Avanzada = Sql
End Function

Private Sub Btn_Agregar_Click()
    Dim itmX As ListItem
    Set itmX = Me.Lst_Avanzada.ListItems.Add(, , arrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1))
    itmX.ListSubItems.Add , , Me.cmbItemBusqueda.Text
    itmX.ListSubItems.Add , , Me.txtDato
    If Me.Opt_Operador(0).Value = True Then
        itmX.ListSubItems.Add , , "Y"
    Else
        itmX.ListSubItems.Add , , "O"
    End If
End Sub

Private Sub cmbItemBusqueda_Click()
    Me.txtDato = ""
    If Band = True Then
        If Me.txtDato.Enabled Then Me.txtDato.SetFocus
    End If
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub CmdTeclado_Click()
    
    'If PuedeObtenerFoco(CampoT) Then CampoT.SetFocus
    'TECLADO.Show vbModal
    TecladoPOS CampoT
    
End Sub

Private Sub Lst_Avanzada_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Me.Lst_Avanzada.ListItems.Remove Me.Lst_Avanzada.SelectedItem.index
    End If
End Sub

Private Sub Tim_Progreso_Timer()
    ContPuntos = ContPuntos + 1
    If ContPuntos > 3 Then
        ContPuntos = 0
    End If
    Me.Lbl_Progreso.Caption = StrCont + "Buscando" + String(ContPuntos, ".")
End Sub

Private Sub Chk_Avanzada_Click()
    Me.Lst_Avanzada.ListItems.Clear
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 6040
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 7530
    End If
End Sub

Private Sub CmdAceptarCancelar_Click(index As Integer)
'    txtDato_KeyDown vbKeyEscape, 0
    Me.strBotonPresionado = "Cancelar"
End Sub

Private Sub CmdBuscar_Click(index As Integer)
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub Form_Activate()
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 6900
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 8900
    End If
    If Me.txtDato.Enabled = True Then
        Me.txtDato.SetFocus
    End If
    If isPrimeravez Then
        If Me.GRID.ListItems.Count Then
            If Me.GRID.Enabled And Me.GRID.Visible Then
                Me.GRID.SetFocus
            End If
            isPrimeravez = False
        End If
    End If
    Band = True
End Sub

Private Sub Form_Load()
    
    ReDim arrCamposBusquedas(0)
    ReDim arrCamposDeLaConsulta(0)
    ReDim arrRegistroSeleccionado(0)
    ReDim arrPropiedadCampo(0)
    Band = True
    isPrimeravez = True
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(64) 'informaci�n
    Toolbar1.Buttons(2).Caption = "F12" & " " & Stellar_Mensaje(54) 'salir
    Toolbar1.Buttons(3).Caption = Stellar_Mensaje(7) 'ayuda
    
    'Label1.Caption = "Criterios de B�squeda" '"Stellar_Mensaje(6057) 'criterios de busqueda
    Chk_Avanzada.Caption = Stellar_Mensaje(67) 'avanzada
    Lbl_Progreso.Caption = Stellar_Mensaje(68) 'progreso
    CmdBuscar(0).Caption = Stellar_Mensaje(102) 'buscar
    CmdAceptarCancelar(0).Caption = Stellar_Mensaje(105) 'cancelar
    
    Btn_Agregar.Caption = Stellar_Mensaje(197) 'agregar
    Frame1.Caption = Stellar_Mensaje(10134) 'condicion
    Opt_Operador(0).Caption = Stellar_Mensaje(69) 'y
    Opt_Operador(1).Caption = Stellar_Mensaje(70) 'o
    
    Lst_Avanzada.ColumnHeaders(2).Text = Stellar_Mensaje(10147) 'campo
    Lst_Avanzada.ColumnHeaders(3).Text = Stellar_Mensaje(2045) 'valor
    Lst_Avanzada.ColumnHeaders(4).Text = Stellar_Mensaje(71) 'operador
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo Errores
    Select Case KeyCode
                
        Case Is = vbKeyF1
            ' Llamar Ayuda
            
        Case Is = vbKeyF2
'            Me.formPadre.Boton = 1
'            Call grid_DblClick
        
        Case Is = vbKeyF12
            txtDato_KeyDown vbKeyEscape, 0
    
    End Select
Exit Sub
Errores:
    'Call GRABAR_ERRORES(Err.Number, Err.Description, LCCAJERO, Me.Name, Err.Source)
    Unload Me
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case "Informaci�n"
            Me.formPadre.boton = 1
            Call grid_DblClick
        
        Case "Salir"
            txtDato_KeyDown vbKeyEscape, 0
    End Select
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then
       strBotonPresionado = "Salir"
       arrResultado = Array("", "")
       Unload Me
       'Me.Hide
    End If
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            If txtDato = "" Then
                txtDato.SetFocus
                Exit Sub
            End If
            
            If Me.Chk_Avanzada.Value = 1 Then
                strSqlMasCondicion = Consulta_Avanzada
                Consulta_Mostrar
            Else
                If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                    strSqlMasCondicion = strSQL
                    If Trim(Me.strCondicion) <> "" Then
                        If InStr(strSQL, "where") Or InStr(strSQL, "WHERE") Then
                            strSqlMasCondicion = strSQL + " AND (" + Me.strCondicion + ") "
                        Else
                            strSqlMasCondicion = strSQL + " WHERE (" + Me.strCondicion + ") "
                        End If
                    End If
                    Consulta_Mostrar
                Else
                    'strCadBusDes = arrCamposDeLaConsulta(Me.cmbItemBusqueda.ListIndex + 1)
                    ' Ivan Espinoza 31/08/02
                    strSqlMasCondicion = strSQL
                    ''''Debug.Print strSqlMasCondicion
                    If Trim(Me.strCondicion) <> "" Then
                        If InStr(UCase(strSQL), "WHERE") Then
                            strSqlMasCondicion = strSQL + " AND (" + Me.strCondicion + ") "
                        Else
                            strSqlMasCondicion = strSQL + " WHERE (" + Me.strCondicion + ") "
                        End If
                    End If
                    '''''Debug.Print strSqlMasCondicion
                    strCadBusDes = arrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1)
                    If strCadBusDes <> "" Then
                        'If InStr(UCase(strSQL), "WHERE") Then
                        If InStr(UCase(strSqlMasCondicion), "WHERE") Then
                            strSqlMasCondicion = strSqlMasCondicion + " AND " + strCadBusDes + " LIKE '" + Me.txtDato + "%'"
                        Else
                        '19695633
                        'Solo busca productos activos, los inactivos NO.
                            strSqlMasCondicion = strSqlMasCondicion + " WHERE  " + strCadBusDes + " LIKE '" + Me.txtDato + "%' "
                        End If
                    Else
                        Mensaje True, "Debe especificar Mejor la Consulta"
                        Screen.MousePointer = 0
                        Exit Sub
                    End If
                    '''''Debug.Print strSqlMasCondicion
                    '''''Debug.Print strSqlMasCondicion
                    Consulta_Mostrar
                End If
            End If
    End Select
    
    Screen.MousePointer = 0
    
    On Error GoTo 0
    
    Exit Sub
    
GetError:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(txtDato_Keypress)"
    
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    'a = Abs((grid.SortOrder + 1) - 2)
    'grid.SortOrder = Abs((grid.SortOrder + 1) - 2)
    If GRID.SortOrder = lvwAscending Then
        GRID.SortOrder = lvwDescending
    Else
        GRID.SortOrder = lvwAscending
    End If
    GRID.SortKey = ColumnHeader.index - 1
    GRID.Sorted = True
End Sub

Private Sub grid_DblClick()
    If GRID.ListItems.Count = 0 Then Exit Sub
     arrResultado = Objlw.TomardataLw(Me.GRID, Me.GRID.SelectedItem.index)
     Unload Me
 End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Public Function buscar() As Variant
    Dim ArrResultados()
    Dim rsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection

    If Trim(strSQL) = "" Then
        Exit Function
    End If
    If Trim(strCondicion) <> "" Then
        If InStr(UCase(strSQL), "WHERE") Then
            strSQL = strSQL + " AND (" + strCondicion + ") "
        Else
            strSQL = strSQL + " WHERE (" + strCondicion + ") "
        End If
    End If
    
    On Error GoTo GetError
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    rsConsulta.CursorLocation = adUseServer
    Apertura_Recordset rsConsulta
    rsConsulta.Open strSQL, Cnx, adOpenKeyset, adLockReadOnly
    ReDim ArrResultados(rsConsulta.Fields.Count - 1)
    If Not rsConsulta.EOF Then
        For i = 0 To rsConsulta.Fields.Count - 1
            ArrResultados(i) = rsConsulta.Fields(i)
        Next
    Else
        For i = 0 To rsConsulta.Fields.Count - 1
            ArrResultados(i) = ""
        Next
        Mensaje True, "No hay ning�n Item que cumpla con los Par�metros de B�squeda"
    End If
    rsConsulta.Close
    buscar = ArrResultados
    Exit Function
    
GetError:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Buscar)"
    
End Function

Public Function Consulta_Mostrar() As Boolean
    Dim rsConsulta As New ADODB.Recordset
    Dim Item As ListItem
    Dim lista As ListView
    Dim oField As ADODB.Field
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    Dim Sql As String, Rs As New ADODB.Recordset
    Dim CodigoBusq As String
    Set lista = GRID
    Cont = 0
    If Trim(strSqlMasCondicion) = "" Then
        Exit Function
    End If
    Me.strBotonPresionado = ""
    Call Desactivar_Objetos(False)
    On Error GoTo GetError
    Screen.MousePointer = 11
    GRID.ListItems.Clear
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    If Trim(UCase(Me.cmbItemBusqueda.Text)) = "CODIGO" And UsaCodigosAlternos = True Then
        Sql = " SELECT c_codnasa AS CodMaestro, c_codigo AS CodAlterno From MA_CODIGOS"
'        If FormatoAplicar = True Then
'            CodigoBusq = Format(Me.txtDato, String(FormatoCantStr - 1, FormatoRelleno) + "#")
'        End If
        CodigoBusq = Me.txtDato
'        strSqlMasCondicion = strSqlMasCondicion + " WHERE " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
        
        Sql = Sql + " WHERE c_codigo LIKE '" & CodigoBusq & "%'"
        Rs.CursorLocation = adUseServer
'        '''''Debug.Print Sql
        Rs.Open Sql, Cnx, adOpenKeyset, adLockReadOnly
        Do While Not Rs.EOF
            CodigoBusq = Rs!CodMaestro
'            If FormatoAplicar = True Then
'                CodigoBusq = Format(CodigoBusq, String(FormatoCantStr - 1, FormatoRelleno) + "#")
'            End If
            If InStr(UCase(strSqlMasCondicion), "WHERE") Then
                strSqlMasCondicion = strSqlMasCondicion + " OR " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
            Else
                strSqlMasCondicion = strSqlMasCondicion + " WHERE " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
            End If
            Rs.MoveNext
        Loop
    End If
    If strOrderBy <> "" Then
        strSqlMasCondicion = strSqlMasCondicion + " Order By  " + Me.strOrderBy
    'Agregado el 10/10/2012
    Else
        If Replace(Mid(strSqlMasCondicion, 18, 9), ",", "", 1, 9) = "C_DESCRI" Then
            Debug.Print "Existe"
            strSqlMasCondicion = strSqlMasCondicion + " ORDER BY c_DESCRI"
        Else
            strSqlMasCondicion = strSqlMasCondicion
            Debug.Print "No Existe"
        End If
    End If
    rsConsulta.CursorLocation = adUseServer
    Apertura_Recordset rsConsulta
    
    Debug.Print strSqlMasCondicion
    'MsgBox strSqlMasCondicion
    rsConsulta.Open strSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly

     If Not rsConsulta.EOF Then
        rsConsulta.MoveLast
        rsConsulta.MoveFirst
        ContFound = 0
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = rsConsulta.RecordCount
        ContFound = rsConsulta.RecordCount
        Do Until rsConsulta.EOF
            DoEvents
            Set Item = lista.ListItems.Add(, , rsConsulta.Fields(arrCamposDeLaConsulta(1)))
            For A = 2 To GRID.ColumnHeaders.Count
                If Left(UCase(arrCamposDeLaConsulta(A)), Len(arrCamposDeLaConsulta(A)) - 1) = "N_PRECIO" Then
                    If Not arrPropiedadCampo(A) Then
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(arrCamposDeLaConsulta(A))), FormatNumber(rsConsulta.Fields(arrCamposDeLaConsulta(A)), 2), "")
                    Else
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(arrCamposDeLaConsulta(A))), Fix(rsConsulta.Fields(arrCamposDeLaConsulta(A))), "")
                    End If
                Else
                    If Not arrPropiedadCampo(A) Then
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(arrCamposDeLaConsulta(A))), rsConsulta.Fields(arrCamposDeLaConsulta(A)), "")
                    Else
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(arrCamposDeLaConsulta(A))), Fix(rsConsulta.Fields(arrCamposDeLaConsulta(A))), "")
                    End If
                End If
            Next A
            If Me.strBotonPresionado = "Cancelar" Then
                Call Desactivar_Objetos(True)
                Screen.MousePointer = 0
                Me.Tim_Progreso.Enabled = False
                Me.Lbl_Progreso.Caption = "Progreso"
                Me.Barra_Prg.Value = 0
                Exit Do
            End If
            Me.Barra_Prg.Value = ContItems
            rsConsulta.MoveNext
            ContItems = ContItems + 1
            StrCont = CStr(FormatNumber(ContItems, 0)) + " de " + CStr(FormatNumber(ContFound, 0)) + " "
            Me.Lbl_Progreso = "Buscando" + String(ContPuntos, ".") + String(3 - ContPuntos, " ") + "  " + StrCont
        Loop
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        Call Desactivar_Objetos(True)
        If Me.Visible Then lista.SetFocus
    Else
        Mensaje True, "No hay ning�n Item que cumpla con los Par�metros de B�squeda"
    End If
    rsConsulta.Close
    Screen.MousePointer = 0
    Me.Tim_Progreso.Enabled = False
    Me.Lbl_Progreso.Caption = CStr(FormatNumber(ContItems, 0)) + " Items Encontrados"
    Me.Barra_Prg.Value = 0
    On Error GoTo 0
    Consulta_Mostrar = True
    Me.strBotonPresionado = ""
    Call Desactivar_Objetos(True)
    Exit Function
    
GetError:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Consulta_Mostrar)"
    
    Call Desactivar_Objetos(True)
    
    Me.strBotonPresionado = Empty
    
    Screen.MousePointer = 0
    
    Me.Tim_Progreso.Enabled = False
    Me.Lbl_Progreso.Caption = "Progreso"
    Me.Barra_Prg.Value = 0
    
    Consulta_Mostrar = False
    
    Me.GRID.SetFocus
    
End Function

Sub Desactivar_Objetos(Tipo As Boolean)
    Me.Frame3.Enabled = Tipo
    Me.txtDato.Enabled = Tipo
    GRID.Enabled = Tipo
    GRID.Visible = Tipo
    Me.CmdBuscar(0).Enabled = Tipo
End Sub

Public Function Inicializar(CadenaSql As String, srtTTitulo, ConexionBD As ADODB.Connection, Optional frmPantInfo As Form) As Boolean
    Dim m
    Me.cmbItemBusqueda.Clear
    GRID.ListItems.Clear
    GRID.ColumnHeaders.Clear
    Me.Lst_Avanzada.ListItems.Clear
    Me.strCondicion = ""
    intNumCol = 0
    
    arrResultado = Empty
    Me.Height = 7000
    ReDim arrCamposBusquedas(0)
    ReDim arrCamposDeLaConsulta(0)
    ReDim arrRegistroSeleccionado(0)
    ReDim arrPropiedadCampo(0)
    Me.UsaCodigosAlternos = False
    FormatoAplicar = False
    FormatoCantStr = 0
    FormatoRelleno = ""
    
    
    Set Conex_SC = ConexionBD
    strSQL = CadenaSql
    
    If Not frmPantInfo Is Nothing Then
       Set Me.frmPantallaInfo = frmPantInfo
    End If
    'Me.Caption = UCase(srtTTitulo)
    Me.lbl_Organizacion = UCase(srtTTitulo)
    'lblTitulo = UCase(srtTTitulo)
End Function

Public Function Add_ItemSearching(strDescripcion, strCampoTabla)
         ReDim Preserve arrCamposBusquedas(UBound(arrCamposBusquedas) + 1)
         'para guardar los campos del registro seleccionados
         'ReDim Preserve arrRegistroSeleccionado(UBound(arrRegistroSeleccionado) + 1)
         'arrCamposDeLaConsulta(UBound(arrCamposDeLaConsulta)) = strCampoTabla
         Band = False
         arrCamposBusquedas(UBound(arrCamposBusquedas)) = strCampoTabla
         Me.cmbItemBusqueda.AddItem strDescripcion
         Me.cmbItemBusqueda.Text = Me.cmbItemBusqueda.List(0)
End Function

Public Function Add_ItemLabels(strDescripcion As String, strCampoTabla As String, intWidth As Integer, intAligmnet As Integer, Optional TruncarDatos As Boolean = False)
         ReDim Preserve arrCamposDeLaConsulta(UBound(arrCamposDeLaConsulta) + 1)
         ReDim Preserve arrPropiedadCampo(UBound(arrPropiedadCampo) + 1)
         arrPropiedadCampo(UBound(arrPropiedadCampo)) = TruncarDatos
         arrCamposDeLaConsulta(UBound(arrCamposDeLaConsulta)) = strCampoTabla
         GRID.ColumnHeaders.Add , , UCase(strDescripcion), intWidth, intAligmnet
End Function
