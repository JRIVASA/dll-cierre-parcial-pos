VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Lista_Denominacionreal 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8445
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   10530
   ControlBox      =   0   'False
   Icon            =   "Lista_Denominacionreal.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8445
   ScaleWidth      =   10530
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame5 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8355
         TabIndex        =   9
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   10665
      TabIndex        =   5
      Top             =   421
      Width           =   10695
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   10230
         _ExtentX        =   18045
         _ExtentY        =   1429
         ButtonWidth     =   1561
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   7
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               Object.ToolTipText     =   "Teclado"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Bancos"
               Key             =   "Buscar"
               Description     =   "Bancos"
               ImageIndex      =   5
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox total 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   7560
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "0.00"
      Top             =   7800
      Width           =   2655
   End
   Begin VB.TextBox totalcantidad 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   3480
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "0.00"
      Top             =   7800
      Width           =   2655
   End
   Begin MSFlexGridLib.MSFlexGrid grid_denominaciones 
      Height          =   5805
      Left            =   180
      TabIndex        =   1
      Top             =   1740
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   10239
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16448250
      BackColorSel    =   16761024
      ForeColorSel    =   16448250
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   -180
      Top             =   1530
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":BB40
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":D8D2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txteditor 
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   2520
      MaxLength       =   20
      TabIndex        =   0
      Top             =   2160
      Width           =   1365
   End
   Begin VB.Label Label1 
      Caption         =   "Totales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   480
      TabIndex        =   4
      Top             =   7800
      Width           =   975
   End
End
Attribute VB_Name = "Lista_Denominacionreal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum ColGridDenomina
    dBanco = 0
    dNumero
    dMonto
    dTotal
    dCodigoDenominacion
    dCodigoBanco
End Enum

Dim CellRow As Long, CellCol As Long
Dim mDenominacion As String
Dim mGrabo As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF4
            If Not txteditor.Visible Then
                'If VerificarPaso("�Desea guardar los datos?") Then
                If VerificarPaso(Stellar_Mensaje(16052, True)) Then
                    Call GrabarDenominaciones
                End If
            End If
        Case vbKeyF6
            Toolbar1_ButtonClick Toolbar1.Buttons("Buscar")
        Case vbKeyF12
            If Not txteditor.Visible Then
                Unload Me
            End If
    End Select
End Sub

Private Sub Form_Load()
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    Me.txteditor.Visible = False
    Call IniciarGridDenominacion
    mDenominacion = Denomina_Cod
    CargarDenominaciones mDenominacion
    'Me.Caption = denomina_des
    Me.lbl_Organizacion = Denomina_Des
    
    Me.Toolbar1.Buttons(1).Caption = "F4" & " " & Stellar_Mensaje(103, True) 'grabar
    Me.Toolbar1.Buttons(3).Caption = "F12" & " " & Stellar_Mensaje(54, True) 'salir
    Me.Toolbar1.Buttons(5).Caption = Stellar_Mensaje(7, True) 'ayuda
    Me.Toolbar1.Buttons(6).Caption = Stellar_Mensaje(16040, True) 'teclado
    Me.Toolbar1.Buttons(7).Caption = "F6" & " " & Stellar_Mensaje(189, True) 'banco
    
End Sub

Private Sub IniciarGridDenominacion()
    
'    With grid_denominaciones
'        .Rows = 2
'        .Cols = 6
'        .Row = 0
'        .ColWidth(ColGridDenomina.dBanco) = 2800
'        .TextMatrix(.Row, ColGridDenomina.dBanco) = "BANCO"
'        .ColWidth(ColGridDenomina.dNumero) = 1500
'        .TextMatrix(.Row, ColGridDenomina.dNumero) = "CANT"
'        .Col = ColGridDenomina.dNumero
'        totalcantidad.Move .Left + .CellLeft, totalcantidad.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), totalcantidad.Height
'        .ColWidth(ColGridDenomina.dMonto) = 2500
'        .TextMatrix(.Row, ColGridDenomina.dMonto) = "DENOMINACION"
'        .ColWidth(ColGridDenomina.dTotal) = 3000
'        .TextMatrix(.Row, ColGridDenomina.dTotal) = "TOTAL"
'        .Col = ColGridDenomina.dTotal
'        total.Move .Left + .CellLeft, total.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), total.Height
'        .ColWidth(ColGridDenomina.dCodigoDenominacion) = 0
'        .TextMatrix(.Row, ColGridDenomina.dCodigoDenominacion) = "CODDENOMINA"
'        .ColWidth(ColGridDenomina.dCodigoBanco) = 0
'        .TextMatrix(.Row, ColGridDenomina.dCodigoBanco) = "CODDBANCO"
'    End With

    With grid_denominaciones
        .RowHeightMin = 720
        .Rows = 2
        .Cols = 6
        .Row = 0
        .ColWidth(ColGridDenomina.dBanco) = 2800
        .TextMatrix(.Row, ColGridDenomina.dBanco) = UCase(Stellar_Mensaje(189, True))
        .ColWidth(ColGridDenomina.dNumero) = 1500
        .TextMatrix(.Row, ColGridDenomina.dNumero) = UCase(Stellar_Mensaje(3001, True))
        .Col = ColGridDenomina.dNumero
        totalcantidad.Move .Left + .CellLeft, totalcantidad.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), totalcantidad.Height
        .ColWidth(ColGridDenomina.dMonto) = 2500
        .TextMatrix(.Row, ColGridDenomina.dMonto) = UCase(Stellar_Mensaje(2517, True))
        .ColWidth(ColGridDenomina.dTotal) = 3000
        .TextMatrix(.Row, ColGridDenomina.dTotal) = UCase(Stellar_Mensaje(141, True))
        .Col = ColGridDenomina.dTotal
        total.Move .Left + .CellLeft, total.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), total.Height
        .ColWidth(ColGridDenomina.dCodigoDenominacion) = 0
        .TextMatrix(.Row, ColGridDenomina.dCodigoDenominacion) = "CODDENOMINA"
        .ColWidth(ColGridDenomina.dCodigoBanco) = 0
        .TextMatrix(.Row, ColGridDenomina.dCodigoBanco) = "CODDBANCO"
    End With
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not mGrabo And Val(total.Text) > 0 Then
        Cancel = IIf(VerificarPaso("�Desea salir sin guardar?"), 0, 1)
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    If Not mGrabo Then
        
        ConexionVAD20Red.Execute _
        "UPDATE TR_DENOMINA_TEMP SET " & _
        "Cantidad = 0 " & _
        "WHERE CodMoneda = '" & Moneda_Cod & "' " & _
        "AND CodDenomina = '" & mDenominacion & "' " & _
        "AND Caja = '" & nCaja & "' " & _
        "AND Usuario = '" & LcCajero & "' " & _
        "AND Turno = " & CDec(Turno) & " "
        
        With Forma.monitor
            .Row = .RowSel
            .Col = 4
                .Text = FormatNumber(0, 2)
            .Col = 0
            oTeclado.Key_Right
        End With
        
    Else
        
        With Forma.monitor
           .Row = .RowSel
            .Col = 3
                montov = CDbl(.Text)
            .Col = 4
                Call TotalizarColumna(grid_denominaciones)
                .Text = total.Text
            .Col = 8
                .Text = FormatNumber(CDbl(montov - total.Text), 2, , , vbTrue)
            .Col = 0
              
            oTeclado.Key_Right
        End With
        
    End If
    
    Forma.Calculo
    
End Sub

Private Sub grid_denominaciones_DblClick()
    Dim mValor As Variant
    If grid_denominaciones.Row >= 1 Then
        Select Case grid_denominaciones.Col
            Case ColGridDenomina.dBanco, ColGridDenomina.dNumero
                Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
            Case ColGridDenomina.dMonto, ColGridDenomina.dCodigoDenominacion
                'Mvalor = grid_denominaciones.TextMatrix(grid_denominaciones.Row, 0)
                'If IsNumeric(Mvalor) Then
                    'If Mvalor > 0 Then
                    Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                'End If
        End Select
    End If
End Sub

Private Sub grid_denominaciones_KeyDown(KeyCode As Integer, Shift As Integer)
    If grid_denominaciones.Row >= 1 Then
        Select Case KeyCode
            Case vbKeyDelete
                If grid_denominaciones.Col = ColGridDenomina.dBanco Then
                    If grid_denominaciones.Rows > 2 Then
                        grid_denominaciones.RemoveItem (grid_denominaciones.Row)
                    Else
                        Call LimpiarFila(grid_denominaciones, grid_denominaciones.Row)
                    End If
                    Call TotalizarColumna(Me.grid_denominaciones)
                End If
            Case vbKeyF2
                If grid_denominaciones.Col = ColGridDenomina.dBanco Then
                    BuscarBancos
                End If
        End Select
    End If
End Sub

Private Sub grid_denominaciones_KeyPress(KeyAscii As Integer)
    
    Dim miVar As String
    
    If grid_denominaciones.Row >= 1 Then
        If KeyAscii >= 32 Then
            Select Case grid_denominaciones.Col
                Case ColGridDenomina.dBanco
                    Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                    txteditor.Text = Chr$(KeyAscii)
                    txteditor.SelStart = 1
                Case ColGridDenomina.dMonto, ColGridDenomina.dNumero
                    Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                    If IsNumeric(Chr(KeyAscii)) Then
                        txteditor.Text = Chr$(KeyAscii)
                        txteditor.SelStart = 1
                    End If
                    
            End Select
        ElseIf KeyAscii = vbKeyReturn Then
            If grid_denominaciones.Col = ColGridDenomina.dBanco And grid_denominaciones.Row > 1 Then
                If grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, ColGridDenomina.dCodigoBanco) <> Empty _
                    And grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, ColGridDenomina.dBanco) <> Empty Then
                        'Asignamos el banco de la fila de arriba
                        grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dBanco) = grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, ColGridDenomina.dBanco)
                        grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dCodigoBanco) = grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, ColGridDenomina.dCodigoBanco)
                        grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dCodigoDenominacion) = mDenominacion
                        grid_denominaciones.Col = ColGridDenomina.dNumero
                        grid_denominaciones.SetFocus
                End If
            End If
        
        End If
    End If
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case "TECLADO"
            If txteditor.Visible Then
Escribir:
                
                'Set CampoT = txteditor
                'configPosTecladoManual
                'TECLADO.Show vbModal
                TecladoPOS txteditor
            Else
                grid_denominaciones_DblClick
                txteditor.Text = ""
                GoTo Escribir
            End If
        Case "BUSCAR"
            txtEditor_KeyDown vbKeyEscape, 0
            grid_denominaciones_KeyDown vbKeyF2, 0
    End Select
End Sub

Private Sub txtEditor_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim mDato As Variant
    Dim Acum As Double
    Select Case KeyCode
        Case 13
            Call OcultarEditorTexto(Me.grid_denominaciones, Me.txteditor, CellRow, CellCol)
            mDato = txteditor.Text
        Case 27
            Call OcultarEditorTexto(grid_denominaciones, txteditor, CellRow, CellCol, True)
        Case vbKeyF2
            Call OcultarEditorTexto(grid_denominaciones, txteditor, CellRow, CellCol, True)
            grid_denominaciones_KeyDown KeyCode, Shift
    End Select
End Sub

Private Sub txtEditor_KeyPress(KeyAscii As Integer)
    If grid_denominaciones.Col = ColGridDenomina.dMonto Then
    Select Case KeyAscii
        Case 48 To 57
        Case 8
        Case Asc(","), Asc(".")
        Case Else
            KeyAscii = 0
    End Select
    End If
End Sub

Private Sub txtEditor_LostFocus()
    If IsNumeric(txteditor.Text) Then
        Call OcultarEditorTexto(grid_denominaciones, txteditor, CellRow, CellCol)
    End If
End Sub

Private Sub MostrarEditorTexto(frm As Form, pGrd As Object, ByRef txteditor As Object, ByRef CellRow As Long, ByRef CellCol As Long, Optional pForzar As Boolean = False)
    With pGrd
        
        .RowSel = .Row
        .ColSel = .Col
        txteditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - frm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - frm.ScaleY(1, vbPixels, vbTwips)
        txteditor.Text = .Text
        txteditor.Visible = True
        txteditor.ZOrder
        txteditor.SetFocus
        CellRow = .Row
        CellCol = .Col
    
     End With
End Sub

Private Sub OcultarEditorTexto(ByRef pGrd As MsFlexGrid, ByRef txteditor As TextBox, pRow As Long, pCol As Long, Optional Cancelar As Boolean = False)
    
    Dim mTipo As Integer
    Dim mValor As Variant, mValorAux As Variant
    Dim mDiferencia As Double
    
    On Error GoTo Errores
    
    If txteditor.Visible Then
        If Not Cancelar Then
            mValor = txteditor.Text
            Select Case pCol
                Case ColGridDenomina.dBanco  ' Banco
                    If mValor = " " Then BuscarBancos: txteditor.Visible = False: pGrd.SetFocus: Exit Sub
                    If mValor <> "" Then BuscarBancoGrid grid_denominaciones, mValor, pRow
                    If Val(grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dNumero)) <= 0 Then
                        grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dNumero) = "1.00"
                        grid_denominaciones.Col = ColGridDenomina.dMonto
                        If grid_denominaciones.Visible Then grid_denominaciones.SetFocus
                        txteditor.Visible = False
                        DoEvents
                        grid_denominaciones_KeyPress vbKey0
                        Exit Sub
                    End If
                Case ColGridDenomina.dNumero  ' Cantidad
                    If pGrd.TextMatrix(pRow, ColGridDenomina.dBanco) <> Empty _
                    And IsNumeric(mValor) Then
                        If Val(mValor) > 0 Then
                            pGrd.TextMatrix(pRow, pCol) = FormatNumber(mValor, 2, , , vbTrue)
                            If IsNumeric(pGrd.TextMatrix(pRow, ColGridDenomina.dMonto)) Then
                                pGrd.TextMatrix(pRow, ColGridDenomina.dTotal) = FormatNumber(mValor * CDbl(pGrd.TextMatrix(pRow, ColGridDenomina.dMonto)), 2, , , vbTrue)
                            Else
                                pGrd.TextMatrix(pRow, ColGridDenomina.dTotal) = FormatNumber(0, 2)
                            End If
                            Call TotalizarColumna(Me.grid_denominaciones)
                            pGrd.Col = ColGridDenomina.dMonto
                            pGrd.SetFocus
                        Else
                            grid_denominaciones.Col = ColGridDenomina.dBanco
                            grid_denominaciones_KeyDown vbKeyDelete, 0
                        End If
                    Else
                        pGrd.TextMatrix(pRow, pCol) = Empty
                    End If
                Case ColGridDenomina.dMonto  ' Monto
                    If IsNumeric(mValor) _
                    And pGrd.TextMatrix(pRow, ColGridDenomina.dBanco) <> Empty Then
                        pGrd.TextMatrix(pRow, pCol) = FormatNumber(mValor, 2, , , vbTrue)
                        If IsNumeric(pGrd.TextMatrix(pRow, ColGridDenomina.dNumero)) Then
                            pGrd.TextMatrix(pRow, ColGridDenomina.dTotal) = FormatNumber(mValor * CDbl(pGrd.TextMatrix(pRow, ColGridDenomina.dNumero)), 2, , , vbTrue)
                        Else
                            pGrd.TextMatrix(pRow, ColGridDenomina.dTotal) = FormatNumber(0, 2)
                        End If
                        Call TotalizarColumna(Me.grid_denominaciones)
                        If ValidarFila(pGrd, pRow) Then
                            If pRow = pGrd.Rows - 1 Then pGrd.Rows = pGrd.Rows + 1
                            pGrd.Row = pGrd.Rows - 1
                            pGrd.Col = ColGridDenomina.dBanco
                            pGrd.LeftCol = ColGridDenomina.dBanco
                            pGrd.SetFocus
                        End If
                    Else
                        pGrd.TextMatrix(pRow, pCol) = FormatNumber(0, 2)
                    End If
            End Select
            txteditor.Visible = False
            pGrd.SetFocus
        Else
            txteditor.Visible = False
            If txteditor.Visible And txteditor.Enabled Then pGrd.SetFocus
        End If
    End If
    
    Exit Sub
    
Errores:
    
End Sub

Private Function ValidarFila(pGrd As MsFlexGrid, pFila As Long) As Boolean
    
    Dim Mcol As Long, mValidar As Boolean
    
    mValidar = True
    
    Do While Mcol <= pGrd.Cols - 1 And mValidar
        Select Case Mcol
            Case ColGridDenomina.dNumero, ColGridDenomina.dMonto, ColGridDenomina.dTotal
                If IsNumeric(pGrd.TextMatrix(pFila, Mcol)) Then
                    mValidar = CDbl(pGrd.TextMatrix(pFila, Mcol)) > 0
                End If
            Case Else
                mValidar = pGrd.TextMatrix(pFila, Mcol) <> Empty
        End Select
        Mcol = Mcol + 1
    Loop
    
    ValidarFila = mValidar
    
End Function

Private Sub LimpiarFila(pGrd As MsFlexGrid, pFila As Long)
    
    Dim Mcol As Long
    
    For Mcol = 0 To pGrd.Cols - 1
        pGrd.TextMatrix(pFila, Mcol) = Empty
    Next Mcol
    
End Sub

Private Sub BuscarBancos()
    
    Dim mResul As Variant, mSQL As String
    
    On Error GoTo Errores
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    mSQL = "Select c_codigo, c_descripcio from ma_bancos "
    
    Frm_Super_Consultas.Inicializar mSQL, "B A N C O S", ConexionVAD10Red

    Frm_Super_Consultas.CampoCodigo = "c_codigo"
    Frm_Super_Consultas.Add_ItemLabels "Codigo", "c_codigo", 2640, 0
    Frm_Super_Consultas.Add_ItemLabels "Descripcion", "c_descripcio", 8520, 0
    
    Frm_Super_Consultas.Add_ItemSearching "Descripcion", "c_descripcio"
    Frm_Super_Consultas.Add_ItemSearching "Codigo", "c_codigo"
    
    Frm_Super_Consultas.txtDato.Text = "%"
    Frm_Super_Consultas.CmdBuscar(0).Value = True
    Frm_Super_Consultas.GRID.Font.Size = 20
    Frm_Super_Consultas.Show vbModal
    
    If Not IsEmpty(Frm_Super_Consultas.arrResultado) Then 'If Not IsEmpty(mResul) Then
        If Trim(Frm_Super_Consultas.arrResultado(0)) <> Empty Then
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dBanco) = Frm_Super_Consultas.arrResultado(1)
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dCodigoBanco) = Frm_Super_Consultas.arrResultado(0)
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dCodigoDenominacion) = mDenominacion
            grid_denominaciones.Col = ColGridDenomina.dNumero
            If Val(grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dNumero)) <= 0 Then
                grid_denominaciones.TextMatrix(grid_denominaciones.Row, ColGridDenomina.dNumero) = "1.00"
                grid_denominaciones.Col = ColGridDenomina.dMonto
                If grid_denominaciones.Visible Then grid_denominaciones.SetFocus
                grid_denominaciones_KeyPress vbKey0
                Exit Sub
            End If
            grid_denominaciones.SetFocus
        End If
    End If
    
    Exit Sub
    
Errores:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(BuscarBancos)"
    
End Sub

Private Sub BuscarBancoGrid(pGrd As MsFlexGrid, pvalor, pFila As Long)
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mSqlDet As String
    
    On Error GoTo Errores
    
    mSQL = "Select c_codigo, c_descripcio " & _
    "from ma_bancos " & _
    "where c_codigo = '" & Trim(pvalor) & "' " & _
    "or c_descripcio = '" & Trim(pvalor) & "' "
    
    mRs.Open mSQL, ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        pGrd.TextMatrix(pFila, ColGridDenomina.dBanco) = mRs!c_Descripcio
        pGrd.TextMatrix(pFila, ColGridDenomina.dCodigoBanco) = mRs!c_Codigo
        pGrd.TextMatrix(pFila, ColGridDenomina.dCodigoDenominacion) = mDenominacion
        pGrd.Col = ColGridDenomina.dNumero
        pGrd.SetFocus
    Else
        LimpiarFila pGrd, pFila
    End If
    
    mRs.Close
    
    Exit Sub
    
Errores:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(BuscarBancoGrid)"
    
End Sub

Private Function VerificarPaso(pMensaje As String) As Boolean
    Dim Obj As Object
    
    Set Obj = CreateObject("recsun.OBJ_MENSAJERIA")
    Obj.Mensaje pMensaje, True
    VerificarPaso = Obj.PressBoton
    Set Obj = Nothing
End Function

Private Sub GrabarDenominaciones()
    
    Dim mCn As New ADODB.Connection
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mSqlDet As String
    Dim mFila As Long, mUltimaFila As Long
    Dim mInicio As Boolean
    
    On Error GoTo Err_Manager
    
    mInicio = False
    mUltimaFila = 0
    
    For mFila = 1 To grid_denominaciones.Rows - 1
        If mFila = grid_denominaciones.Rows - 1 Then
            If grid_denominaciones.TextMatrix(mFila, ColGridDenomina.dBanco) <> Empty _
                And grid_denominaciones.TextMatrix(mFila, ColGridDenomina.dTotal) <> Empty Then
                If Not ValidarFila(grid_denominaciones, mFila) Then
                    Mensaje True, "Faltan por llenar datos en la fila  " & mFila
                    Exit Sub
                End If
                mUltimaFila = mFila
            End If
        Else
            If Not ValidarFila(grid_denominaciones, mFila) Then
                Mensaje True, "Faltan por llenar datos en la fila  " & mFila
                Exit Sub
            End If
            mUltimaFila = mFila
        End If
    Next mFila
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    mSQL = "SELECT * FROM TR_DENOMINA_TEMP " & _
    "WHERE 1 = 2"
    
    mCn.Open ConexionVAD20Red.ConnectionString
    mCn.BeginTrans
    
    mInicio = True
    
    mCn.Execute _
    "DELETE FROM TR_DENOMINA_TEMP " & _
    "WHERE Caja = '" & NumDoc & "' " & _
    "AND Usuario = '" & LcCajero & "' " & _
    "AND Turno = " & CDec(Turno) & " " & _
    "AND CodMoneda = '" & Moneda_Cod & "' " & _
    "AND CodDenomina = '" & mDenominacion & "' "
    
    mRs.Open mSQL, mCn, adOpenDynamic, adLockOptimistic, adCmdText
        
        For mFila = 1 To mUltimaFila
            
            mRs.AddNew
                
                mRs!CodMoneda = Moneda_Cod
                mRs!CodDenomina = mDenominacion
                mRs!DesDenomina = Denomina_Des
                mRs!Cantidad = CDec(grid_denominaciones.TextMatrix(mFila, ColGridDenomina.dNumero))
                mRs!Valor = CDec(grid_denominaciones.TextMatrix(mFila, ColGridDenomina.dMonto))
                mRs!Factor = Moneda_Fac
                
                mRs!Caja = NumDoc
                mRs!Usuario = LcCajero
                mRs!Turno = CDec(Turno)
                
                mRs!CodigoBanco = grid_denominaciones.TextMatrix(mFila, ColGridDenomina.dCodigoBanco)
                
            mRs.Update
            
        Next mFila
        
    mRs.Close
    
    mCn.CommitTrans
    mInicio = False
    
    mGrabo = True
    
    Unload Me
    
    Exit Sub
    
Err_Manager:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If mInicio Then
        mCn.RollbackTrans
        mInicio = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GrabarDenominaciones)"
    
End Sub

Private Sub TotalizarColumna(pGrd As MsFlexGrid)
    
    Dim mCant As Double, mTotal As Double
    Dim mFila As Long
    
    mCant = 0
    mTotal = 0
    
    For mFila = 1 To grid_denominaciones.Rows - 1
        If IsNumeric(pGrd.TextMatrix(mFila, ColGridDenomina.dNumero)) Then
            mCant = mCant + CDbl(pGrd.TextMatrix(mFila, ColGridDenomina.dNumero))
        End If
        If IsNumeric(pGrd.TextMatrix(mFila, ColGridDenomina.dTotal)) Then
            mTotal = mTotal + CDbl(pGrd.TextMatrix(mFila, ColGridDenomina.dTotal))
        End If
    Next mFila
    
    total.Text = FormatNumber(mTotal, 2, , , vbTrue)
    totalcantidad.Text = FormatNumber(mCant, 2, , , vbTrue)
    
End Sub

Private Sub CargarDenominaciones(ByVal pDenominacion As String)
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mFila As Integer
    
    mSQL = "SELECT TR.*, BAN.c_Descripcio AS Banco " & _
    "FROM TR_DENOMINA_TEMP TR " & _
    "LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_BANCOS BAN " & _
    "ON BAN.c_Codigo = TR.CodigoBanco " & _
    "WHERE TR.Caja = '" & NumDoc & "' " & _
    "AND TR.Usuario = '" & LcCajero & "' " & _
    "AND TR.Turno = " & CDec(Turno) & " " & _
    "AND TR.CodMoneda = '" & Moneda_Cod & "' " & _
    "AND TR.CodDenomina = '" & pDenominacion & "'" & _
    "AND TR.Valor > 0 " & _
    "AND TR.Cantidad > 0 "
    
    mRs.Open mSQL, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    With grid_denominaciones
        
        Do While Not mRs.EOF
            
            mFila = mFila + 1
            
            If Not IsNull(mRs!Banco) Then
                
                .TextMatrix(mFila, ColGridDenomina.dBanco) = mRs!Banco
                .TextMatrix(mFila, ColGridDenomina.dCodigoBanco) = mRs!CodigoBanco
                .TextMatrix(mFila, ColGridDenomina.dCodigoDenominacion) = pDenominacion
                .TextMatrix(mFila, ColGridDenomina.dMonto) = FormatNumber(mRs!Valor, 2)
                .TextMatrix(mFila, ColGridDenomina.dNumero) = FormatNumber(mRs!Cantidad, 2)
                .TextMatrix(mFila, ColGridDenomina.dTotal) = FormatNumber(mRs!Cantidad * mRs!Valor, 2)
                
                grid_denominaciones.Rows = grid_denominaciones.Rows + 1
                
            End If
            
            mRs.MoveNext
            
        Loop
        
    End With
    
    Call TotalizarColumna(grid_denominaciones)
    
    grid_denominaciones.Row = grid_denominaciones.Rows - 1
    grid_denominaciones.Col = ColGridDenomina.dBanco
    
End Sub
