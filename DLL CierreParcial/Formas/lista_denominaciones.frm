VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form lista_denominaciones 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7425
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   6570
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   6570
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   6945
      TabIndex        =   5
      Top             =   420
      Width           =   6975
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   6
         Top             =   120
         Width           =   6450
         _ExtentX        =   11377
         _ExtentY        =   1429
         ButtonWidth     =   1561
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   7
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               Object.ToolTipText     =   "Teclado"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9075
         TabIndex        =   3
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.TextBox txtedit 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   2520
      MaxLength       =   6
      TabIndex        =   1
      Top             =   2160
      Width           =   1365
   End
   Begin MSFlexGridLib.MSFlexGrid grid_denominaciones 
      Height          =   5445
      Left            =   180
      TabIndex        =   0
      Top             =   1740
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   9604
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   450
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      ScrollBars      =   2
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   5940
      Top             =   1530
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "lista_denominaciones.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "lista_denominaciones.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "lista_denominaciones.frx":3B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "lista_denominaciones.frx":58B6
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "lista_denominaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private SelectedRow As Long

Private bDepositosCP As Boolean

Private mTabla As String

Public Property Let DepositosCP(ByVal pvalor As Boolean)
    DepositosCP = pvalor
End Property

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        
        Select Case KeyCode
            Case Is = vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
            Case Is = vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
            Case Is = vbKeyA
        End Select
        
    Else
        
        Select Case KeyCode
            
            Case Is = vbKeyF12
                
                If Mensaje(False, "Al salir, los cambios realizados no ser�n grabados." _
                & vbNewLine & "�Est� seguro de salir?") Then
                    
                    LcGrabar = False
                    
                    If RxDenomina.State = adStateOpen Then
                        RxDenomina.Close
                    End If
                    
                    Unload Me
                    
                    Exit Sub
                    
                End If
                
            Case Is = vbKeyF4
                
                With grid_denominaciones
                    
                    Monto = 0
                    
                    For i = 1 To .Rows - 1
                        
                        .Row = i
                        .Col = 4
                            
                            If Not IsNumeric(.Text) Then
                                .Text = 0
                            End If
                            
                            Monto = Monto + CDbl(.Text)
                            
                    Next
                    
                End With
                
                With Forma.monitor
                    .Row = .RowSel
                    .Col = 3
                        montov = CDbl(.Text)
                    .Col = 4
                        .Text = FormatNumber(Monto, 2)
                    .Col = 0
                    oTeclado.Key_Right
                End With
                
                LcGrabar = True
                
                If RxDenomina.State = adStateOpen Then
                    RxDenomina.Close
                End If
                
                Unload Me
                
        End Select
        
    End If
    
End Sub

Private Sub Form_Load()
    
    LcGrabar = False
    
'    With grid_denominaciones
'        .Row = 0
'        .Col = 0
'            .Text = "Codigo"
'        .Col = 1
'            .Text = "Denominaci�n"
'        .Col = 2
'            .Text = "Valor"
'        .Col = 3
'            .Text = "Cantidad"
'        .Col = 4
'            .Text = "Total"
'        .ColWidth(0) = 0
'        .ColWidth(1) = 2000
'        .ColWidth(2) = 0
'        .ColWidth(3) = 1900
'        .ColWidth(4) = 1900
'
'   End With
    
    With grid_denominaciones
        .RowHeightMin = 600
        .Row = 0
        .Col = 0
            .Text = Stellar_Mensaje(15002, True)
        .Col = 1
            .Text = Stellar_Mensaje(16047, True)
        .Col = 2
            .Text = Stellar_Mensaje(2045, True)
        .Col = 3
            .Text = Stellar_Mensaje(3001, True)
        .Col = 4
            .Text = Stellar_Mensaje(141, True)
        .ColWidth(0) = 0
        .ColWidth(1) = 2000
        .ColWidth(2) = 0
        .ColWidth(3) = 1900
        .ColWidth(4) = 1900
        
    End With
    
    'Me.lbl_Organizacion.Caption = "Denominaciones"
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(16047, True)
    
    Me.Toolbar1.Buttons(1).Caption = "F4" & " " & Stellar_Mensaje(103, True) 'grabar
    Me.Toolbar1.Buttons(3).Caption = "F12" & " " & Stellar_Mensaje(54, True) 'salir
    Me.Toolbar1.Buttons(5).Caption = Stellar_Mensaje(7, True) 'ayuda
    Me.Toolbar1.Buttons(7).Caption = Stellar_Mensaje(16040, True) 'teclado
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    If bDepositosCP Then
        mTabla = "TR_DENOMINA_TEMP_DEPOSITOS"
    Else
        mTabla = "TR_DENOMINA_TEMP"
    End If
        
    Call Cargar_Denomina
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not ValidarConexion(ConexionVAD20RedGrabar) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    Call ValidarConexion(ConexionVAD10Red)
    
    If LcGrabar = False Then
        
        ConexionVAD20RedGrabar.Execute _
        "UPDATE " & mTabla & " " & _
        "SET Cantidad = 0 " & _
        "WHERE CodMoneda = '" & Moneda_Cod & "' " & _
        "AND Caja = '" & nCaja & "' " & _
        "AND CodigoBanco = '' "
        
        With Forma.monitor
            .Row = .RowSel
            .Col = 4
                .Text = FormatNumber(0, 2)
            .Col = 0
            oTeclado.Key_Right
        End With
        
    End If
    
    Forma.Calculo
    
End Sub

Private Sub grid_denominaciones_DblClick()
    If grid_denominaciones.Col = 3 Then
        SelectedRow = grid_denominaciones.Row
        msflexgridedit grid_denominaciones, txtedit, 0
        txtedit.SelStart = 0
        txtedit.SelLength = Len(txtedit.Text)
    End If
End Sub

Private Sub grid_denominaciones_KeyPress(KeyAscii As Integer)
    
    With grid_denominaciones
        
        Select Case .Col
            
            Case 3                     ''***** Cantidad
                
                Select Case KeyAscii
                    
                    Case 48 To 57, 8
                        
                        SelectedRow = grid_denominaciones.Row
                        txtedit.MaxLength = 14
                        msflexgridedit grid_denominaciones, txtedit, KeyAscii
                    
                    Case Is = vbKeyReturn
                        .SetFocus
                        .Col = 3
                        grid_denominaciones_DblClick
                    
                    Case Else
                        Beep
                        .Col = 3
                    
                End Select
                
        End Select
        
    End With
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case Is = "Grabar"
            Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "Salir"
            Call Form_KeyDown(vbKeyF12, 0)
            
        Case Is = "Teclado"
            If txtedit.Visible Then
Escribir:
                'Set CampoT = txtedit
                'configPosTecladoManual
                'TECLADO.Show vbModal
                TecladoPOS txtedit
            Else
                grid_denominaciones_DblClick
                txtedit.Text = Empty
                GoTo Escribir
            End If
    End Select
End Sub

Private Sub Cargar_Denomina()
    
    If RxDenomina.State = adStateOpen Then
        RxDenomina.Close
    End If
    
    RxDenomina.Open _
    "SELECT * FROM " & mTabla & " " & _
    "WHERE CodMoneda = '" & Moneda_Cod & "' " & _
    "AND CodigoBanco = '' " & _
    "AND Valor > 0 " & _
    "AND Caja = '" & nCaja & "' " & _
    "AND Usuario = '" & LcCajero & "' " & _
    IIf(Not bDepositosCP, _
    "AND Turno = " & CDec(Turno) & " ", Empty) & _
    "ORDER BY Factor DESC, Valor DESC ", _
    ConexionVAD20Red, adOpenDynamic, adLockBatchOptimistic
    
    If Not RxDenomina.EOF Then
        
        With grid_denominaciones
            
            i = 1
            
            Do Until RxDenomina.EOF
                .Row = i
                .Col = 0
                    .Text = RxDenomina!CodDenomina
                .Col = 1
                    .Text = Trim(RxDenomina!DesDenomina)
                    .CellAlignment = 7
                .Col = 2
                    .Text = RxDenomina!Valor
                .Col = 3
                    .Text = RxDenomina!Cantidad
                    .CellAlignment = 7
                .Col = 4
                    .Text = FormatNumber(RxDenomina!Valor * RxDenomina!Cantidad, 2)
                    .CellAlignment = 7
                RxDenomina.MoveNext
                i = i + 1
                .Rows = .Rows + 1
            Loop
            
            .Rows = .Rows - 1
            .Row = 1
            .Col = 3
            
        End With
        
    Else
        'MsgBox "No existen valores para esta denominaci�n.", vbCritical + vbOKOnly, "Mensaje Stellar"
        Mensaje True, Stellar_Mensaje(16051, True)
    End If
    
End Sub

Private Sub grid_denominaciones_GotFocus()
    
    If txtedit.Visible = False Then
        Exit Sub
    End If
    
    txtedit.Visible = False
    
End Sub

Sub msflexgridedit(MsFlexGrid As Control, Edt As Control, KeyAscii As Integer)
    Select Case KeyAscii
        Case 0 To 32
           Edt = MsFlexGrid
            'edt.SelStart = 1000
        Case Else
            Edt = Chr(KeyAscii)
            Edt.SelStart = 1
    End Select
    Edt.Move MsFlexGrid.CellLeft + 170, MsFlexGrid.CellTop + 1720, MsFlexGrid.CellWidth - 15, MsFlexGrid.CellHeight
    Edt.Visible = True
    Edt.SetFocus
End Sub

Private Sub txtedit_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57, 8
        
        Case Is = vbKeyReturn
            KeyAscii = 0
            oTeclado.Key_Tab
        
        Case Else
            Beep
            KeyAscii = 0
        
    End Select
End Sub

Private Sub txtedit_LostFocus()
    
    If Not ValidarConexion(ConexionVAD20Red) Then
        'Mensaje True, "Existe una falla de conexi�n con el servidor. Verifique antes de proceder e intente nuevamente."
        Mensaje True, StellarMensaje(16054)
        Exit Sub
    End If
    
    With grid_denominaciones
        
        Dim RsCambio As New ADODB.Recordset, Sql As String
        
        Select Case .ColSel
            
            Case Is = 3         ''***** Cantidad
                
                .Row = SelectedRow
                
                If Not IsNumeric(txtedit.Text) Then
                    txtedit.Text = 0
                End If
                
                txtedit.Text = Format(txtedit, "#####0")
                    .Text = txtedit.Text
                    If CDbl(.Text) <> 0 Then
                        .CellForeColor = azul
                    Else
                        .CellForeColor = negro
                    End If
                .Col = 0
                    denomina_sel = .Text
                .Col = 2
                    bs_recibidos = CDbl(.Text) * CDbl(txtedit)
                .Col = 4
                    .Text = FormatNumber(bs_recibidos, 2)
                    
                ' si el recordset esta todo el tiempo abierto y se inicia una transaccion en otra maquina puede causar problemas
                ' pendiente por revisar consecuencias
                'rxdenomina.Requery
                'rxdenomina.MoveFirst
                'rxdenomina.Find "coddenomina = " & denomina_sel, , adSearchForward, 1
                'rxdenomina.Update
                '    rxdenomina!cantidad = CDbl(txtedit.Text)
                'rxdenomina.UpdateBatch
                
                Sql = "SELECT * FROM " & mTabla & " " & _
                "WHERE Caja = '" & nCaja & "' " & _
                "AND CodigoBanco = '' " & _
                "AND Valor > 0 " & _
                "AND Turno = " & CDec(Turno) & " " & _
                "AND CodMoneda = '" & Moneda_Cod & "' " & _
                "AND CodDenomina = '" & denomina_sel & "' "
                
                RsCambio.Open Sql, ConexionVAD20Red, adOpenDynamic, adLockBatchOptimistic
                
                If Not RsCambio.EOF Then
                    RsCambio.Update
                    RsCambio!Cantidad = CDbl(txtedit.Text)
                Else
                    Exit Sub
                End If
                
                ' RsCambio.Update
                RsCambio.UpdateBatch
                oTeclado.Key_Down
                .Col = 3
                
                If .Visible And .Enabled Then .SetFocus
                
                RsCambio.Close
                RsCambio.ActiveConnection = Nothing
                
        End Select
        
    End With
    
End Sub
