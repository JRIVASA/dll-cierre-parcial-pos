VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmInputBox 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5445
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7290
   ControlBox      =   0   'False
   Icon            =   "FrmInputBox.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5445
   ScaleWidth      =   7290
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox PlaceHolder 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   2640
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.TextBox PlaceHolderScroll 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   2880
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   3720
      Picture         =   "FrmInputBox.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   4050
      Width           =   1095
   End
   Begin VB.TextBox UserInputScroll 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   2280
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.TextBox UserInput 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   2040
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.TextBox RequestScroll 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   250
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   1320
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.TextBox Request 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   870
      Left            =   250
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   960
      Visible         =   0   'False
      Width           =   6800
   End
   Begin VB.PictureBox Medir 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      ScaleHeight     =   195
      ScaleWidth      =   6735
      TabIndex        =   4
      Top             =   2160
      Visible         =   0   'False
      Width           =   6800
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   2160
      TabIndex        =   0
      Top             =   1080
      Width           =   6795
      _ExtentX        =   11986
      _ExtentY        =   53
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   720
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7320
      Begin VB.Image CmdClose 
         Height          =   480
         Left            =   6660
         Picture         =   "FrmInputBox.frx":800C
         Top             =   120
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Stellar isBUSINESS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   735
         Left            =   195
         TabIndex        =   3
         Top             =   0
         Width           =   6450
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7275
         TabIndex        =   2
         Top             =   75
         Width           =   1935
      End
   End
   Begin VB.CommandButton ContainerTeclado 
      Appearance      =   0  'Flat
      Caption         =   "Teclado"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   4050
      Width           =   1095
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   5220
      MouseIcon       =   "FrmInputBox.frx":9D8E
      MousePointer    =   99  'Custom
      Picture         =   "FrmInputBox.frx":A098
      Stretch         =   -1  'True
      Top             =   4440
      Width           =   600
   End
End
Attribute VB_Name = "FrmInputBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ESTE FORMULARIO ES PARA SOLICITAR DATOS DESDE UN TEXTBOX
' DE MANERA SIMILAR A LA FUNCION INPUTBOX, SIN EMBARGO,
' ESTE POSEE UN DISE�O PERSONALIZABLE.

' EL TEXTBOX DONDE EL USUARIO ESCRIBE PUEDE CAMBIAR
' DE ACUERDO A LA DETECCION DE LOS CAMBIOS DE LINEA.
' SI EL USUARIO ESCRIBE MAS LINEAS QUE LAS CORRESPONDIENTES
' A UserInputMaxLines, ENTONCES APARECE UN TEXTBOX CON SCROLL BAR
' VERTICAL.

' ESTO QUIERE DECIR QUE EL INPUT DEL USUARIO ES ILIMITADO, SIN EMBARGO
' VISUALMENTE SOLO PUEDE OBSERVAR DICHA CANTIDAD DE LINEAS PERO PUEDE
' HACER SCROLL.

' CON RESPECTO AL TEXTO DE SOLICITUD / TITULO, FUNCIONA DE LA MISMA MANERA, SIN EMBARGO
' SE DEBE CERTIFICAR EL ASPECTO DESEADO DEL TITULO, A�ADIENDO ESPACIOS POR EJEMPLO
' DEBIDO A QUE ES IMPOSIBLE CALCULAR CON EXACTITUD LA CANTIDAD DE LINEAS QUE SE DEBEN
' MOSTRAR AL USUARIO DEBIDO A LA FUNCIONALIDAD WORD WRAP DEL TEXTBOX.

' EL WORD WRAP HACE QUE VISUALMENTE SE COLOQUEN PALABRAS LARGAS EN NUEVAS LINEAS DEL TEXTBOX PARA NO DEJAR
' UN ASPECTO DE PALABRA CORTADA, SIN EMBARGO, LA FUNCIONALIDAD DE DETECCION DE LONGITUD DEL TEXTO NO TOMA
' EN CUENTA LAS MODIFICACIONES VISUALES QUE OCURREN EN EL TEXTBOX A CAUSA DEL WORD WRAP,
' DE AHI QUE HAYA DISCREPANCIA VISUAL EN ALGUNOS CASOS

' POR EJEMPLO, SI ANTES DE LLAMAR A ESTE FORM, SE LE COLOCA COMO REQUEST.TEXT LO SIGUIENTE:

'"INGRESE TODA LA INFORMACION QUE PUEDA PROPORCIONAR ACERCA DE SI MISMO CON RESPECTO A ***PRUEBA***"

' *** PRUEBA *** NO SE MOSTRARA, NI SE GENERAR� UNA NUEVA LINEA...

' EN CAMBIO SI COLOCAMOS:

'"INGRESE TODA LA INFORMACION QUE PUEDA PROPORCIONAR ACERCA DE SI MISMO CON RESPECTO A     ***PRUEBA***"

' SE DETECTA QUE SE ALCANZO EL MAXIMO DE LINEAS, Y POR LO TANTO SE MUESTRA EL TEXTBOX CON SCROLL.


Public PressEnter As Boolean

Public DefaultInput As String
Public DefaultOutput As String
Public OptionalPlaceHolder As String
Public InputData As String
Public TecladoAutomatico As Boolean
Public AllowEmptyResp As Boolean

Private Const RequestMaxLines = 3
Private Const UserInputMaxLines = 4

Private mArrTabStop() As Boolean

Public EvitarActivate As Boolean

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub aceptar_Click()
    
    If UserInput.Visible Then
        InputData = UserInput.Text
    ElseIf UserInputScroll.Visible Then
        InputData = UserInputScroll.Text
    End If
    
    Unload Me
    
End Sub

Private Sub CmdClose_Click()
    If Not CmdClose.Visible Then Exit Sub
    InputData = DefaultOutput
    Unload Me
End Sub

Private Sub CmdTeclado_Click()
    
    On Error Resume Next
    
    CampoT.SelStart = Len(CampoT.Text)
    
    SafeFocus CampoT
    
    'If TipoTeclado = 0 Then
        
        'TECLADO.Show vbModal
        
    'ElseIf TipoTeclado = 1 Then
        
        'LlamarTeclado CampoT
        
    'ElseIf TipoTeclado = 2 Then
        
        If CampoT.Text = Empty Then
            CampoT.Text = " "
            DoEvents
        End If
        
        TecladoPOS CampoT
        
        If CampoT.Text = " " Then
            CampoT.Text = Empty
        End If
        
    'End If
    
    SafeFocus CampoT
    
End Sub

Private Sub ContainerTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub Form_Activate()
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    
    If RequestScroll.Visible Then
        RequestScroll_Change
    Else
        Request_Change
    End If
    
    UserInput.Text = DefaultInput
    
    If UserInputScroll.Visible Then
        UserInputScroll_Change
    Else
        UserInput_Change
    End If
    
    If OptionalPlaceHolder <> vbNullString Then
        PlaceHolder.Text = OptionalPlaceHolder
        BringPlaceHolder True
    End If
    
    ReposicionarElementos
    
    Dim TmpCtrlInput As Object
    
    If UserInputScroll.Visible Then
        Set TmpCtrlInput = UserInputScroll
    ElseIf UserInput.Visible Then
        Set TmpCtrlInput = UserInput
    End If
    
    If PuedeObtenerFoco(TmpCtrlInput) Then TmpCtrlInput.SetFocus
    
    TmpCtrlInput.SelStart = Len(TmpCtrlInput.Text)
    
    If DefaultInput <> vbNullString Then
        SeleccionarTexto TmpCtrlInput
    Else
        TmpCtrlInput.SelStart = Len(TmpCtrlInput.Text)
    End If
    
    Set CampoT = TmpCtrlInput
    
    If TecladoAutomatico Then
        CmdTeclado_Click
    End If
    
End Sub

Private Sub ReposicionarElementos()
    
    On Error Resume Next
    
    Dim TmpCtrlInput As Object
    
    If UserInputScroll.Visible Then
        Set TmpCtrlInput = UserInputScroll
    ElseIf UserInput.Visible Then
        Set TmpCtrlInput = UserInput
    End If
    
    Set CampoT = TmpCtrlInput
    
    Dim TmpCtrlRequest As Object
    
    If RequestScroll.Visible Then
        Set TmpCtrlRequest = RequestScroll
    ElseIf Request.Visible Then
        Set TmpCtrlRequest = Request
    End If
    
    Dim TmpCtrlPlaceHolder As Object
    
    If PlaceHolderScroll.Visible Then
        Set TmpCtrlPlaceHolder = PlaceHolderScroll
    ElseIf PlaceHolder.Visible Then
        Set TmpCtrlPlaceHolder = PlaceHolder
    End If
    
    UserInput.Top = (TmpCtrlRequest.Top + TmpCtrlRequest.Height) + 250
    
    UserInputScroll.Top = UserInput.Top
    PlaceHolder.Top = UserInput.Top
    PlaceHolderScroll.Top = UserInput.Top
    
    If TmpCtrlInput = vbNullString And Not TmpCtrlPlaceHolder Is Nothing Then
        ContainerTeclado.Top = (TmpCtrlPlaceHolder.Top + TmpCtrlPlaceHolder.Height) + 250
    Else
        ContainerTeclado.Top = (TmpCtrlInput.Top + TmpCtrlInput.Height) + 250
    End If
    
    Aceptar.Top = ContainerTeclado.Top

    Me.Height = (ContainerTeclado.Top + ContainerTeclado.Height) + 250
    
End Sub

Private Sub Form_Load()
    
    Me.Aceptar.Caption = StellarMensaje(5) 'Aceptar
    Me.ContainerTeclado.Caption = Stellar_Mensaje(16040, True) 'teclado
    LlenarArrayTabStop
    
    RequestScroll.Left = Request.Left
    RequestScroll.Top = Request.Top
    RequestScroll.Width = Request.Width
    RequestScroll.Height = Request.Height
    
    UserInputScroll.Left = UserInput.Left
    UserInputScroll.Top = UserInput.Top
    UserInputScroll.Width = UserInput.Width
    UserInputScroll.Height = UserInput.Height
    
    PlaceHolder.Left = UserInput.Left
    PlaceHolder.Top = UserInput.Top
    PlaceHolder.Width = UserInput.Width
    PlaceHolder.Height = UserInput.Height
    
    PlaceHolderScroll.Left = UserInput.Left
    PlaceHolderScroll.Top = UserInput.Top
    PlaceHolderScroll.Width = UserInput.Width
    PlaceHolderScroll.Height = UserInput.Height

    CmdTeclado.Visible = False: ContainerTeclado.Visible = True
    ContainerTeclado.Picture = CmdTeclado.Picture
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Me.Visible Then
        Cancel = 1
        Me.Hide
    Else
        Cancel = 0
        'Unload
    End If
    
End Sub

Private Sub PlaceHolder_Change()
        
    Tmp = Replace(PlaceHolder.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            PlaceHolder.Text = ""
        End If
    End If
        
    Medir.Font = PlaceHolder.Font
    Medir.FontSize = PlaceHolder.FontSize
    Medir.FontBold = PlaceHolder.FontBold
    Medir.FontItalic = PlaceHolder.FontItalic
    Medir.FontStrikethru = PlaceHolder.FontStrikethru
    Medir.FontUnderline = PlaceHolder.FontUnderline
    
    'TabCount = Len(placeholder.Text) - Len(Replace(placeholder.Text, vbTab, ""))
    LineCount = Len(PlaceHolder.Text) - Len(Replace(PlaceHolder.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(PlaceHolder.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = PlaceHolder.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(PlaceHolder.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D >= UserInputMaxLines Then
    
        PlaceHolderScroll.Height = (E) * (UserInputMaxLines)
        PlaceHolderScroll.Width = PlaceHolder.Width
        
        PlaceHolderScroll.Left = PlaceHolder.Left
        PlaceHolderScroll.Top = PlaceHolder.Top
        
        PlaceHolderScroll.Font = PlaceHolder.Font
        PlaceHolderScroll.FontSize = PlaceHolder.FontSize
        PlaceHolderScroll.FontBold = PlaceHolder.FontBold
        PlaceHolderScroll.FontItalic = PlaceHolder.FontItalic
        PlaceHolderScroll.FontStrikethru = PlaceHolder.FontStrikethru
        PlaceHolderScroll.FontUnderline = PlaceHolder.FontUnderline
        
        PlaceHolderScroll.Text = PlaceHolder.Text
        'PlaceHolderScroll.SelStart = Len(PlaceHolderScroll.Text)
        
        PlaceHolder.Visible = False: PlaceHolderScroll.Visible = True
                
    Else
        PlaceHolder.Visible = True
        PlaceHolder.Height = (E) * (D)
    End If
    
    'PressEnter = False
    
End Sub

Private Sub PlaceHolder_GotFocus()
    PlaceHolder_LostFocus
End Sub

Private Sub PlaceHolder_LostFocus()
    BringPlaceHolder False
End Sub

Private Sub PlaceHolderScroll_Change()
        
    Tmp = Replace(PlaceHolderScroll.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            PlaceHolderScroll.Text = ""
        End If
    End If
    
    Medir.Font = PlaceHolderScroll.Font
    Medir.FontSize = PlaceHolderScroll.FontSize
    Medir.FontBold = PlaceHolderScroll.FontBold
    Medir.FontItalic = PlaceHolderScroll.FontItalic
    Medir.FontStrikethru = PlaceHolderScroll.FontStrikethru
    Medir.FontUnderline = PlaceHolderScroll.FontUnderline
    
    'TabCount = Len(PlaceHolderScroll.Text) - Len(Replace(PlaceHolderScroll.Text, vbTab, ""))
    LineCount = Len(PlaceHolderScroll.Text) - Len(Replace(PlaceHolderScroll.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(PlaceHolderScroll.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = PlaceHolderScroll.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(PlaceHolderScroll.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D < UserInputMaxLines Then
    
        PlaceHolder.Text = PlaceHolderScroll.Text
        PlaceHolder.SelStart = Len(PlaceHolderScroll.Text)
        
        PlaceHolderScroll.Visible = False: PlaceHolder.Visible = True
                
    End If
    
    'PressEnter = False
        
End Sub

Private Sub PlaceHolderScroll_GotFocus()
    PlaceHolderScroll_LostFocus
End Sub

Private Sub PlaceHolderScroll_LostFocus()
    BringPlaceHolder False
End Sub

Private Sub Request_Change()
    
    Tmp = Replace(Request.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            Request.Text = ""
        End If
    End If
    
    Medir.Font = Request.Font
    Medir.FontSize = Request.FontSize
    Medir.FontBold = Request.FontBold
    Medir.FontItalic = Request.FontItalic
    Medir.FontStrikethru = Request.FontStrikethru
    Medir.FontUnderline = Request.FontUnderline
    
    'TabCount = Len(Request.Text) - Len(Replace(Request.Text, vbTab, ""))
    LineCount = Len(Request.Text) - Len(Replace(Request.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(Request.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = Request.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(Request.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D > RequestMaxLines Then
    
        RequestScroll.Height = (E) * (RequestMaxLines)
        RequestScroll.Width = Request.Width
        
        RequestScroll.Left = Request.Left
        RequestScroll.Top = Request.Top
        
        RequestScroll.Font = Request.Font
        RequestScroll.FontSize = Request.FontSize
        RequestScroll.FontBold = Request.FontBold
        RequestScroll.FontItalic = Request.FontItalic
        RequestScroll.FontStrikethru = Request.FontStrikethru
        RequestScroll.FontUnderline = Request.FontUnderline
        
        RequestScroll.Text = Request.Text
        RequestScroll.SelStart = Len(RequestScroll.Text)
        
        Request.Visible = False: RequestScroll.Visible = True: If PuedeObtenerFoco(RequestScroll) Then RequestScroll.SetFocus
                
    Else
        Request.Visible = True
        Request.Height = (E) * (D)
    End If
    
    'PressEnter = False
    
End Sub

Private Sub Request_GotFocus()
    Request_LostFocus
End Sub

Private Sub Request_LostFocus()
    If PuedeObtenerFoco(UserInput) Then
        UserInput.SetFocus
    ElseIf PuedeObtenerFoco(UserInputScroll) Then
        UserInputScroll.SetFocus
    End If
End Sub

Private Sub RequestScroll_Change()
    
    Tmp = Replace(RequestScroll.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            RequestScroll.Text = ""
        End If
    End If
    
    Medir.Font = RequestScroll.Font
    Medir.FontSize = RequestScroll.FontSize
    Medir.FontBold = RequestScroll.FontBold
    Medir.FontItalic = RequestScroll.FontItalic
    Medir.FontStrikethru = RequestScroll.FontStrikethru
    Medir.FontUnderline = RequestScroll.FontUnderline
    
    'TabCount = Len(RequestScroll.Text) - Len(Replace(RequestScroll.Text, vbTab, ""))
    LineCount = Len(RequestScroll.Text) - Len(Replace(RequestScroll.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(RequestScroll.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = RequestScroll.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(RequestScroll.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D < RequestMaxLines Then
    
        Request.Text = RequestScroll.Text
        Request.SelStart = Len(RequestScroll.Text)
        
        RequestScroll.Visible = False: Request.Visible = True: If PuedeObtenerFoco(Request) Then Request.SetFocus
                
    End If
    
    PressEnter = False
        
End Sub

Private Sub UserInput_Change()
    
    Tmp = Replace(UserInput.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            UserInput.Text = ""
        End If
    End If
        
    Medir.Font = UserInput.Font
    Medir.FontSize = UserInput.FontSize
    Medir.FontBold = UserInput.FontBold
    Medir.FontItalic = UserInput.FontItalic
    Medir.FontStrikethru = UserInput.FontStrikethru
    Medir.FontUnderline = UserInput.FontUnderline
    
    'TabCount = Len(UserInput.Text) - Len(Replace(UserInput.Text, vbTab, ""))
    LineCount = Len(UserInput.Text) - Len(Replace(UserInput.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(UserInput.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = UserInput.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(UserInput.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D >= UserInputMaxLines Then
        
        UserInputScroll.Height = (E) * (UserInputMaxLines)
        UserInputScroll.Width = UserInput.Width
        
        UserInputScroll.Left = UserInput.Left
        UserInputScroll.Top = UserInput.Top
        
        UserInputScroll.Font = UserInput.Font
        UserInputScroll.FontSize = UserInput.FontSize
        UserInputScroll.FontBold = UserInput.FontBold
        UserInputScroll.FontItalic = UserInput.FontItalic
        UserInputScroll.FontStrikethru = UserInput.FontStrikethru
        UserInputScroll.FontUnderline = UserInput.FontUnderline
        
        UserInputScroll.Text = UserInput.Text
        UserInputScroll.SelStart = Len(UserInputScroll.Text)
        
        UserInput.Visible = False: UserInputScroll.Visible = True
        If PuedeObtenerFoco(UserInputScroll) Then UserInputScroll.SetFocus
        Set CampoT = UserInputScroll
                        
        ReposicionarElementos
                        
    Else
        UserInput.Visible = True
        UserInput.Height = (E) * (D)
        UserInputScroll.Visible = False
        ReposicionarElementos
    End If
    
    PressEnter = False
    
    BringPlaceHolder True
    
End Sub

Private Sub BringPlaceHolder(Show As Boolean)
    
    Dim TmpCtrlPlaceHolder As Object
    
DetectarPlaceHolder:
    
   If PlaceHolderScroll.Visible Then
        Set TmpCtrlPlaceHolder = PlaceHolderScroll
    ElseIf PlaceHolder.Visible Then
        Set TmpCtrlPlaceHolder = PlaceHolder
    Else
        If OptionalPlaceHolder <> vbNullString Then
            PlaceHolder_Change
            If PlaceHolder.Visible Or PlaceHolderScroll.Visible Then
                GoTo DetectarPlaceHolder
            Else
                Exit Sub
            End If
        Else
            Exit Sub
        End If
    End If
    
    If Show Then
        If OptionalPlaceHolder <> vbNullString Then
            If UserInputScroll.Visible Then
                If UserInputScroll.Text = vbNullString Then
                    TmpCtrlPlaceHolder.ZOrder 0
                    ReposicionarElementos
                Else
                    UserInputScroll.ZOrder 0
                End If
            Else
                If UserInput.Text = vbNullString Then
                    TmpCtrlPlaceHolder.ZOrder 0
                    ReposicionarElementos
                Else
                    UserInput.ZOrder 0
                    TmpCtrlPlaceHolder.Visible = False
                End If
            End If
        Else
            TmpCtrlPlaceHolder.Visible = False
        End If
    Else
        If PuedeObtenerFoco(UserInputScroll) Then
            'UserInputScroll.ZOrder 0
            UserInputScroll.SetFocus
        ElseIf PuedeObtenerFoco(UserInput) Then
            'UserInput.ZOrder 0
            UserInput.SetFocus
        End If
    End If
    
End Sub

Private Sub UserInput_GotFocus()
    RemoverTabStop
    Set CampoT = UserInput
End Sub

Private Sub UserInput_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        ' Continue before NewLine.
        'If Not (UserInput.Text = vbNullString) Then
            Tmp = Replace(UserInput.Text, vbNewLine, vbNullString)
            'Debug.Print Tmp
            If (Len(Tmp) <= 0 And AllowEmptyResp) Or (Len(Tmp) > 0) Then
                Call aceptar_Click
            Else
                UserInput.Text = ""
            End If
        'End If
    ElseIf Shift = 0 And (KeyCode = vbKeyEscape Or KeyCode = vbKeyF12) Then
        Form_KeyDown KeyCode, Shift
    ElseIf Shift <> 0 And KeyCode = vbKeyReturn Then
        PressEnter = True
        ' Allows to add a NewLine.
    End If
    If Shift = vbCtrlMask Then
        If KeyCode = vbKeyE Or KeyCode = vbKeyA Then
            SeleccionarTexto CampoT
        ElseIf KeyCode = vbKeyC Then
            On Error Resume Next
            If CampoT.SelLength = 0 Then UserInput_KeyDown vbKeyE, vbCtrlMask
            CtrlC CampoT.SelText
            Mensaje True, StellarMensaje(64)
        End If
    End If
End Sub

Private Sub UserInput_LostFocus()
    RestablecerTabStop
End Sub

Private Sub UserInputScroll_Change()
        
    Tmp = Replace(UserInputScroll.Text, vbNewLine, vbNullString)
    
    If Tmp = vbNullString Then
        If Not PressEnter Then
            PressEnter = False
            UserInputScroll.Text = ""
        End If
    End If
    
    Medir.Font = UserInputScroll.Font
    Medir.FontSize = UserInputScroll.FontSize
    Medir.FontBold = UserInputScroll.FontBold
    Medir.FontItalic = UserInputScroll.FontItalic
    Medir.FontStrikethru = UserInputScroll.FontStrikethru
    Medir.FontUnderline = UserInputScroll.FontUnderline
    
    'TabCount = Len(UserInputScroll.Text) - Len(Replace(UserInputScroll.Text, vbTab, ""))
    LineCount = Len(UserInputScroll.Text) - Len(Replace(UserInputScroll.Text, vbNewLine, ""))
    
    A = Medir.TextWidth(UserInputScroll.Text)
    
    'A = A + (((Medir.TextWidth(String(15, " ")) - Fix(Medir.TextWidth(" ") / 2) - Medir.TextWidth(vbTab))) * TabCount)
    
    b = UserInputScroll.Width
    
    C = CDbl(A / b)
    
    D = IIf(A < b, 1, IIf(C > Fix(C), Fix(C) + 1, C))
    
    E = Medir.TextHeight(UserInputScroll.Text)
    
    If LineCount > 0 Then
        E = E / ((LineCount / 2) + 1)
        D = D + (LineCount / 2)
    End If
    
    If D < UserInputMaxLines Then
        UserInput.Text = UserInputScroll.Text
        UserInput.SelStart = Len(UserInputScroll.Text)
        
        UserInputScroll.Visible = False: UserInput.Visible = True
        If PuedeObtenerFoco(UserInput) Then UserInput.SetFocus
        Set CampoT = UserInput
        
        ReposicionarElementos
    Else
        Set CampoT = UserInputScroll
        'UserInputScroll.SelStart = Len(UserInputScroll.Text)
    End If
    
    PressEnter = False
    
    BringPlaceHolder True
        
End Sub

Private Sub LlenarArrayTabStop()
    
    On Error Resume Next
    
    ReDim mArrTabStop(Me.Controls.Count - 1) As Boolean
    
    For i = 0 To Me.Controls.Count - 1
        mArrTabStop(i) = Me.Controls(i).TabStop
    Next i
    
End Sub

Private Sub RemoverTabStop()

    On Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
        Me.Controls(i).TabStop = False
    Next i

End Sub

Private Sub RestablecerTabStop()

    On Error Resume Next
    
    For i = 0 To Me.Controls.Count - 1
       Me.Controls(i).TabStop = CBool(mArrTabStop(i))
    Next i
    
End Sub

Private Sub UserInputScroll_GotFocus()
    RemoverTabStop
    Set CampoT = UserInputScroll
End Sub

Private Sub UserInputScroll_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        ' Continue before NewLine.
        'If Not (UserInputScroll.Text = vbNullString) Then
            Tmp = Replace(UserInputScroll.Text, vbNewLine, vbNullString)
            'Debug.Print Tmp
            If (Len(Tmp) <= 0 And AllowEmptyResp) Or (Len(Tmp) > 0) Then
                aceptar_Click
            Else
                UserInputScroll.Text = ""
            End If
        'End If
    ElseIf Shift = 0 And (KeyCode = vbKeyEscape Or KeyCode = vbKeyF12) Then
        Form_KeyDown KeyCode, Shift
    ElseIf Shift <> 0 And KeyCode = vbKeyReturn Then
        PressEnter = True
        ' Allows to add a NewLine.
    End If
    If Shift = vbCtrlMask Then
        If KeyCode = vbKeyE Or KeyCode = vbKeyA Then
            SeleccionarTexto CampoT
        ElseIf KeyCode = vbKeyC Then
            On Error Resume Next
            If CampoT.SelLength = 0 Then UserInputScroll_KeyDown vbKeyE, vbCtrlMask
            CtrlC CampoT.SelText
            Mensaje True, StellarMensaje(64)
        End If
    End If
End Sub

Private Sub UserInputScroll_LostFocus()
    RestablecerTabStop
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Or KeyCode = vbKeyF12 Then
        CmdClose_Click
    End If
End Sub
