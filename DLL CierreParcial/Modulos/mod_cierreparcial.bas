Attribute VB_Name = "mod_cierreparcial"
'** DATOS GENERALES

Public ConexionVAD10Red As ADODB.Connection
Public ConexionVAD20Red As ADODB.Connection
Public ConexionVAD10Local As ADODB.Connection
Public ConexionVAD20Local As ADODB.Connection
Public ConexionVAD20RedGrabar As ADODB.Connection

Global Srv_Remote_BD_ADM        As String
Global Srv_Remote_BD_POS        As String
Global Srv_Local_BD_ADM         As String
Global Srv_Local_BD_POS         As String

Global mVarImprimirCopia        As Boolean

Public DllFiscal As Object
Public EsImpresoraFiscal As Boolean
Public Tipo_ImpresoraFiscal As TipoImpresoraFiscal

Public Denominacion_Sel As String
Public Caja As String
Public Localidad As String
Public Forma As Object
Public Forma1 As Object
Public LcGrabar As Boolean
Public Moneda_Sel As String
Public LcConsecu As String
Public Denomina_Cod As String
Public Denomina_Des As String
Public Denomina_Real As String

'** DATOS DE LA MONEDA
Public Moneda_Cod As String
Public Moneda_Des As String
Public Moneda_Fac As Double
Public Std_Decm As Integer

'** DATOS DEL CAJERO Y EL TURNO
Public LcCajero             As String
Public LcDesCajero          As String
Public NumDoc               As String
Public Turno As Double
Public nCaja As String

'** OTROS
'Public Bs_Recibidos As Double
Public Bs_Diferencia As Double
'Public Diferencia As Double
Public MontoLn As Double
Public MontoGr As Double
Public RxDenomina As New ADODB.Recordset
Public TipoPos_TecladoManual As Integer
Public CampoT As Object

Global Tecla_Pulsada    As Boolean
Global Retorno As Boolean
Global Uno As Boolean
Global ModalDisponible As Boolean
Global txtMensaje1 As String
Global txtMensaje2 As String

Global ImpresoraTicket_TipoLetra                            As String
Global ImpresoraTicket_TamañoFuente                         As Double
Global POS_IngresarEfectivoMontoDirecto                     As Integer
Global POS_DeclararEfectivoDesgloseAutomatico               As Integer
Global POS_CP_AutoDeposito_PorLote                          As Boolean

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Const ABM_GETTASKBARPOS = &H5

Private Declare Function SHAppBarMessage Lib "shell32.dll" _
(ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
(ByVal hdc As Long, ByVal nIndex As Long) As Long

Private Declare Function SendMessage Lib "user32" _
Alias "SendMessageA" (ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
lParam As Any) As Long

Private Declare Sub GetSystemTime Lib "kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Sub GetLocalTime Lib "kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Function GetTickCount Lib "kernel32" () As Long

Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SystemTime) As Long
Private Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SystemTime, lpFileTime As FILETIME) As Long

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Public Declare Sub ReleaseCapture Lib "user32" ()

Const HorizontalResolution = 8
Const VerticalResolution = 10

Public Enum Alienacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Public Enum TipoImpresoraFiscal
    mtBMC
    mtBematech
    mtEpson
    mtCamel
    mtVmax
    mtHKA
    mtDascomTally
    mtIBM
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Public Type SystemTime
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Sub Apertura_RecordsetC(ByRef rec As ADODB.Recordset)
    If rec.State = adStateOpen Then rec.Close
    rec.CursorLocation = adUseClient
End Sub

Public Sub Apertura_Recordset(rec As ADODB.Recordset)
    If rec.State = adStateOpen Then rec.Close
    rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(rec As ADODB.Recordset)
    If rec.State = adStateOpen Then rec.Close
    Set rec = Nothing
End Sub

Public Function BuscarFactorMonedaPredeterminada() As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = _
    "SELECT c_CodMoneda, n_Factor " & _
    "FROM MA_MONEDAS " & _
    "WHERE b_Preferencia = 1 "
    
    mRs.Open mSQL, ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarFactorMonedaPredeterminada = mRs!n_Factor
    Else
        BuscarFactorMonedaPredeterminada = 1
    End If
    
    mRs.Close
        
End Function

Public Sub MSGridAsign(ByRef GridObj As Object, Fila As Integer, Columna As Integer, Texto As Variant, Optional Tamano As Long = -1, Optional Alinear As Alienacion, Optional Formato As String = "")
    GridObj.Row = Fila
    GridObj.Col = Columna
    If Tamano >= 0 Then
        GridObj.ColWidth(GridObj.ColSel) = Tamano
    End If
    If Alinear > -1 Then
        GridObj.CellAlignment = Alinear
    End If
    If Formato <> "" Then
        Texto = Format(Texto, Formato)
    End If
    GridObj.Text = IIf(IsNull(Texto), "", Texto)
End Sub

Public Function Consecutivos(Campo_Cons As String, Conexion As ADODB.Connection) As String
    
    Dim Rec_Cons As New ADODB.Recordset
    Dim mRsCorrelativoSRV As New ADODB.Recordset
    Dim Actual As String
    
    'mmsql = "SELECT  name AS Tabla FROM  sysobjects WHERE     (xtype = 'U') AND (name <> 'dtproperties') AND (name = N'ma_correlativos')"
    'mRsCorrelativoSRV.Close
    'mRsCorrelativoSRV.CursorLocation = adUseServer
    'mRsCorrelativoSRV.Open mmsql, conexion, adOpenForwardOnly, adLockReadOnly
    'If mRsCorrelativoSRV.EOF Then
        
        Call Apertura_Recordset(Rec_Cons)
        
        Rec_Cons.Open _
        "select nu_valor " & _
        "from ma_correlativos " & _
        "where cu_campo = '" & Campo_Cons & "' ", _
        Conexion, adOpenDynamic, adLockPessimistic, adCmdText
        
        Rec_Cons.Update
            Actual = Rec_Cons.Fields(0).Value + 1
            Rec_Cons.Fields(0).Value = Actual
        Rec_Cons.UpdateBatch
        
        Consecutivos = Actual
        
    'Else
        'Debug.Print campo_cons
        'Consecutivos = CDbl(gClsDatos.NO_CONSECUTIVO(gCodProducto, conexion, campo_cons))
    'End If
    'mRsCorrelativoSRV.Close
    
    Rec_Cons.Close
    
End Function

Public Function BuscarValorDenominaAdm(pMoneda, pDenomina) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    Dim BuscarValorDenomina As Double
    
    mSQL = "SELECT * FROM MA_DENOMINACIONES " & vbNewLine & _
    "WHERE c_CodMoneda = '" & pMoneda & "' " & vbNewLine & _
    "AND c_CodDenomina = '" & pDenomina & "' "
    
    mRs.Open mSQL, ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorDenomina = mRs!n_Valor
    End If
    
    mRs.Close
    
    BuscarValorDenominaAdm = BuscarValorDenomina
    
End Function

Public Function Fecha() As String
'    If SRV_LOCAL <> "HGUERRERO" Then
        Fecha = Format(Year(Date), aaaa) & "-" & Format(Month(Date), "00") & "-" & Format(Day(Date), "00")
'    Else
'        FECHA = Day(Date) & "/" & Month(Date) & "/" & Year(Date)
'    End If
End Function

Function Imprimir_Retiro(TipoDoc As String)
    
    If EsImpresoraFiscal Then
        'If Tipo_ImpresoraFiscal = mtBMC _
        'Or Tipo_ImpresoraFiscal = mtCamel _
        'Or Tipo_ImpresoraFiscal = mtHKA _
        'Or Tipo_ImpresoraFiscal = mtDascomTally _
        'Then
            Call Retiro_Parcial(TipoDoc)
        'Else
            'MsgBox ("Actualmente solo puede imprimir retiros en Impresoras Fiscales Aclas/Bixolon/BMC/Camel/HKA-112/DascomTally.")
        'End If
    Else
        
        Printer.Font = "Courier New"
        Printer.Font.Size = 10
        
        'Printer.Font = "Lucida Console"
        'Printer.FontSize = 9.35 ' SIRVE CAMBIAR EL TIPO DE LETRA Y TAMAÑO _
        PARA AJUSTARSE DEPENDIENDO DEL ANCHO DE PAPEL DE LA IMPRESORA. _
        HAY QUE HACER REGLA DE NEGOCIO.
        
        If ImpresoraTicket_TipoLetra <> Empty Then
            Printer.Font = ImpresoraTicket_TipoLetra
        End If
        
        If ImpresoraTicket_TamañoFuente > 0 Then
            Printer.Font.Size = ImpresoraTicket_TamañoFuente
        End If
        
        Call Retiro_No_Fiscal(TipoDoc)
        
    End If
    
    'Printer.EndDoc
    
    Exit Function
    
End Function

Private Sub Retiro_No_Fiscal(TipoDoc As String)
    
    On Error GoTo ErrorImpresion
    
    '*** CABECERO
    Dim Temp As String
    Dim RsEmpresa As New ADODB.Recordset
    
    'Open "c:\RetiroVoucher.dat" For Output As 1
    
    If RsEmpresa.State = adStateOpen Then RsEmpresa.Close
    
    RsEmpresa.Open "select * from estruc_sis", ConexionVAD10Red, adOpenDynamic, adLockReadOnly
    
    mTam_Papel = 40
    
    Printer.Print ""
    
    If mVarImprimirCopia Then
        
        Printer.Print Centrar_Cabecera(TipoDoc, mTam_Papel)
        
        Printer.Print ""
        
    End If
    
    'Printer.Print Centrar_Cabecera(rsempresa!nom_org, mTam_papel)
    'Printer.Print Centrar_Cabecera("Sucursal:" & lclocalidaddes_user, mTam_papel)
    'Printer.Print Rellenar_Space(Temp, mTam_papel, "-")
    
    Printer.Print Centrar_Cabecera(RsEmpresa!Nom_Org, mTam_Papel)
    'printer.print Centrar_Cabecera("Sucursal:" & lclocalidaddes_user, mTam_papel)
    
    If Len(RsEmpresa!Dir_Org) <= 40 Then
        Printer.Print Centrar_Cabecera(RsEmpresa!Dir_Org, mTam_Papel)
    Else
        Printer.Print Centrar_Cabecera(Left(RsEmpresa!Dir_Org, mTam_Papel), mTam_Papel)
        Printer.Print Centrar_Cabecera(Mid(RsEmpresa!Dir_Org, mTam_Papel + 1, mTam_Papel), mTam_Papel)
    End If
    
    Printer.Print Rellenar_Space(Temp, mTam_Papel, "-")
    
    '******* DATOS DEL CAJERO
    
    Printer.Print TipoCon & " # " & NumDoc
    Printer.Print "CAJA    # " & nCaja
    Printer.Print "CAJERO(A) : " & UCase(LcDesCajero)

    Printer.Print "FECHA   : " & Date & "  " & Time
    Printer.Print "Tipo de Documento: Retiro Parcial"
    Printer.Print Rellenar_Space(Temp, mTam_Papel, "-")
    
    '
    '    '*** DETALLE
    '
    
    Dim Temp2 As String
    Dim MontoRecibido As Double
    Dim MontoIngresado As Double
    Dim MontoDevolucion As Double
    Dim MontoVendido As Double
    Dim mSimboloMonedaPredeterminada As String
    Dim mFactorMonedaPredeterminada As Double
    Dim mCambioMoneda As Boolean
    Dim RsCierres As New ADODB.Recordset
    Dim RxMonedas As New ADODB.Recordset
    Dim RsCodigos As New ADODB.Recordset
    
    Dim mTmpLn As String
    Dim CharLen As Long
    
    mSimboloMonedaPredeterminada = BuscarSimboloMonedaPredeterminada
    mFactorMonedaPredeterminada = BuscarFactorMonedaPredeterminada
    
    mTam_Papel = 40
    CharLen = mTam_Papel
    
    TotalLn = 0
    TotalCantLn = 0
    
    RANGO = Fecha()
    Call Apertura_Recordset(RsCierres)

    mSQL = "SELECT T.c_CodMoneda, T.c_CodDenomina, T.c_CodBanco, T.c_Real, T.n_Factor, " & _
    "T.n_Cantidad, T.N_BOLIVARES, T.C_TDC, T.n_Valor " & _
    "FROM TR_CIERRES T " & _
    "LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = T.c_CodMoneda " & _
    "WHERE T.c_Documento = '" & NumDoc & "' " & _
    "AND T.c_Concepto = 'CIP' " & _
    "AND T.c_CodCajero = '" & LcCajero & "' " & _
    "AND T.Turno = " & Turno & " " & _
    "ORDER BY M.b_Preferencia DESC, T.c_Real DESC, T.c_CodMoneda, T.c_CodDenomina, T.c_CodBanco "
    
    RsCierres.Open mSQL, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsCierres.EOF Then
        
        Dim Mon, Den, Real, Simbolo
        
        Mon = Empty
        Den = Empty
        Real = Empty
        Simbolo = Empty
        
        Mon = RsCierres!c_CodMoneda
        
        Call Apertura_Recordset(RxMonedas)
        
        RxMonedas.Open _
        "select * from ma_monedas " & _
        "where c_codmoneda = '" & Mon & "' ", _
        ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Printer.Print "Moneda : " & RxMonedas!c_Descripcion
        Simbolo = RxMonedas!c_Simbolo
        
        Den = RsCierres!c_CodDenomina
        Real = RsCierres!c_Real
        MonedaAnterior = Mon
        
        If RsCierres!c_Real = 1 And RsCierres!c_CodBanco = "" _
        Or RsCierres!c_Real = "S" And RsCierres!c_CodBanco = "" Then
            
            Printer.Print Centrar_Cabecera("EFECTIVO", mTam_Papel)
            
        Else
            
            Call Apertura_Recordset(RxDenomina)
            
            RxDenomina.Open _
            "SELECT * FROM MA_DENOMINACIONES " & _
            "WHERE c_CodDenomina = '" & Den & "' " & _
            "AND c_CodMoneda = '" & Mon & "' " & _
            "ORDER BY c_CodMoneda ", _
            ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            Printer.Print Centrar_Cabecera(UCase(RxDenomina!c_Denominacion), mTam_Papel)
            
        End If
        
        TotalLn = 0
        TotalCantLn = 0
        
        Do Until RsCierres.EOF
            
            If Mon <> RsCierres!c_CodMoneda Then
                mCambioMoneda = True
                Mon = RsCierres!c_CodMoneda
            Else
                mCambioMoneda = False
            End If
            
            If Den <> RsCierres!c_CodDenomina Or mCambioMoneda Then
                
                Den = RsCierres!c_CodDenomina
                Real = RsCierres!c_Real
                
                Call Apertura_Recordset(RxDenomina)
                
                RxDenomina.Open _
                "SELECT * FROM MA_DENOMINACIONES " & _
                "WHERE c_CodDenomina = '" & Den & "' " & _
                "AND c_CodMoneda = '" & Mon & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                'If rxdenomina!c_real = 0 Then printer.print  Rellenar_Space(temp2, mTam_papel, "-")
                'If rxdenomina!c_real = 0 T|hen printer.print  Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
                
                If Real = "0" Or Real = "N" Then
                    
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    'Printer.Print "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    Printer.Print Rellenar_Space("Total Denominación:", 22, " ") & Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    TotalLn = 0
                    TotalCantLn = 0
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                ElseIf Real = "1" And RxDenomina!n_Valor = 0 Then
                    
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    'printer.print Rellenar_Space("Total Denominación:", 1, " ") & Rellenar_Space(FormatNumber(totalln, 2), 2, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
                    'Printer.Print "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    Printer.Print Rellenar_Space("Total Denominación:", 22, " ") & Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    TotalLn = 0
                    TotalCantLn = 0
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                ElseIf Real = "1" And RxDenomina!n_Valor <> 0 And MonedaAnterior <> RsCierres!c_CodMoneda Then
                    
                    MonedaAnterior = RsCierres!c_CodMoneda
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    'printer.print Rellenar_Space("Total Denominación:", 1, " ") & Rellenar_Space(FormatNumber(totalln, 2), 2, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
                    'Printer.Print "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    Printer.Print Rellenar_Space("Total Denominación:", 22, " ") & Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    TotalLn = 0
                    TotalCantLn = 0
                    Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                End If
                
                If mCambioMoneda Then
                    
                    Call Apertura_Recordset(RxMonedas)
                    
                    RxMonedas.Open _
                    "SELECT * FROM MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & Mon & "' ", _
                    ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    Printer.Print "Moneda : " & RxMonedas!c_Descripcion
                    
                    Simbolo = RxMonedas!c_Simbolo
                    
                    mCambioMoneda = False
                    
                    If ((RsCierres!c_Real = 1 Or RsCierres!c_Real = "S") And RxDenomina!n_Valor > 0) Then
                        Real = RsCierres!c_Real
                        Printer.Print Centrar_Cabecera("EFECTIVO", mTam_Papel)
                    End If
                    
                End If
                
                If Not ((RsCierres!c_Real = 1 Or RsCierres!c_Real = "S") And RxDenomina!n_Valor > 0) Then
                    Den = RsCierres!c_CodDenomina
                    Printer.Print Centrar_Cabecera(UCase(RxDenomina!c_Denominacion), mTam_Papel)
                End If
                
            End If

            If Real = "1" And RsCierres!c_CodBanco = "" Or RsCierres!c_CodBanco = "" And Real = "S" Then
                
                Call Apertura_Recordset(RxDenomina)
                
                RxDenomina.Open _
                "SELECT * FROM MA_DENOMINACIONES " & _
                "WHERE c_CodDenomina = '" & RsCierres!c_CodDenomina & "' " & _
                "AND c_CodMoneda = '" & Mon & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If POS_DeclararEfectivoDesgloseAutomatico <= 0 Then
                    Printer.Print Rellenar_Space(UCase(RxDenomina!c_Denominacion), 10, " ") & _
                    "   x " & Rellenar_Space(Format(RsCierres!n_Cantidad, "####0"), 8, " ") & _
                    " " & Rellenar_Space(FormatNumber(RsCierres!n_Bolivares, 2), 16, " ")
                End If
                
            ElseIf Real = 1 And RsCierres!c_CodBanco <> "" Or Real = "S" And RsCierres!c_CodBanco <> "" Then
                
                Call Apertura_Recordset(RsCodigos)
                
                RsCodigos.Open _
                "SELECT * FROM MA_BANCOS " & _
                "WHERE c_Codigo = '" & RsCierres!c_CodBanco & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not RsCodigos.EOF Then
                    mmmBancodes = RsCodigos!c_Descripcio
                Else
                    mmmBancodes = Space(10)
                End If
                
                'printer.print Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & " " & rscierres!N_CANTIDAD & " x " & FormatNumber(rscierres!n_valor, 2) & " " & String(15 - Len(FormatNumber(rscierres!N_bolivares, 2)), " ") & FormatNumber(rscierres!N_bolivares, 2)
                
                If RsCierres!n_Cantidad = 1 And RsCierres!n_Bolivares = RsCierres!n_Valor Then
                    mTmpLn = RellenarCadenasSeparadas(Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10), " " & RsCierres!n_Cantidad & " x " & FormatNumber(RsCierres!n_Valor, 2), CharLen, , False)
                Else
                    mTmpLn = RellenarCadenasSeparadas(Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & " " & RsCierres!n_Cantidad & " x " & FormatNumber(RsCierres!n_Valor, 2) & " ", FormatNumber(RsCierres!n_Bolivares, 2), CharLen, , False)
                End If
                
                Printer.Print mTmpLn
                
            Else
                
                Call Apertura_Recordset(RsCodigos)
                
                RsCodigos.Open _
                "SELECT * FROM MA_BANCOS " & _
                "WHERE c_Codigo = '" & RsCierres!c_CodBanco & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not RsCodigos.EOF Then
                    mmmBancodes = RsCodigos!c_Descripcio
                Else
                    mmmBancodes = Space(10)
                End If
                
                Printer.Print Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & _
                " " & Rellenar_Blanco(Mid(RsCierres!c_TDC, 1, 16), 16) & _
                Rellenar_Space(FormatNumber(RsCierres!n_Bolivares, 2), 13, " ")
                
            End If
            
            TotalLn = RoundUp(TotalLn + (RsCierres!n_Bolivares * RsCierres!n_Factor), 8)  ' *
            TotalCantLn = RoundUp(TotalCantLn + _
            (RsCierres!n_Bolivares), 8)
            
            MontoRecibido = RoundUp(MontoRecibido + (RsCierres!n_Bolivares * RsCierres!n_Factor), 8)  ' *
            
            RsCierres.MoveNext
            
            If Not RsCierres.EOF Then
                Real = RsCierres!c_Real
            End If
            
        Loop
        
        Printer.Print Rellenar_Space(Temp2, mTam_Papel, "=")
        'printer.print Rellenar_Space("Total Denominación:", 5, " ") & Rellenar_Space(FormatNumber(totalln, 2), 14, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
        'Printer.Print "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
        Printer.Print "Total Denominación: " & FormatNumber(TotalCantLn, 2) & " " & Left(Simbolo, 4)
        Printer.Print Rellenar_Space(Temp2, mTam_Papel, "-")
        '//////// imprime el monto recibido en el cierre total de caja
        Printer.Print Rellenar_Space(Temp2, mTam_Papel, "-")
        Printer.Print ""
        'printer.print "Total Retiro: " & FormatNumber(montorecibido, 2) & " " & mSimboloMonedaPredeterminada
        Printer.Print "Total Retiro: " & FormatNumber(MontoRecibido, 2) & " " & mSimboloMonedaPredeterminada
        Printer.Print " "
        
        Printer.EndDoc
        
        Call Cerrar_Recordset(RsCodigos)
        Call Cerrar_Recordset(RxDenomina)
        Call Cerrar_Recordset(RsCierres)
        Call Cerrar_Recordset(RxMonedas)
        
    Else
        Mensaje True, "No se consiguio el Cierre Parcial."
    End If
    
    'Close #1
    'Dim mobjfiscal As Object
    'Set mobjfiscal = CreateObject("Imp_Fiscal.cls_Imp_Fiscal")
    'Call DllFiscal.EnviarArchivoPlanoBmc("c:\RetiroVoucher.dat")
    
    'datosEnviar = DllFiscal.CargarDatos("c:\RetiroVoucher.dat")
    'Call DllFiscal.imprimirCierreLocalBmc(datosEnviar)
    'MsgBox Printer.DeviceName
    
    Mensaje True, "Si desea puede reimprimir comprobante de este retiro en caja principal."
    
    Exit Sub
    
ErrorImpresion:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Close #1
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Retiro_No_Fiscal)"
    
End Sub

Private Sub Retiro_Parcial(TipoDoc As String)
    
    On Error GoTo ErrorImpresion
    
    '*** CABECERO
    
    Dim Temp As String
    Dim RsEmpresa As New ADODB.Recordset
    
    Dim mRuta As String
    mRuta = App.Path & "\RetiroVoucher.dat"
    
    Open mRuta For Output As 1
    
    If RsEmpresa.State = adStateOpen Then RsEmpresa.Close
    
    RsEmpresa.Open "SELECT * FROM ESTRUC_SIS", ConexionVAD10Red, adOpenDynamic, adLockReadOnly
    'mTam_papel = 30
    
    mTam_Papel = 38
    
    'Printer.Print Centrar_Cabecera(rsempresa!nom_org, mTam_papel)
    'Printer.Print Centrar_Cabecera("Sucursal:" & lclocalidaddes_user, mTam_papel)
    'Printer.Print Rellenar_Space(Temp, mTam_papel, "-")
    
    'Print #1, ""
    'Print #1, "80 " & Centrar_Cabecera(RsEmpresa!nom_org, mTam_papel)
    ''Print #1, "80 " & Centrar_Cabecera("Sucursal:" & lclocalidaddes_user, mTam_papel)
    'Print #1, "80 " & Rellenar_Space(Temp, mTam_papel, "-")
    '
    ''******* DATOS DEL CAJERO
    '
    'Print #1, "80 " & tipocon & " # " & NumDoc
    'Print #1, "80 " & "CAJA    # " & NCAJA
    'Print #1, "80 " & "CAJERO(A) : " & UCase(lcdescajero)
    '
    'Print #1, "80 " & "FECHA   : " & Date & "  " & Time
    'Print #1, "80 " & "Tipo de Documento: Retiro Parcial"
    'Print #1, "80 " & Rellenar_Space(Temp, mTam_papel, "-")
    '
    ''
    ''    '*** DETALLE
    ''
    '
    'Dim temp2 As String
    'Dim montorecibido As Double
    'Dim montoingresado As Double
    'Dim montodevolucion As Double
    'Dim montovendido As Double
    'Dim mSimboloMonedaPredeterminada As String
    'Dim mFactorMonedaPredeterminada As Double
    'Dim mCambioMoneda As Boolean
    'Dim rscierres As New ADODB.Recordset
    'Dim rxmonedas As New ADODB.Recordset
    'Dim rscodigos As New ADODB.Recordset
    '
    'mSimboloMonedaPredeterminada = BuscarSimboloMonedaPredeterminada
    'mFactorMonedaPredeterminada = BuscarFactorMonedaPredeterminada
    '
    'mTam_papel = 30
    'totalln = 0
    'RANGO = FECHA()
    'Call Apertura_Recordset(rscierres)
    '
    'mSql = " Select T.C_CODMONEDA,T.C_CODDENOMINA,T.C_CODBANCO,T.C_REAL,T.N_FACTOR,T.N_CANTIDAD,T.N_BOLIVARES,T.C_TDC,T.n_valor from TR_CIERRES T " _
    ' & " LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M ON M.C_CODMONEDA=T.C_CODMONEDA " _
    ' & " WHERE C_DOCUMENTO ='" & NumDoc & "' AND C_CONCEPTO = 'CIP' AND C_CODCAJERO = '" & lccajero & "' and TURNO = " & turno _
    ' & " Order by M.B_PREFERENCIA DESC,c_real DESC, c_coddenomina,C_CODBANCO"
    '
    'rscierres.Open mSql, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '
    'If Not rscierres.EOF Then
    'mon = ""
    'den = ""
    'Real = ""
    '
    'mon = rscierres!C_CODMONEDA
    'Call Apertura_Recordset(rxmonedas)
    'rxmonedas.Open "select * from ma_monedas where c_codmoneda = '" & mon & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '
    'Print #1, "80 " & "Moneda : " & rxmonedas!c_descripcion
    '
    'den = rscierres!c_coddenomina
    'Real = rscierres!c_real
    'MonedaAnterior = mon
    'If rscierres!c_real = 1 And rscierres!c_codbanco = "" Or rscierres!c_real = "S" And rscierres!c_codbanco = "" Then
    '    Print #1, "80 " & Centrar_Cabecera("EFECTIVO", mTam_papel)
    'Else
    '    Call Apertura_Recordset(rxdenomina)
    '    rxdenomina.Open "select * from ma_denominaciones where c_codmoneda = '" & mon & "' and c_coddenomina = '" & den & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, ADCMETEXT
    '
    '    Print #1, "80 " & Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    'End If
    'totalln = 0
    'Do Until rscierres.EOF
    '    If mon <> rscierres!C_CODMONEDA Then
    '        mCambioMoneda = True
    '        mon = rscierres!C_CODMONEDA
    '    End If
    '
    '
    '    If den <> rscierres!c_coddenomina Then
    '        den = rscierres!c_coddenomina
    '        Real = rscierres!c_real
    '        Call Apertura_Recordset(rxdenomina)
    '        rxdenomina.Open "select * from ma_denominaciones where c_codmoneda = '" & mon & "' and c_coddenomina = '" & den & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '        'If rxdenomina!c_real = 0 Then Print #1, "80 " &  Rellenar_Space(temp2, mTam_papel, "-")
    '        'If rxdenomina!c_real = 0 Then Print #1, "80 " &  Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    '        If Real = "0" Or Real = "N" Then
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "=")
    '            Print #1, "80 Total Denominación: " & FormatNumber(totalln, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
    '            totalln = 0
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '            Print #1, "80 " & Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '        ElseIf Real = "1" And rxdenomina!n_valor = 0 Then
    '
    '
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "=")
    '            'Print #1, "80 " & Rellenar_Space("Total Denominación:", 1, " ") & Rellenar_Space(FormatNumber(totalln, 2), 2, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
    '            Print #1, "80 Total Denominación: " & FormatNumber(totalln, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
    '            totalln = 0
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '            Print #1, "80 " & Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '
    '        ElseIf Real = "1" And rxdenomina!n_valor <> 0 And MonedaAnterior <> rscierres!C_CODMONEDA Then
    '            MonedaAnterior = rscierres!C_CODMONEDA
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "=")
    '            'Print #1, "80 " & Rellenar_Space("Total Denominación:", 1, " ") & Rellenar_Space(FormatNumber(totalln, 2), 2, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
    '            Print #1, "80 Total Denominación: " & FormatNumber(totalln, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
    '            totalln = 0
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '            Print #1, "80 " & Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    '            Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    '
    '        End If
    '        If mCambioMoneda Then
    '            Call Apertura_Recordset(rxmonedas)
    '            rxmonedas.Open "select * from ma_monedas where c_codmoneda = '" & mon & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '            Print #1, "80 " & "Moneda : " & rxmonedas!c_descripcion
    '            If rscierres!c_real = 1 Or rscierres!c_real = "S" Then
    '                Real = rscierres!c_real
    '                Print #1, "80 " & Centrar_Cabecera("EFECTIVO", mTam_papel)
    '            Else
    '                den = rscierres!c_coddenomina
    '                Call Apertura_Recordset(rxdenomina)
    '                rxdenomina.Open "select * from ma_denominaciones where c_codmoneda = '" & mon & "' and c_coddenomina = '" & den & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '                Print #1, "80 " & Centrar_Cabecera(UCase(rxdenomina!c_denominacion), mTam_papel)
    '
    '            End If
    '        End If
    '    End If
    '
    '    If Real = "1" And rscierres!c_codbanco = "" Or rscierres!c_codbanco = "" And Real = "S" Then
    '        Call Apertura_Recordset(rxdenomina)
    '        rxdenomina.Open "select * from ma_denominaciones where c_codmoneda = '" & mon & "' and c_coddenomina = '" & rscierres!c_coddenomina & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '        'Print #1, "80 " & Rellenar_Space(UCase(rxdenomina!c_denominacion), 10, " ") & "   x " & Rellenar_Space(Format(rscierres!N_CANTIDAD, "####0"), 8, " ") & " " & Rellenar_Space(FormatNumber(rscierres!N_bolivares, 2), 16, " ")
    '        Print #1, "80 " & rxdenomina!c_denominacion & " x " & Format(rscierres!N_CANTIDAD, "####0") & " " & FormatNumber(rscierres!N_bolivares, 2)
    '    ElseIf Real = 1 And rscierres!c_codbanco <> "" Or Real = "S" And rscierres!c_codbanco <> "" Then
    '         Call Apertura_Recordset(rscodigos)
    '        rscodigos.Open "select * from ma_bancos where c_codigo = '" & rscierres!c_codbanco & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '        If Not rscodigos.EOF Then
    '            mmmBancodes = rscodigos!C_DESCRIPCIO
    '        Else
    '            mmmBancodes = Space(10)
    '        End If
    '
    '        'Print #1, "80 " & Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & " " & rscierres!N_CANTIDAD & " x " & FormatNumber(rscierres!n_valor, 2) & " " & String(15 - Len(FormatNumber(rscierres!N_bolivares, 2)), " ") & FormatNumber(rscierres!N_bolivares, 2)
    '        Print #1, "80 " & Mid(mmmBancodes, 1, 10) & " " & rscierres!N_CANTIDAD & " x " & FormatNumber(rscierres!n_valor, 2) & " " & FormatNumber(rscierres!N_bolivares, 2)
    '    Else
    '        Call Apertura_Recordset(rscodigos)
    '        rscodigos.Open "select * from ma_bancos where c_codigo = '" & rscierres!c_codbanco & "'", ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    '
    '        If Not rscodigos.EOF Then
    '            mmmBancodes = rscodigos!C_DESCRIPCIO
    '        Else
    '            mmmBancodes = Space(10)
    '        End If
    '
    '        'Print #1, "80 " & Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & " " & Rellenar_Blanco(Mid(rscierres!c_tdc, 1, 16), 16) & Rellenar_Space(FormatNumber(rscierres!N_bolivares, 2), 13, " ")
    '        Print #1, "80 " & Mid(mmmBancodes, 1, 10) & " " & Mid(rscierres!c_tdc, 1, 16) & " " & FormatNumber(rscierres!N_bolivares, 2)
    '
    '
    '
    '    End If
    '    totalln = totalln + (rscierres!N_bolivares / rscierres!n_factor) ' *
    '    montorecibido = montorecibido + (rscierres!N_bolivares / rscierres!n_factor) ' *
    '    rscierres.MoveNext
    '
    '    If Not rscierres.EOF Then
    '        Real = rscierres!c_real
    '    End If
    '
    'Loop
    '
    'Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "=")
    ''Print #1, "80 " & Rellenar_Space("Total Denominación:", 5, " ") & Rellenar_Space(FormatNumber(totalln, 2), 14, " ") & Space(1) & Left(mSimboloMonedaPredeterminada, 4)
    'Print #1, "80 Total Denominación: " & FormatNumber(totalln, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
    'Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    ''//////// imprime el monto recibido en el cierre total de caja
    'Print #1, "80 " & Rellenar_Space(temp2, mTam_papel, "-")
    'Print #1, "80 " & ""
    ''Print #1, "80 " & "Total Retiro: " & FormatNumber(montorecibido, 2) & " " & mSimboloMonedaPredeterminada
    'Print #1, "80 " & "Total Retiro: " & FormatNumber(montorecibido, 2) & " " & mSimboloMonedaPredeterminada
    'Print #1, "80 "
    'Print #1, "81 "
    
    Print #1, ""
    
    If mVarImprimirCopia Then
        
        Print #1, Centrar_Cabecera(TipoDoc, mTam_Papel)
        
        Print #1, ""
        
    End If
    
    Print #1, Centrar_Cabecera(RsEmpresa!Nom_Org, mTam_Papel)
    
    If Len(RsEmpresa!Dir_Org) <= 38 Then
        Print #1, Centrar_Cabecera(RsEmpresa!Dir_Org, mTam_Papel)
    Else
        Print #1, Centrar_Cabecera(Left(RsEmpresa!Dir_Org, mTam_Papel), mTam_Papel)
        Print #1, Centrar_Cabecera(Mid(RsEmpresa!Dir_Org, mTam_Papel + 1, mTam_Papel), mTam_Papel)
    End If
    
    Print #1, Rellenar_Space(Temp, mTam_Papel, "-")
    
    '******* DATOS DEL CAJERO
    
    Print #1, TipoCon & " # " & NumDoc
    Print #1, "CAJA    # " & nCaja
    Print #1, "CAJERO(A) : " & UCase(LcDesCajero)

    Print #1, "FECHA   : " & Date & "  " & Time
    Print #1, "Tipo de Documento: Retiro Parcial"
    Print #1, Rellenar_Space(Temp, mTam_Papel, "-")
    
    '
    '    '*** DETALLE
    '

    Dim Temp2 As String
    Dim MontoRecibido As Double
    Dim MontoIngresado As Double
    Dim MontoDevolucion As Double
    Dim MontoVendido As Double
    Dim mSimboloMonedaPredeterminada As String
    Dim mFactorMonedaPredeterminada As Double
    Dim mCambioMoneda As Boolean
    Dim RsCierres As New ADODB.Recordset
    Dim RxMonedas As New ADODB.Recordset
    Dim RsCodigos As New ADODB.Recordset
    
    Dim mTmpLn As String
    Dim CharLen As Long
    
    mSimboloMonedaPredeterminada = BuscarSimboloMonedaPredeterminada
    mFactorMonedaPredeterminada = BuscarFactorMonedaPredeterminada
    
    CharLen = mTam_Papel
    
    TotalLn = 0
    TotalCantLn = 0
    
    RANGO = Fecha()
    Call Apertura_Recordset(RsCierres)
    
    mSQL = "SELECT T.c_CodMoneda, T.c_CodDenomina, T.c_CodBanco, T.c_Real, T.n_Factor, " & _
    "T.n_Cantidad, T.N_BOLIVARES, T.C_TDC, T.n_Valor " & _
    "FROM TR_CIERRES T " & _
    "LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = T.c_CodMoneda " & _
    "WHERE T.c_Documento = '" & NumDoc & "' " & _
    "AND T.c_Concepto = 'CIP' " & _
    "AND T.c_CodCajero = '" & LcCajero & "' " & _
    "AND T.Turno = " & Turno & " " & _
    "ORDER BY M.b_Preferencia DESC, T.c_Real DESC, T.c_CodMoneda, T.c_CodDenomina, T.c_CodBanco "
    
    RsCierres.Open mSQL, ConexionVAD20Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsCierres.EOF Then
        
        Mon = vbNullString
        Den = vbNullString
        Real = vbNullString
        
        Simbolo = vbNullString
        
        Mon = RsCierres!c_CodMoneda
        
        Call Apertura_Recordset(RxMonedas)
        
        RxMonedas.Open _
        "SELECT * FROM MA_MONEDAS " & _
        "WHERE c_CodMoneda = '" & Mon & "' ", _
        ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Print #1, "Moneda : " & RxMonedas!c_Descripcion
        Simbolo = RxMonedas!c_Simbolo
        
        Den = RsCierres!c_CodDenomina
        Real = RsCierres!c_Real
        MonedaAnterior = Mon
        
        If RsCierres!c_Real = 1 And RsCierres!c_CodBanco = Empty _
        Or RsCierres!c_Real = "S" And RsCierres!c_CodBanco = Empty Then
            Print #1, Centrar_Cabecera("EFECTIVO", mTam_Papel)
        Else
            
            Call Apertura_Recordset(RxDenomina)
            
            RxDenomina.Open _
            "SELECT * FROM MA_DENOMINACIONES " & _
            "WHERE c_CodDenomina = '" & Den & "' " & _
            "AND c_CodMoneda = '" & Mon & "' " & _
            "ORDER BY c_CodMoneda ", _
            ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            Print #1, Centrar_Cabecera(UCase(RxDenomina!c_Denominacion), mTam_Papel)
            
        End If
        
        TotalLn = 0
        TotalCantLn = 0
        
        Do Until RsCierres.EOF
            
            If Mon <> RsCierres!c_CodMoneda Then
                mCambioMoneda = True
                Mon = RsCierres!c_CodMoneda
            Else
                mCambioMoneda = False
            End If
            
            If UCase(Den) <> UCase(RsCierres!c_CodDenomina) _
            Or mCambioMoneda Then
                
                Den = RsCierres!c_CodDenomina
                Real = RsCierres!c_Real
                
                Call Apertura_Recordset(RxDenomina)
                
                RxDenomina.Open _
                "select * from ma_denominaciones " & _
                "where c_codmoneda = '" & Mon & "' " & _
                "and c_coddenomina = '" & Den & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Real = "0" Or Real = "N" Then
                    
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    'Print #1, "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    'Print #1, "Total Denominación: " & FormatNumber(TotalCantLn, 2) & " " & Left(Simbolo, 4)
                    Print #1, Rellenar_Space("Total Denominación:", 19, " ") & _
                    Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    TotalLn = 0
                    TotalCantLn = 0
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                ElseIf Real = "1" And RxDenomina!n_Valor = 0 Then
                    
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    'Print #1, "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    'Print #1, "Total Denominación: " & FormatNumber(TotalCantLn, 2) & " " & Left(Simbolo, 4)
                    Print #1, Rellenar_Space("Total Denominación:", 19, " ") & _
                    Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    
                    TotalLn = 0
                    TotalCantLn = 0
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                ElseIf Real = "1" And RxDenomina!n_Valor <> 0 _
                And UCase(MonedaAnterior) <> UCase(RsCierres!c_CodMoneda) Then
                    
                    MonedaAnterior = RsCierres!c_CodMoneda
                    
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    'Print #1, "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
                    'Print #1, "Total Denominación: " & FormatNumber(TotalCantLn, 2) & " " & Left(Simbolo, 4)
                    Print #1, Rellenar_Space("Total Denominación:", 19, " ") & _
                    Rellenar_Space(FormatNumber(TotalCantLn, 2), 14, " ") & Space(1) & Left(Simbolo, 4)
                    TotalLn = 0
                    TotalCantLn = 0
                    Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
                    
                End If
                
                If mCambioMoneda Then
                    
                    Call Apertura_Recordset(RxMonedas)
                    
                    RxMonedas.Open _
                    "SELECT * FROM MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & Mon & "' ", _
                    ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    Print #1, "Moneda : " & RxMonedas!c_Descripcion
                    Simbolo = RxMonedas!c_Simbolo
                    mCambioMoneda = False
                    
                    If ((RsCierres!c_Real = 1 Or RsCierres!c_Real = "S") _
                    And RxDenomina!n_Valor > 0) Then
                        Real = RsCierres!c_Real
                        Print #1, Centrar_Cabecera("EFECTIVO", mTam_Papel)
                    End If
                    
                End If
                
                If Not ((RsCierres!c_Real = 1 Or RsCierres!c_Real = "S") _
                And RxDenomina!n_Valor > 0) Then
                    Den = RsCierres!c_CodDenomina
                    Print #1, Centrar_Cabecera(UCase(RxDenomina!c_Denominacion), mTam_Papel)
                End If
                
            End If
            
            If Real = "1" And RsCierres!c_CodBanco = "" _
            Or RsCierres!c_CodBanco = "" And Real = "S" Then
                
                Call Apertura_Recordset(RxDenomina)
                
                RxDenomina.Open _
                "SELECT * FROM MA_DENOMINACIONES " & _
                "WHERE c_CodDenomina = '" & RsCierres!c_CodDenomina & "' " & _
                "AND c_CodMoneda = '" & Mon & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If POS_DeclararEfectivoDesgloseAutomatico <= 0 Then
                    'Print #1, RxDenomina!c_Denominacion & " x " & Format(RsCierres!n_Cantidad, "####0") & " " & FormatNumber(RsCierres!N_bolivares, 2)
                    Print #1, Rellenar_Space(UCase(RxDenomina!c_Denominacion), 10, " ") & _
                    " x " & Rellenar_Space(Format(RsCierres!n_Cantidad, "####0"), 8, " ") & _
                    " " & Rellenar_Space(FormatNumber(RsCierres!n_Bolivares, 2), 16, " ")
                End If
                
            ElseIf Real = 1 And RsCierres!c_CodBanco <> "" _
            Or Real = "S" And RsCierres!c_CodBanco <> "" Then
                
                Call Apertura_Recordset(RsCodigos)
                
                RsCodigos.Open _
                "SELECT * FROM MA_BANCOS " & _
                "WHERE c_Codigo = '" & RsCierres!c_CodBanco & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not RsCodigos.EOF Then
                    mmmBancodes = RsCodigos!c_Descripcio
                Else
                    mmmBancodes = Space(10)
                End If
                
                'Print #1, Mid(mmmBancodes, 1, 10) & " " & _
                RsCierres!n_Cantidad & " x " & FormatNumber(RsCierres!n_Valor, 2) & " " & _
                FormatNumber(RsCierres!N_bolivares, 2)
                
                If RsCierres!n_Cantidad = 1 And RsCierres!n_Bolivares = RsCierres!n_Valor Then
                    mTmpLn = RellenarCadenasSeparadas(Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10), _
                    " " & RsCierres!n_Cantidad & " x " & FormatNumber(RsCierres!n_Valor, 2), CharLen, , False)
                Else
                    mTmpLn = RellenarCadenasSeparadas(Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & _
                    " " & RsCierres!n_Cantidad & " x " & FormatNumber(RsCierres!n_Valor, 2) & " ", _
                    FormatNumber(RsCierres!n_Bolivares, 2), CharLen, , False)
                End If
                
                Print #1, mTmpLn
                
            Else
                
                Call Apertura_Recordset(RsCodigos)
                
                RsCodigos.Open _
                "SELECT * FROM MA_BANCOS " & _
                "WHERE c_Codigo = '" & RsCierres!c_CodBanco & "' ", _
                ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not RsCodigos.EOF Then
                    mmmBancodes = RsCodigos!c_Descripcio
                Else
                    mmmBancodes = Space(10)
                End If
                
                'Print #1, Mid(mmmBancodes, 1, 10) & " " & Mid(RsCierres!c_tdc, 1, 16) & " " & _
                FormatNumber(RsCierres!N_bolivares, 2)
                
                Print #1, Rellenar_Blanco(Mid(mmmBancodes, 1, 10), 10) & _
                " " & Rellenar_Blanco(Mid(RsCierres!c_TDC, 1, 14), 14) & _
                Rellenar_Space(FormatNumber(RsCierres!n_Bolivares, 2), 13, " ")
                
            End If
            
            TotalLn = RoundUp(TotalLn + _
            (RsCierres!n_Bolivares * RsCierres!n_Factor), 8)  ' *
            
            TotalCantLn = RoundUp(TotalCantLn + _
            (RsCierres!n_Bolivares), 8)
            
            MontoRecibido = RoundUp(MontoRecibido + _
            (RsCierres!n_Bolivares * RsCierres!n_Factor), 8)  ' *
            
            RsCierres.MoveNext
            
            If Not RsCierres.EOF Then
                Real = RsCierres!c_Real
            End If
            
        Loop
        
        Print #1, Rellenar_Space(Temp2, mTam_Papel, "=")
        'Print #1, "Total Denominación: " & FormatNumber(TotalLn, 2) & " " & Left(mSimboloMonedaPredeterminada, 4)
        Print #1, "Total Denominación: " & FormatNumber(TotalCantLn, 2) & " " & Left(Simbolo, 4)
        Print #1, Rellenar_Space(Temp2, mTam_Papel, "-")
        Print #1, Rellenar_Space(Temp2, mTam_Papel, "-")
        Print #1, ""
        Print #1, "Total Retiro: " & FormatNumber(MontoRecibido, 2) & " " & mSimboloMonedaPredeterminada
        Print #1, ""
        Print #1, ""
        
        Call Cerrar_Recordset(RsCodigos)
        Call Cerrar_Recordset(RxDenomina)
        Call Cerrar_Recordset(RsCierres)
        Call Cerrar_Recordset(RxMonedas)
        
    Else
        Mensaje True, "No se consiguio el Cierre Parcial."
    End If
    
    Close #1
    
    'Dim mobjfiscal As Object
    'Set mobjfiscal = CreateObject("Imp_Fiscal.cls_Imp_Fiscal")
    'Call DllFiscal.EnviarArchivoPlanoBmc("c:\RetiroVoucher.dat")
    
    'DatosEnviar = DllFiscal.CargarDatos(mRuta) '"c:\RetiroVoucher.dat")
    
    'Call DllFiscal.imprimirCierreLocalBmc(DatosEnviar)
    DllFiscal.ImprimirComandaLocal mRuta
    
    Exit Sub
    
ErrorImpresion:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume HandleErr
    
HandleErr:
    
    On Error Resume Next
    
    Close #1
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Retiro_Parcial)"
    
End Sub

Public Function Rellenar_Blanco(intValue, intDigits)
    '*** ESPACIOA A LA DER
    mValorLon = intDigits - Len(intValue)
    Rellenar_Blanco = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), " ")
End Function

Public Function Centrar_Cabecera(Titulo, Tam_papel) As String
    lon = Len(Titulo)
    mValorLon = ((Tam_papel - lon))
    Centrar_Cabecera = Rellenar_Space(Titulo, RoundUp(IIf(mValorLon < 0, 0, (mValorLon)) / 2), " ")
End Function

Public Function Rellenar_Space(intValue, intDigits, car)
    '*** ESPACIOA A LA IZQ
    mValorLon = intDigits - Len(intValue)
    Rellenar_Space = String(IIf(mValorLon < 0, intDigits, mValorLon), car) & intValue
    'MsgBox String(IIf(mValorLon < 0, intDigits, mValorLon), car) & intValue
End Function

Public Function SDecimal() As String
    Dim Numero As Double, Caracteres As String
    Numero = 123.123
    Caracteres = Trim(CStr(Numero))
    SDecimal = Mid(Caracteres, 4, 1)
End Function

Public Sub configPosTecladoManual()

    Select Case TipoPos_TecladoManual
        ' Posicion por Defecto: Centro de la pantalla (CenterScreen)
        Case 0
Default:
            TECLADO.ParentForm_PosT = 0
            TECLADO.ParentForm_PosH = 0
            TECLADO.PosT = 0
            TECLADO.PosH = 0
        ' Posicion Centrado al Fondo (CenterBottom)
        Case 1
            ' Como el valor por defecto es el centro de la pantalla, ya no necesito cambiar
            ' la posicion horizontal del teclado, solo calculo la posición para colocarlo en el fondo
            TECLADO.ParentForm_PosT = 0
            TECLADO.ParentForm_PosH = 0
            'TECLADO.PosT = Screen.Height - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight)
            'MsgBox TECLADO.PosT & " - " & ((GetDeviceCaps(TECLADO.hdc, VerticalResolution) * Screen.TwipsPerPixelY) - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight))
            TECLADO.PosT = (GetDeviceCaps(TECLADO.hdc, VerticalResolution) * Screen.TwipsPerPixelY) - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight)
            TECLADO.PosH = 0
        Case 2
            'Debajo de un control especifico
            'Under Development
            TipoPos_TecladoManual = 0
            GoTo Default
        Case 3
            'Encima de un control especifico
            'Under Development
            TipoPos_TecladoManual = 0
            GoTo Default
    End Select

End Sub

Public Function GetTaskBarHeight() As Long
    
    Dim ABD As APPBARDATA
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarHeight = ABD.rc.Bottom - ABD.rc.Top
    
    'MsgBox "Width:" & ABD.rc.Right - ABD.rc.Left
    'MsgBox " Height:" & ABD.rc.Bottom - ABD.rc.Top
    
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)
    
    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function MsjErrorRapido(ByVal pDescripcion As String, _
Optional ByVal pMsjIntro As String = "[StellarMensaje]")
    
    If pMsjIntro = "[StellarMensaje]" Then
        pMsjIntro = Replace(pMsjIntro, "[StellarMensaje]", StellarMensaje(286)) & GetLines(2) '"Ha ocurrido un error, por favor reporte lo siguiente: " & vbNewLine & vbNewLine
    End If
    
    Mensaje True, pMsjIntro & pDescripcion
    
End Function

Public Sub TecladoPOS(ByRef ObjText As Object)
    Dim TECLADO As Object
    Set TECLADO = CreateObject("DLLKeyboard.DLLTeclado")
    Call TECLADO.ShowKeyboardPOS(ObjText)
    Set TECLADO = Nothing
End Sub

Public Function ExecuteSafeSQL(ByVal Sql As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False, _
Optional ByVal DisconnectedRS As Boolean = False, _
Optional ByVal Updatable As Boolean = False) As ADODB.Recordset
    
    On Error GoTo Error
    
    If ReturnsResultSet Then
        
        If DisconnectedRS Or Updatable Then
            
            Set ExecuteSafeSQL = New ADODB.Recordset
            
            ExecuteSafeSQL.CursorLocation = adUseClient
            ExecuteSafeSQL.Open Sql, pCn, adOpenStatic, _
            IIf(Updatable, adLockBatchOptimistic, adLockReadOnly), adCmdText
            
            If DisconnectedRS Then
                Set ExecuteSafeSQL.ActiveConnection = Nothing
            End If
            
            Records = ExecuteSafeSQL.RecordCount
            
        Else
            
            Set ExecuteSafeSQL = pCn.Execute(Sql, Records)
            
        End If
        
    Else
        
        pCn.Execute Sql, Records
        Set ExecuteSafeSQL = Nothing
        
    End If
    
    Exit Function
    
Error:
    
    Set ExecuteSafeSQL = Nothing
    Records = -1
    
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    
    On Error GoTo Error
    
    If Not IsMissing(pRs) Then
        mAux = pRs.Fields(pCampo).Value
        
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        If IsMissing(pCn) Then
            mRs.Open pSql, ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
        Else
            mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        
        ExisteCampoTabla = True
        
        mRs.Close
    End If
    
    Exit Function
    
Error:
    
    'MsgBox Err.Description
    'Debug.Print Err.Description
    
    Err.Clear
    ExisteCampoTabla = False
    
End Function

Public Function ExisteCampoTablaV2(ByVal pTabla As String, ByVal pCampo As String, _
pCn As ADODB.Connection, _
Optional ByVal pBD As String = "", _
Optional ByVal pTipoCampoVerificar As String = "", _
Optional ByVal pCampoTextoCriterioCaracteres As String = "", _
Optional ByVal pCampoNumeroCriterioPrecisionEntera As String = "", _
Optional ByVal pCampoNumeroCriterioPrecisionDecimal As String = "", _
Optional ByVal pCampoDateTimeCriterioPrecision As String = "" _
) As Boolean
    
    ' Ej pTipoCampoVerificar: "char", "varchar", "text", "numeric", "float", "int", "date", "time", "datetime", _
    "ntext", "nchar", "nvarchar", "bigint", "binary", "varbinary", "bit", "money", "real", "xml", etc...
    
    ' Ej pCampoTextoCriterioCaracteres: " = 50", " <= 100", " >= 255", etc..
    
    ' Ej pCampoNumeroCriterioPrecisionEntera: " >= 10", " = 18", etc
    
    ' Ej pCampoNumeroCriterioPrecisionDecimal: " <= 2", " = 4", " = 0", "IS NULL", "IS NOT NULL", etc
    
    ' Ej pCampoDateTimeCriterioPrecision: " = 3", " = 0", etc...
    
    ' CHARACTER_MAXIMUM_LENGTH = -1 CUANDO EL CAMPO ES NVARCHAR(MAX). PARA NO ENTRAR EN CONFLICTO
    ' CON VALIDACION DE LONGITUD POR DEBAJO DE pCampoTextoCriterioCaracteres, ASUMIREMOS MAX COMO 999999
    ' SI QUEREMOS SABER SI UN CAMPO TIENE NVARCHAR(MAX) PASAMOS pCampoTextoCriterioCaracteres = 999999
    
    On Error Resume Next
    
    Dim pRs As ADODB.Recordset, mSQL As String, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    
    mSQL = "SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS " & _
    "WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pCampo & "'" & _
    IIf(Len(pTipoCampoVerificar) > 0, " AND DATA_TYPE = '" & pTipoCampoVerificar & "'", "") & _
    IIf(Len(pCampoTextoCriterioCaracteres) > 0, " AND CASE WHEN CHARACTER_MAXIMUM_LENGTH = -1 THEN 999999 ELSE CHARACTER_MAXIMUM_LENGTH END " & pCampoTextoCriterioCaracteres, "") & _
    IIf(Len(pCampoNumeroCriterioPrecisionEntera) > 0, " AND NUMERIC_PRECISION " & pCampoNumeroCriterioPrecisionEntera, "") & _
    IIf(Len(pCampoNumeroCriterioPrecisionDecimal) > 0, " AND NUMERIC_SCALE " & pCampoNumeroCriterioPrecisionDecimal, "") & _
    IIf(Len(pCampoDateTimeCriterioPrecision) > 0, " AND DATETIME_PRECISION " & pCampoDateTimeCriterioPrecision, "")
    
    Set pRs = pCn.Execute(mSQL)
    
    ExisteCampoTablaV2 = Not (pRs.EOF And pRs.BOF)
    
    pRs.Close
    
End Function

Public Function AsEnumerable(ArrayList As Variant, _
Optional ByVal pCtrlError As Boolean = True) As Collection
    
    Set AsEnumerable = New Collection
    
    If pCtrlError Then On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal Sql As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open Sql, IIf(pCn Is Nothing, ConexionVAD10Red, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function BuscarReglaNegocioStr(ByVal pCampo, _
Optional ByVal pDefault As String) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = '" & pCampo & "' "
    
    mRs.Open mSQL, ConexionVAD10Red, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function


Public Function RellenarCadenasSeparadas(ByVal pCadenaIzq As String, ByVal pCadenaDer As String, _
ByVal pLongitud As Long, _
Optional pChar As String = " ", _
Optional TruncarSiNoHayEspacioRestante As Boolean = True) As String
    
    Dim Restantes As Long
    
    Restantes = pLongitud - (Len(pCadenaIzq) + Len(pCadenaDer))
    
    If Restantes > 0 Then
        RellenarCadenasSeparadas = pCadenaIzq & String(Restantes, pChar) & pCadenaDer
    Else
        If TruncarSiNoHayEspacioRestante Then
            RellenarCadenasSeparadas = Left(pCadenaIzq & pCadenaDer, pLongitud)
        Else
            RellenarCadenasSeparadas = pCadenaIzq & pCadenaDer
        End If
    End If
    
End Function

Public Function RellenarCadena_ALaIzq(ByVal pCadena As String, ByVal pLongitud As Long, _
Optional pChar As String = " ") As String
    
    Dim Restantes As Long
    
    Restantes = pLongitud - Len(pCadena)
    
    If Restantes > 0 Then
        RellenarCadena_ALaIzq = String(Restantes, pChar) & pCadena
    Else
        RellenarCadena_ALaIzq = pCadena
    End If
    
End Function

Public Function RellenarCadena_ALaDer(ByVal pCadena As String, ByVal pLongitud As Long, _
Optional pChar As String = " ") As String
    
    Dim Restantes As Long
    
    Restantes = pLongitud - Len(pCadena)
    
    If Restantes > 0 Then
        RellenarCadena_ALaDer = pCadena & String(Restantes, pChar)
    Else
        RellenarCadena_ALaDer = pCadena
    End If
    
End Function

Public Function FechaBD(ByVal Expression, _
Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si aún así se desea utilizar este formato, volver a llamar a esta función sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function RoundUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Double
    RoundUp = CDbl(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    Call ReleaseCapture
    
    lngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas están en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        If Forma.Left < 0 Then Forma.Left = 0
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resolución mínima de Stellar, se centra el form.
        Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
        Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
    End If
End Sub

Public Sub ActivarMensajeGrande(Optional ByVal pPorcentaje As Integer = 20)
    
    On Error GoTo Falla_Local
    
    Dim PorcWidth As Double, PorcHeight As Double
    
    If pPorcentaje <= 0 Then Exit Sub
    
    If pPorcentaje > 100 Then pPorcentaje = 100
    
    PorcWidth = (1 + (CDbl(pPorcentaje) / 2 / 100))
    PorcHeight = (1 + (CDbl(pPorcentaje) / 100))
    
    With frm_Mensajeria
    
        .Width = .Width * PorcWidth
        .Height = .Height * (1 + (CDbl(pPorcentaje) / 1.4 / 100))
        
        .FrameTitulo.Width = .FrameTitulo.Width * PorcWidth
        .lbl_Organizacion.Width = .Width
        
        .lbl_Website.Left = .lbl_Website.Left * PorcWidth
        .Exit.Left = .Exit.Left * PorcWidth
        
        .Mensaje.Width = .Mensaje.Width * PorcWidth
        .Mensaje.Height = .Mensaje.Height * PorcHeight
        
        .lblMarginTop.Width = .lblMarginTop.Width * PorcWidth
        .lblMarginTop.Height = .lblMarginTop.Height * PorcHeight
        
        '.btnCopy.Left = .btnCopy.Left * PorcWidth
        .btnCopy.Top = .btnCopy.Top * (1 + (CDbl(pPorcentaje) / 1.25 / 100))
        .btnCopy.Width = .btnCopy.Width * PorcWidth
        .btnCopy.Height = .btnCopy.Height * PorcWidth
        
        .Cancelar.Left = .Cancelar.Left * (1 + (CDbl(pPorcentaje) / 2.25 / 100))
        .Cancelar.Top = .btnCopy.Top
        .Cancelar.Width = .Cancelar.Width * PorcWidth
        .Cancelar.Height = .Cancelar.Height * PorcWidth
        
        .Aceptar.Left = .Aceptar.Left * (1 + (CDbl(pPorcentaje) / 2.25 / 100))
        .Aceptar.Top = .btnCopy.Top
        .Aceptar.Width = .Aceptar.Width * PorcWidth
        .Aceptar.Height = .Aceptar.Height * PorcWidth
        
    End With
    
Falla_Local:

End Sub

Public Function FormatoDecimalesDinamicos(ByVal pMonto As Double, _
Optional ByVal pMinimoDecimales As Integer = 0) As String
    
    Dim mFormato As String
    
    If pMinimoDecimales = 0 And (Round(pMonto - Fix(pMonto), 8)) = 0 Then
        FormatoDecimalesDinamicos = Format(pMonto, "###,###,##0")
    Else
        mFormato = "###,###,##0." & _
        IIf(pMinimoDecimales > 0, String(pMinimoDecimales, "0"), Empty) & _
        "##########"
        FormatoDecimalesDinamicos = Format(pMonto, mFormato)
    End If
    
End Function

Public Function Mensaje(ByVal Activo As Boolean, ByVal Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    
    Uno = Activo
    
    On Error GoTo Falla_Local
    
    If Not IsMissing(txtBoton1) Then txtMensaje1 = CStr(txtBoton1) Else txtMensaje1 = ""
    If Not IsMissing(txtBoton2) Then txtMensaje2 = CStr(txtBoton2) Else txtMensaje2 = ""
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            
            frm_Mensajeria.Show
            
            frm_Mensajeria.Aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
        End If
        
    End If
    
    Mensaje = Retorno
    
Falla_Local:

End Function

Public Function QuickInputRequest(ByVal pTituloSolicitud As String, Optional pMostrarBotonSalir As Boolean = True, _
Optional ByVal pDefaultAlCancelar As String = "", Optional ByVal pDefaultAlIniciar As String = "", _
Optional pPlaceHolder As String = "", Optional pTituloBarra As String = "Stellar POS", _
Optional pAlineamientoTituloSolicitud As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoInput As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoPlaceHolder As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pAlineamientoTituloBarra As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pTecladoAutomatico As Boolean = False, _
Optional pAllowEmptyResp As Boolean = False, _
Optional pPwd As Boolean = False) As String
    
    If pMostrarBotonSalir Then
        FrmInputBox.lbl_Organizacion.Width = FrmInputBox.FrameTitulo.Width - FrmInputBox.CmdClose.Width - 100 '4215
        FrmInputBox.CmdClose.Visible = True
    Else
        FrmInputBox.lbl_Organizacion.Width = FrmInputBox.FrameTitulo.Width '4650
    End If
    
    FrmInputBox.Request.Alignment = pAlineamientoTituloSolicitud
    FrmInputBox.RequestScroll.Alignment = pAlineamientoTituloSolicitud
    
    FrmInputBox.UserInput.Alignment = pAlineamientoInput
    FrmInputBox.UserInputScroll.Alignment = pAlineamientoInput
    
    FrmInputBox.PlaceHolder.Alignment = pAlineamientoPlaceHolder
    FrmInputBox.PlaceHolderScroll.Alignment = pAlineamientoPlaceHolder
    
    FrmInputBox.lbl_Organizacion.Alignment = pAlineamientoTituloBarra
    
    FrmInputBox.DefaultInput = pDefaultAlIniciar
    FrmInputBox.DefaultOutput = pDefaultAlCancelar
    FrmInputBox.Request.Text = pTituloSolicitud ' Se recomienda verificar que el Titulo se visualice correctamente.
    FrmInputBox.OptionalPlaceHolder = pPlaceHolder ' Se recomienda verificar que el Titulo se visualice correctamente.
    FrmInputBox.lbl_Organizacion.Caption = Left(pTituloBarra, 45)
    
    FrmInputBox.TecladoAutomatico = pTecladoAutomatico
    FrmInputBox.AllowEmptyResp = pAllowEmptyResp
    
    If pPwd Then
        FrmInputBox.UserInput.PasswordChar = "*"
        FrmInputBox.UserInputScroll.PasswordChar = "*"
    End If
    
    FrmInputBox.Show vbModal
    
    QuickInputRequest = FrmInputBox.InputData
    
    Unload FrmInputBox
    
    Set FrmInputBox = Nothing
    
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If Not Objeto Is Nothing Then
        If OrMode Then
            PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
        Else
            PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
        End If
    End If
End Function

Public Sub SafeFocus(ByVal pObj As Object)
    If PuedeObtenerFoco(pObj) Then pObj.SetFocus
End Sub

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

' Formatea Fecha Corta o Larga si incluye hora, según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta según configuración regional. _
Solo debe usarse en capa de presentación, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    If IsMissing(pFecha) Then pFecha = Now
    pFecha = SDate(CDate(pFecha))
    EndOfDay = DateAdd("h", 23, DateAdd("n", 59, DateAdd("s", 59, pFecha)))
End Function

' Convert a Date into a SYSTEMTIME.
Private Sub DateToSystemTime(ByVal The_Date As Date, ByRef System_Time As SystemTime)
    With System_Time
        .wYear = Year(The_Date)
        .wMonth = Month(The_Date)
        .wDay = Day(The_Date)
        .wHour = Hour(The_Date)
        .wMinute = Minute(The_Date)
        .wSecond = Second(The_Date)
    End With
End Sub

Private Sub SystemTimeToDate(System_Time As SystemTime, ByRef The_Date As Date)
    
    With System_Time
        
        The_Date = DateSerial(.wYear, .wMonth, .wDay) + _
        TimeSerial(.wHour, .wMinute, .wSecond)
        
    End With
    
End Sub

' Convert a local time to UTC.
Private Function LocalTimeToUTC(ByVal The_Date As Date) As Date
    
    Dim System_Time As SystemTime
    Dim Local_File_Time As FILETIME
    Dim UTC_File_Time As FILETIME
    
    ' Convert it into a SYSTEMTIME.
    DateToSystemTime The_Date, System_Time
    
    ' Convert it to a FILETIME.
    SystemTimeToFileTime System_Time, Local_File_Time
    
    ' Convert it to a UTC time.
    LocalFileTimeToFileTime Local_File_Time, UTC_File_Time
    
    ' Convert it to a SYSTEMTIME.
    FileTimeToSystemTime UTC_File_Time, System_Time
    
    ' Convert it to a Date.
    SystemTimeToDate System_Time, The_Date
    
    LocalTimeToUTC = The_Date
    
End Function

' Convert a UTC time into local time.
Private Function UTCToLocalTime(ByVal The_Date As Date) As Date
    
    Dim System_Time As SystemTime
    Dim Local_File_Time As FILETIME
    Dim UTC_File_Time As FILETIME
    
    ' Convert it into a SYSTEMTIME.
    DateToSystemTime The_Date, System_Time
    
    ' Convert it to a UTC time.
    SystemTimeToFileTime System_Time, UTC_File_Time
    
    ' Convert it to a FILETIME.
    FileTimeToLocalFileTime UTC_File_Time, Local_File_Time
    
    ' Convert it to a SYSTEMTIME.
    FileTimeToSystemTime Local_File_Time, System_Time
    
    ' Convert it to a Date.
    SystemTimeToDate System_Time, The_Date
    
    UTCToLocalTime = The_Date
    
End Function

Public Function GetCurrentSystemTime() As SystemTime
    Dim SysT As SystemTime
    On Error Resume Next
    GetLocalTime SysT
    GetCurrentSystemTime = SysT
End Function

Public Function SafeTickCount() As Long
    On Error Resume Next
    SafeTickCount = GetTickCount
End Function

Public Function GetTickDifInMillis(ByVal pTickIni As Long, ByVal pTickEnd As Long, _
Optional ByVal pIgnoreSeconds As Boolean = True, _
Optional ByRef pFixStartDate As Date, _
Optional ByRef pFixEndDate As Date) As Long
    Dim FullTickDifInMillis As Long
    FullTickDifInMillis = (pTickEnd - pTickIni)
    GetTickDifInMillis = FullTickDifInMillis
    If pIgnoreSeconds Then
        GetTickDifInMillis = GetTickDifInMillis - (Fix(CDec(GetTickDifInMillis) / CDec(1000)) * CDec(1000))
    End If
    If CDec(pFixStartDate) > 0 And CDec(pFixEndDate) Then
        If DateDiff("s", pFixStartDate, pFixEndDate) > Fix(CDec(FullTickDifInMillis) / CDec(1000)) Then
            pFixEndDate = DateAdd("s", -1, pFixEndDate)
        End If
    End If
End Function

Public Sub EsperaManualMillis(ByVal pTiempoMilliSegundos As Long)
    
    If pTiempoMilliSegundos > 0 Then
        
        Dim mSumar As Variant, mInicio As Variant, CantCiclos As Variant
        
        'Dim A As SystemTime, B As SystemTime
        
        'A = GetCurrentSystemTime
        
        Dim C As Variant, D As Variant
        
        'C = CDec(A.wMilliseconds) + (CDec(A.wSecond) * CDec(1000)) + _
        (CDec(A.wMinute) * CDec(60000)) + (CDec(A.wHour) * CDec(3600000))
        
        C = SafeTickCount
        
        mInicio = C
        
        D = mInicio + pTiempoMilliSegundos
        
        CantCiclos = CDec(0)
        
        Do While C < D
            
            CantCiclos = CantCiclos + 1
            
            'B = GetCurrentSystemTime
            
            'C = CDec(B.wMilliseconds) + (CDec(B.wSecond) * CDec(1000)) + _
            (CDec(B.wMinute) * CDec(60000)) + (CDec(B.wHour) * CDec(3600000))
            
            C = SafeTickCount
            
            DoEvents
            
            If C = 0 Then
                Exit Do
            End If
            
        Loop
        
    Else
        Exit Sub
    End If
    
End Sub

Public Function ValidarConexion(pConexion As Object, _
Optional pNewConnectionString As String, _
Optional TmpCnTimeOut As Long = 4) As Boolean
    
    On Error GoTo Check
    
    ' La idea de esta función es reestablecer conexiones que se asume estan
    ' en estado abierto pero internamente pudieran estar ocasionando error.
    ' Actualización: O si está cerrada, intentar abrirla nuevamente para poder utilizarla.
    
    'Debug.Print pConexion.ConnectionString
    
    Dim mRs As ADODB.Recordset, mSQL As String, Reintento As Boolean
    Dim mConnectionString As String, mConnectionTimeOut As Long, mCommandTimeOut As Long
    
    mConnectionTimeOut = pConexion.ConnectionTimeout
    mCommandTimeOut = pConexion.CommandTimeout
    
TryAgain:
    
    mSQL = "SELECT 1 AS ConexionActiva"
    
    Set mRs = New ADODB.Recordset
    
    pConexion.CommandTimeout = 4
    
    ' DoEvents ' Mejor nó. No vaya a ser que otros eventos
    ' generados por el usuario provoquen acciones no deseadas.
    
    mRs.Open mSQL, pConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    'Debug.Print "Conexion Activa:", Not MRS.EOF
    
    ValidarConexion = True
    
Finally:
    
    pConexion.CommandTimeout = mCommandTimeOut
    
    Exit Function
    
Check:
    
    'Save Error properties.
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mDLLError = Err.LastDllError
    mErrorSource = Err.Source
    
    ' DoEvents ' Mejor nó. No vaya a ser que otros eventos
    ' generados por el usuario provoquen acciones no deseadas.
    
    Resume ResetErrorHandler
    
ResetErrorHandler:
    
    On Error GoTo Check
    
    If (mErrorNumber = (-2147467259) Or mErrorNumber = (3709)) And Not Reintento Then ' Error de Conexión / Error General de Red / Conexión cerrada.
        
        If pConexion.State <> adStateClosed Then pConexion.Close
        
        mConnectionString = IIf(Trim(pNewConnectionString) <> vbNullString, pNewConnectionString, pConexion.ConnectionString)
        
        'If DebugModeExtendido Then
            'Mensaje True, "Intentando Reestablecer conexión " & vbNewLine & mConnectionString
        'End If
        
        Debug.Print "Intentando Reestablecer conexión", mConnectionString
        
        Reintento = True
        
        pConexion.ConnectionTimeout = TmpCnTimeOut
        
        pConexion.Open mConnectionString
        
        'If DebugModeExtendido Then
            'Mensaje True, "Conexión Reactivada " & vbNewLine & mConnectionString
        'End If
        
        Debug.Print "Conexión Reactivada."
        
        ExecuteSafeSQL "SET DATEFIRST " & DiaInicioSemanaSQL, pConexion
        
        Reintento = False 'Abrio la conexión... por lo tanto, vamos a hacer otro intento de validación.
        
    Else ' Ante otros errores desconocidos / de otra causa, salir.
        Reintento = True
    End If
    
    If Not Reintento Then
        
        GoTo TryAgain
        
    Else
        
        'If DebugModeExtendido Then
            'Mensaje True, "Fallo al intentar reestablecer la conexión."
        'End If
        
        Debug.Print "Fallo al intentar reestablecer la conexión."
        
        If pConexion.State = adStateClosed Then
            pConexion.ConnectionTimeout = mConnectionTimeOut
        End If
        
        ValidarConexion = False
        
    End If
    
    GoTo Finally
    
End Function
